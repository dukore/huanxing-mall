package com.huanxing.cloud.auth.controller;

import cn.dev33.satoken.secure.SaSecureUtil;
import cn.dev33.satoken.stp.SaLoginConfig;
import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONUtil;
import com.huanxing.cloud.common.core.constant.CacheConstants;
import com.huanxing.cloud.common.core.constant.CommonConstants;
import com.huanxing.cloud.common.core.entity.Result;
import com.huanxing.cloud.security.service.HuanxingUserDetailsService;
import com.huanxing.cloud.upms.common.entity.SysUser;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * token
 *
 * @author lijx
 * @since 2022/2/10 16:25
 */
@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/token")
public class TokenController {
  private final HuanxingUserDetailsService huanxingUserDetailsService;

  @ApiOperation(value = "系统用户账号登录")
  @RequestMapping("/login")
  public Result login(String username, String password) {
    SysUser sysUser = huanxingUserDetailsService.loadUserByUsername(username);
    if (ObjectUtil.isNull(sysUser)) {
      return Result.fail("用户不存在");
    }
    if (!sysUser.getPassword().equals(SaSecureUtil.md5(password))) {
      return Result.fail("账号或密码错误");
    }
    if (!CommonConstants.YES.equals(sysUser.getEnableFlag())) {
      return Result.fail("状态异常不可登录");
    }
    StpUtil.login(sysUser.getId(), SaLoginConfig.setExtra("username", sysUser.getUsername()));
    StpUtil.getSession()
        .set(CacheConstants.MENU_CACHE, JSONUtil.toJsonStr(sysUser.getPermissions()));
    StpUtil.getSession().set(CacheConstants.ROLE_CACHE, JSONUtil.toJsonStr(sysUser.getRoles()));
    return Result.success(StpUtil.getTokenInfo());
  }

  @ApiOperation(value = "系统用户手机号登录")
  @RequestMapping("/phone/login")
  public Result phoneLogin(String phone) {
    SysUser sysUser = huanxingUserDetailsService.loadUserByPhone(phone);
    if (ObjectUtil.isNull(sysUser)) {
      return Result.fail("用户不存在");
    }
    if (!sysUser.getEnableFlag().equals(CommonConstants.YES)) {
      return Result.fail("状态异常不可登录");
    }
    StpUtil.login(sysUser.getId(), SaLoginConfig.setExtra("username", sysUser.getUsername()));
    StpUtil.getSession()
        .set(CacheConstants.MENU_CACHE, JSONUtil.toJsonStr(sysUser.getPermissions()));
    StpUtil.getSession().set(CacheConstants.ROLE_CACHE, JSONUtil.toJsonStr(sysUser.getRoles()));
    return Result.success(StpUtil.getTokenInfo());
  }
  /**
   * 用户退出
   *
   * @author lijx
   * @date 2022/5/3 20:46
   * @version 1.0
   */
  @DeleteMapping("/logout")
  public Result<?> logout() {
    StpUtil.logout();
    return Result.success();
  }

  @ApiOperation(value = "令牌列表")
  @GetMapping("/page")
  public Result page() {
    List<String> list = StpUtil.searchTokenValue("", -1, 1);
    return Result.success(list);
  }
}
