package com.huanxing.cloud.auth.listener;

import cn.dev33.satoken.listener.SaTokenListener;
import cn.dev33.satoken.spring.SpringMVCUtil;
import cn.dev33.satoken.stp.SaLoginModel;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.servlet.ServletUtil;
import cn.hutool.http.useragent.UserAgent;
import cn.hutool.http.useragent.UserAgentUtil;
import com.huanxing.cloud.common.core.constant.CommonConstants;
import com.huanxing.cloud.common.core.constant.SecurityConstants;
import com.huanxing.cloud.common.core.util.IpUtils;
import com.huanxing.cloud.upms.common.entity.SysLoginLog;
import com.huanxing.cloud.upms.common.feign.FeignSysLoginLogService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
/**
 * @author lijx
 * @date 2022/7/8
 */
@Slf4j
@Component
@AllArgsConstructor
public class HxSaTokenListener implements SaTokenListener {
  private final FeignSysLoginLogService feignSysLoginLogService;
  /** 每次登录时触发 */
  @Override
  public void doLogin(
      String loginType, Object loginId, String tokenValue, SaLoginModel loginModel) {
    // 处理登录日志
    String userName = loginModel.getExtra("username").toString();
    String agent = SpringMVCUtil.getRequest().getHeader("User-Agent").toLowerCase();
    UserAgent ua = UserAgentUtil.parse(agent);
    SysLoginLog sysLoginLog = new SysLoginLog();
    sysLoginLog.setStatus(CommonConstants.LOGIN_LOG_STATUS_1);
    sysLoginLog.setIpAddr(ServletUtil.getClientIP(SpringMVCUtil.getRequest()));
    sysLoginLog.setLocation(IpUtils.getWhoisAddress(sysLoginLog.getIpAddr()));
    sysLoginLog.setCreateId(loginId.toString());
    sysLoginLog.setOs(ua.getOs().toString());
    sysLoginLog.setBrowser(ua.getBrowser().toString());
    sysLoginLog.setMsg(StrUtil.format("用户：{} 登录成功", userName));
    sysLoginLog.setUserName(userName);
    feignSysLoginLogService.save(sysLoginLog, SecurityConstants.SOURCE_IN);
    //     异步处理登录日志
    //    CompletableFuture.runAsync(
    //        () -> {
    //          String agent =  SaHolder.getRequest().getHeader("User-Agent").toLowerCase();
    //          UserAgent ua = UserAgentUtil.parse(agent);
    //          SysLoginLog sysLoginLog = new SysLoginLog();
    //          sysLoginLog.setStatus(CommonConstants.LOGIN_LOG_STATUS_0);
    //          sysLoginLog.setIpAddr(ServletUtil.getClientIP(request));
    //          sysLoginLog.setLocation(IpUtils.getWhoisAddress(sysLoginLog.getIpAddr()));
    //          sysLoginLog.setUserName(event.getAuthentication().getPrincipal().toString());
    //          sysLoginLog.setOs(ua.getOs().toString());
    //          sysLoginLog.setBrowser(ua.getBrowser().toString());
    //          sysLoginLog.setMsg(
    //              StrUtil.format(
    //                  "用户：{} 登录失败，异常：{}",
    //                  event.getAuthentication().getPrincipal(),
    //                  event.getException().getLocalizedMessage()));
    //
    //          feignSysLoginLogService.save(sysLoginLog, SecurityConstants.SOURCE_IN);
    //        });
  }

  @Override
  public void doLogout(String s, Object o, String s1) {}

  @Override
  public void doKickout(String s, Object o, String s1) {}

  @Override
  public void doReplaced(String s, Object o, String s1) {}

  @Override
  public void doDisable(String s, Object o, long l) {}

  @Override
  public void doUntieDisable(String s, Object o) {}

  @Override
  public void doCreateSession(String s) {}

  @Override
  public void doLogoutSession(String s) {}
}
