package com.huanxing.cloud.upms.admin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huanxing.cloud.upms.admin.mapper.SysMenuMapper;
import com.huanxing.cloud.upms.admin.service.ISysMenuService;
import com.huanxing.cloud.upms.common.entity.SysMenu;
import com.huanxing.cloud.upms.common.vo.MenuVO;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 系统菜单
 *
 * @author lijx
 * @since 2022/2/26 16:47
 */
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu>
        implements ISysMenuService {
    @Override
    public List<MenuVO> findMenuByRoleId(String roleId) {
        return baseMapper.listMenuByRoleId(roleId);
    }
}
