package com.huanxing.cloud.upms.admin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huanxing.cloud.upms.admin.mapper.SysRoleMenuMapper;
import com.huanxing.cloud.upms.admin.service.ISysRoleMenuService;
import com.huanxing.cloud.upms.common.entity.SysRoleMenu;
import org.springframework.stereotype.Service;

/**
 * 角色关联菜单
 *
 * @author lijx
 * @since 2022/2/26 16:47
 */
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu>
        implements ISysRoleMenuService {
}
