package com.huanxing.cloud.mall.admin.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.huanxing.cloud.common.core.entity.Result;
import com.huanxing.cloud.mall.common.properties.MallConfigProperties;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 物流公司配置
 *
 * @author lijx
 * @date 2022/6/13
 */
@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("/logisticscompanyconfig")
@Api(value = "logisticscompanyconfig", tags = "物流公司配置")
public class LogisticsCompanyConfigController {

  private final MallConfigProperties mallConfigProperties;

  @ApiOperation(value = "物流公司列表查询")
  @SaCheckPermission("mall:logisticscompanyconfig:get")
  @GetMapping("/list")
  public Result page() {
    return Result.success(mallConfigProperties.getLogisticsCompanyInfos());
  }
}
