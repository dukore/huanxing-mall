package com.huanxing.cloud.mall.api.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.huanxing.cloud.common.core.constant.CommonConstants;
import com.huanxing.cloud.common.core.entity.Result;
import com.huanxing.cloud.mall.api.service.IMobileThemeService;
import com.huanxing.cloud.mall.common.entity.MobileTheme;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 商城移动端主题
 *
 * @author lijx
 * @since 2022/3/17 10:55
 */
@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("/mobiletheme")
@Api(value = "mobiletheme", tags = "商城移动端主题")
public class MobileThemeController {
    private final IMobileThemeService mobileThemeService;

    @ApiOperation(value = "获取商城移动端主题")
    @GetMapping
    public Result getMobileTheme() {
        return Result.success(mobileThemeService.getOne(Wrappers.<MobileTheme>lambdaQuery().eq(MobileTheme::getIsUse, CommonConstants.YES)));
    }


}
