package com.huanxing.cloud.mall.api.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huanxing.cloud.mall.common.entity.UserCollect;

public interface UserCollectMapper extends BaseMapper<UserCollect> {
}
