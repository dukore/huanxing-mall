package com.huanxing.cloud.mall.api.job;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.huanxing.cloud.mall.api.service.IGrouponRecordService;
import com.huanxing.cloud.mall.common.constant.MallEventConstants;
import com.huanxing.cloud.mall.common.entity.GrouponRecord;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 拼团任务
 *
 * @author lijx
 * @date 2022/10/20
 */
@Component
@AllArgsConstructor
public class GrouponJobHandler {

  private final IGrouponRecordService grouponRecordService;
  /**
   * 关闭超时未成团拼团 Close overtime group
   *
   * @author lijx
   * @date 2022/10/20
   * @return: void
   */
  @XxlJob("closeOvertimeGroupJobHandler")
  public void closeOvertimeGroupJobHandler() throws Exception {
    XxlJobHelper.log("关闭超时未成团拼团, closeOvertimeGroupJobHandler.");

    List<GrouponRecord> grouponRecordList =
        grouponRecordService.list(
            Wrappers.<GrouponRecord>lambdaQuery()
                .eq(GrouponRecord::getStatus, MallEventConstants.GROUPON_RECORD_STATUS_0)
                .lt(GrouponRecord::getOverdueTime, LocalDateTime.now()));
    grouponRecordList.forEach(
            grouponRecordService::closeOvertimeGroup);
    // default success
  }
}
