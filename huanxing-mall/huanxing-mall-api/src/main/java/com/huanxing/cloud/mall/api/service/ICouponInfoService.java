package com.huanxing.cloud.mall.api.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.huanxing.cloud.mall.common.entity.CouponGoods;
import com.huanxing.cloud.mall.common.entity.CouponInfo;

public interface ICouponInfoService extends IService<CouponInfo> {

    IPage<CouponInfo> getPage(Page page, CouponInfo couponInfo, CouponGoods couponGoods);
}
