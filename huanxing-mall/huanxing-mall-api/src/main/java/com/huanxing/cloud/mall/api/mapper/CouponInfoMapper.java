package com.huanxing.cloud.mall.api.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huanxing.cloud.mall.common.entity.CouponGoods;
import com.huanxing.cloud.mall.common.entity.CouponInfo;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;

public interface CouponInfoMapper extends BaseMapper<CouponInfo> {
    IPage<CouponInfo> selectCouponPage(Page page, @Param("query")  CouponInfo couponInfo, @Param("couponGoods") CouponGoods couponGoods);

    CouponInfo selectCouponById(Serializable id);
}
