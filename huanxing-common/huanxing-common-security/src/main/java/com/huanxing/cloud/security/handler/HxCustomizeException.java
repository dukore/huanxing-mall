package com.huanxing.cloud.security.handler;

import com.huanxing.cloud.common.core.constant.CommonConstants;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * 自定义异常
 *
 * @author lijx
 * @date 2022/9/20
 */
@Data
public class HxCustomizeException extends RuntimeException {
  private Integer code;
  private String msg;

  public HxCustomizeException() {
    this.code = CommonConstants.FAIL;
  }

  public HxCustomizeException(String msg) {
    this.code = CommonConstants.FAIL;
    this.msg = msg;
  }

  public HxCustomizeException(Integer code, String msg) {
    this.code = code;
    this.msg = msg;
  }
}
