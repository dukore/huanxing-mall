package com.huanxing.cloud.security.service.impl;

import com.huanxing.cloud.common.core.constant.SecurityConstants;
import com.huanxing.cloud.common.core.entity.Result;
import com.huanxing.cloud.security.service.HuanxingUserDetailsService;
import com.huanxing.cloud.upms.common.entity.SysUser;
import com.huanxing.cloud.upms.common.feign.FeignSysUserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
/**
 * 实现类
 *
 * @author lijx
 * @date 2022/6/10
 */
@Service
@AllArgsConstructor
public class HuanxingUserDetailsServiceImpl implements HuanxingUserDetailsService {
  private final FeignSysUserService feignSysUserService;

  @Override
  public SysUser loadUserByUsername(String username) {

    Result<SysUser> sysUserR =
        feignSysUserService.getUserInfo(username, SecurityConstants.SOURCE_IN);
    if (!sysUserR.isOk()) {
      throw new RuntimeException(sysUserR.getMsg());
    }
    return sysUserR.getData();
  }

  @Override
  public SysUser loadUserByPhone(String phone) {
    Result<SysUser> sysUserR = feignSysUserService.getUserInfoByPhone(phone, SecurityConstants.SOURCE_IN);
    if (!sysUserR.isOk()) {
      throw new RuntimeException(sysUserR.getMsg());
    }
    return sysUserR.getData();
  }
}
