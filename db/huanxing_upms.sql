/*
 Navicat Premium Data Transfer

 Source Server         : 环兴商城
 Source Server Type    : MySQL
 Source Server Version : 80018
 Source Schema         : huanxing_upms

 Target Server Type    : MySQL
 Target Server Version : 80018
 File Encoding         : 65001

 Date: 30/07/2022 13:26:43
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
                             `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'PK',
                             `parent_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '父级id',
                             `dept_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '部门名称',
                             `leader` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '负责人',
                             `leader_phone` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '负责人联系电话',
                             `sort` int(11) NULL DEFAULT NULL COMMENT '排序序号',
                             `enable_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '启用状态：0.否；1.是；',
                             `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                             `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                             `update_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
                             `create_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建ID',
                             `update_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改ID',
                             PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '部门' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES ('1', '0', '环兴科技有限公司', 'lijx', '17615123397', 1, '1', '0', '2022-02-18 17:46:40', '2022-02-18 17:46:42', NULL, NULL);
INSERT INTO `sys_dept` VALUES ('1494674952704667649', '1', '销售部门', '李家兴', NULL, 2, '1', '0', '2022-02-18 22:07:34', NULL, NULL, NULL);
INSERT INTO `sys_dept` VALUES ('2', '1', '开发部门', 'menglt', '17615123398', 1, '1', '0', '2022-02-18 17:53:18', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log`  (
                            `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'PK',
                            `ip_addr` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'ip地址',
                            `status` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '状态：0.失败；1.成功；',
                            `create_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建ID',
                            `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '新增时间',
                            `location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '登录地点',
                            `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '登录用户',
                            `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '日志标题',
                            `request_method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '请求方式',
                            `request_uri` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '请求URI',
                            `request_params` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '请求数据',
                            `request_time` bigint(20) NULL DEFAULT NULL COMMENT '请求时长',
                            `ex_msg` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '异常信息',
                            `method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '操作方法',
                            `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                            PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '操作日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_login_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_login_log`;
CREATE TABLE `sys_login_log`  (
                                  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'PK',
                                  `ip_addr` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'ip地址',
                                  `status` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '状态：0.失败；1.成功；',
                                  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '登录用户',
                                  `location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '登录地点',
                                  `create_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建ID',
                                  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                                  `msg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '信息',
                                  `browser` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '浏览器',
                                  `os` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '操作系统',
                                  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                                  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '登录日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
                             `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'PK',
                             `name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '菜单名称',
                             `permission` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '菜单权限',
                             `path` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'URL',
                             `parent_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '父菜单ID',
                             `icon` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '图标',
                             `component` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '页面地址',
                             `sort` int(11) NOT NULL DEFAULT 0 COMMENT '排序',
                             `type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '类型: 0.菜单; 1.按钮;',
                             `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                             `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
                             `is_del` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                             `outer_status` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '外链状态：0.否；1.是；',
                             `del_flag` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                             PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '菜单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('100001', '菜单管理新增', 'upms:sysmenu:add', NULL, '10002', NULL, '', 0, '1', '2021-12-01 09:44:37', '2022-07-18 14:29:52', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('10001', '系统管理', NULL, '/system', '0', 'icon-xitong', 'Layout', 10, '0', '2021-11-26 11:38:57', '2022-07-18 14:29:52', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('10002', '菜单管理', NULL, '/menu', '10001', 'icon-caidanguanli', 'views/upms/menu/index', 1, '0', '2021-11-26 11:37:40', '2022-07-18 14:29:52', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('10003', '关于', NULL, '/about', '10001', 'icon-guanyuwomen', 'views/upms/about/index', 999, '0', '2022-02-10 13:39:18', '2022-07-18 14:29:52', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1491663510904893441', 'wangEditor', NULL, '/editor', '10001', 'icon-m-fuwenben', 'views/util/editor', 2, '0', '2022-02-10 14:41:09', '2022-07-18 14:29:53', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1491684226094198786', '角色管理', NULL, '/role', '10001', 'icon-navicon-zhgl', 'views/upms/role/index', 1, '0', '2022-02-10 16:03:27', '2022-07-18 14:29:53', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1491690996678021121', '角色管理列表', 'upms:sysrole:page', NULL, '1491684226094198786', '', NULL, 1, '1', '2022-02-10 16:30:21', '2022-07-18 14:29:53', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1491752531735490561', '用户管理', NULL, '/user', '10001', 'icon-yonghuguanli', 'views/upms/user/index', 2, '0', '2022-02-10 20:34:54', '2022-07-18 14:29:53', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1491756888363307009', '用户列表', 'upms:sysuser:page', NULL, '1491752531735490561', '', NULL, 1, '1', '2022-02-10 20:52:13', '2022-07-18 14:29:53', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1491757020773289986', '用户查询', 'upms:sysuser:get', NULL, '1491752531735490561', '', NULL, 1, '1', '2022-02-10 20:52:44', '2022-07-18 14:29:53', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1491757382771085313', '用户新增', 'upms:sysuser:add', NULL, '1491752531735490561', '', NULL, 1, '1', '2022-02-10 20:54:11', '2022-07-18 14:29:53', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1491948958826921986', '文件存储配置', NULL, '/storageconfig', '10001', 'icon-caidanguanli1', 'views/upms/storageconfig/index', 4, '0', '2022-02-11 09:35:25', '2022-07-18 14:29:53', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1491949315883827201', '文件存储配置查询', 'upms:storageconfig:get', NULL, '1491948958826921986', '', NULL, 1, '1', '2022-02-11 09:36:50', '2022-07-18 14:29:53', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1491969633293729794', '文件存储配置修改', 'upms:storageconfig:edit', NULL, '1491948958826921986', '', NULL, 1, '1', '2022-02-11 10:57:34', '2022-07-18 14:29:53', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1491973212968632322', '素材中心', NULL, '/material', '1493584727114936322', 'icon-duosucai', 'views/mall/material/index', 50, '0', '2022-02-11 11:11:47', '2022-07-18 14:29:53', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1493578977630121986', '素材中心列表', 'mall:material:page', NULL, '1491973212968632322', NULL, NULL, 1, '1', '2022-02-15 21:32:38', '2022-07-18 14:29:53', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1493578977978249218', '素材中心查询', 'mall:material:get', NULL, '1491973212968632322', NULL, NULL, 1, '1', '2022-02-15 21:32:38', '2022-07-18 14:29:54', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1493578978313793537', '素材中心添加', 'mall:material:add', NULL, '1491973212968632322', NULL, NULL, 1, '1', '2022-02-15 21:32:38', '2022-07-18 14:29:54', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1493578978649337858', '素材中心修改', 'mall:material:edit', NULL, '1491973212968632322', NULL, NULL, 1, '1', '2022-02-15 21:32:38', '2022-07-18 14:29:54', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1493584727114936322', '商城管理', NULL, '/mall', '0', 'icon-dianpu', 'Layout', 0, '0', '2022-02-15 21:55:29', '2022-07-18 14:29:54', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1493587429257539586', '商城商品', NULL, '/goods', '1493584727114936322', 'icon-dianpu', 'Layout', 1, '0', '2022-02-15 22:06:07', '2022-07-18 14:29:54', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1493587910381957121', '商品管理查询', 'mall:goodsspu:get', NULL, '1532620395988029442', NULL, NULL, 1, '1', '2022-02-15 22:08:02', '2022-07-18 14:29:54', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1493587910721695745', '商品管理添加', 'mall:goodsspu:add', NULL, '1532620395988029442', NULL, NULL, 1, '1', '2022-02-15 22:08:02', '2022-07-18 14:29:54', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1493587911057240066', '商品管理修改', 'mall:goodsspu:edit', NULL, '1532620395988029442', NULL, NULL, 1, '1', '2022-02-15 22:08:02', '2022-07-18 14:29:54', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1493596733666652162', '素材分组查询', 'mall:materialgroup:get', NULL, '1491973212968632322', '', NULL, 2, '1', '2022-02-15 22:43:05', '2022-07-18 14:29:54', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1493766204259942401', '素材分组新增', 'mall:materialgroup:add', NULL, '1491973212968632322', '', NULL, 2, '1', '2022-02-16 09:56:27', '2022-07-18 14:29:54', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1493836091183411202', '角色管理查询', 'upms:sysrole:get', NULL, '1491684226094198786', '', NULL, 1, '1', '2022-02-16 14:34:10', '2022-07-18 14:29:54', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1493836209106268161', '角色管理新增', 'upms:sysrole:add', NULL, '1491684226094198786', '', NULL, 1, '1', '2022-02-16 14:34:38', '2022-07-18 14:29:54', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1493836280589791233', '角色管理修改', 'upms:sysrole:edit', NULL, '1491684226094198786', '', NULL, 1, '1', '2022-02-16 14:34:55', '2022-07-18 14:29:55', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1493841029473042434', '角色管理删除', 'upms:sysrole:del', NULL, '1491684226094198786', '', NULL, 1, '1', '2022-02-16 14:53:47', '2022-07-18 14:29:55', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1493884088730529793', '日志管理', NULL, '/log', '10001', 'icon-caidanguanli', 'Layout', 20, '0', '2022-02-16 17:44:53', '2022-07-18 14:29:55', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1493884379760701442', '登录日志', NULL, '/loginlog', '1493884088730529793', 'icon-caidanguanli', 'views/upms/loginlog/index', 1, '0', '2022-02-16 17:46:02', '2022-07-18 14:29:55', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1494151991157673985', '操作日志', NULL, '/log', '1493884088730529793', 'icon-caidanguanli', 'views/upms/log/index', 20, '0', '2022-02-17 11:29:28', '2022-07-18 14:29:55', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1494153372996255746', '操作日志列表', 'upms:syslog:page', NULL, '1494151991157673985', NULL, NULL, 1, '1', '2022-02-17 11:34:57', '2022-07-18 14:29:55', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1494153373352771586', '操作日志查询', 'upms:syslog:get', NULL, '1494151991157673985', NULL, NULL, 1, '1', '2022-02-17 11:34:58', '2022-07-18 14:29:55', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1494153373696704514', '操作日志新增', 'upms:syslog:add', NULL, '1494151991157673985', NULL, NULL, 1, '1', '2022-02-17 11:34:58', '2022-07-18 14:29:55', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1494153374053220354', '操作日志修改', 'upms:syslog:edit', NULL, '1494151991157673985', NULL, NULL, 1, '1', '2022-02-17 11:34:58', '2022-07-18 14:29:55', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1494153374401347585', '操作日志删除', 'upms:syslog:del', NULL, '1494151991157673985', NULL, NULL, 1, '1', '2022-02-17 11:34:58', '2022-07-18 14:29:55', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1494192758630694913', '登录日志列表', 'upms:sysloginlog:page', NULL, '1493884379760701442', NULL, NULL, 1, '1', '2022-02-17 14:11:28', '2022-07-18 14:29:55', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1494192758974627842', '登录日志查询', 'upms:sysloginlog:get', NULL, '1493884379760701442', NULL, NULL, 1, '1', '2022-02-17 14:11:28', '2022-07-18 14:29:55', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1494192759318560770', '登录日志新增', 'upms:sysloginlog:add', NULL, '1493884379760701442', NULL, NULL, 1, '1', '2022-02-17 14:11:28', '2022-07-18 14:29:56', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1494192759662493698', '登录日志修改', 'upms:sysloginlog:edit', NULL, '1493884379760701442', NULL, NULL, 1, '1', '2022-02-17 14:11:28', '2022-07-18 14:29:56', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1494192760010620930', '登录日志删除', 'upms:sysloginlog:del', NULL, '1493884379760701442', NULL, NULL, 1, '1', '2022-02-17 14:11:28', '2022-07-18 14:29:56', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1494216988311183361', '部门管理', NULL, '/dept', '10001', 'icon-yonghuguanli', 'views/upms/dept/index', 10, '0', '2022-02-17 15:47:45', '2022-07-18 14:29:56', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1494217080162246658', '部门管理列表', 'upms:sysdept:page', NULL, '1494216988311183361', NULL, NULL, 1, '1', '2022-02-17 15:48:06', '2022-07-18 14:29:56', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1494217080510373890', '部门管理查询', 'upms:sysdept:get', NULL, '1494216988311183361', NULL, NULL, 1, '1', '2022-02-17 15:48:06', '2022-07-18 14:29:56', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1494217080858501121', '部门管理新增', 'upms:sysdept:add', NULL, '1494216988311183361', NULL, NULL, 1, '1', '2022-02-17 15:48:06', '2022-07-18 14:29:56', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1494217081206628354', '部门管理修改', 'upms:sysdept:edit', NULL, '1494216988311183361', NULL, NULL, 1, '1', '2022-02-17 15:48:06', '2022-07-18 14:29:56', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1494217081558949890', '部门管理删除', 'upms:sysdept:del', NULL, '1494216988311183361', NULL, NULL, 1, '1', '2022-02-17 15:48:07', '2022-07-18 14:29:56', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1494514119857180674', '用户修改', 'upms:sysuser:edit', NULL, '1491752531735490561', '', NULL, 1, '1', '2022-02-18 11:28:25', '2022-07-18 14:29:56', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1495687621054353410', '菜单管理修改', 'upms:sysmenu:edit', NULL, '10002', '', NULL, 1, '1', '2022-02-21 17:11:31', '2022-07-18 14:29:56', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1495687858816864257', '菜单管理删除', 'upms:sysmenu:del', NULL, '10002', '', NULL, 1, '1', '2022-02-21 17:12:28', '2022-07-18 14:29:56', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1496012487833960450', '商品管理删除', 'mall:goodsspu:del', NULL, '1532620395988029442', '', NULL, 1, '1', '2022-02-22 14:42:24', '2022-07-18 14:29:57', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1496327483721789441', '商品类目', NULL, '/goodscategory', '1493584727114936322', 'icon-rizhi', 'views/mall/goodscategory/index', 10, '0', '2022-02-23 11:34:04', '2022-07-18 14:29:57', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1496327552973942785', '商品类目列表', 'mall:goodscategory:page', NULL, '1496327483721789441', NULL, NULL, 1, '1', '2022-02-23 11:34:20', '2022-07-18 14:29:57', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1496327553334652930', '商品类目查询', 'mall:goodscategory:get', NULL, '1496327483721789441', NULL, NULL, 1, '1', '2022-02-23 11:34:21', '2022-07-18 14:29:57', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1496327553699557377', '商品类目新增', 'mall:goodscategory:add', NULL, '1496327483721789441', NULL, NULL, 1, '1', '2022-02-23 11:34:21', '2022-07-18 14:29:57', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1496327554068656130', '商品类目修改', 'mall:goodscategory:edit', NULL, '1496327483721789441', NULL, NULL, 1, '1', '2022-02-23 11:34:21', '2022-07-18 14:29:57', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1496327554433560577', '商品类目删除', 'mall:goodscategory:del', NULL, '1496327483721789441', NULL, NULL, 1, '1', '2022-02-23 11:34:21', '2022-07-18 14:29:57', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1496327802522447873', '运费模板', NULL, '/freighttemplate', '1493584727114936322', 'icon-caidanguanli', 'views/mall/freighttemplate/index', 15, '0', '2022-02-23 11:35:20', '2022-07-18 14:29:57', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1496327860647112706', '运费模板列表', 'mall:freighttemplate:page', NULL, '1496327802522447873', NULL, NULL, 1, '1', '2022-02-23 11:35:34', '2022-07-18 14:29:57', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1496327861003628545', '运费模板查询', 'mall:freighttemplate:get', NULL, '1496327802522447873', NULL, NULL, 1, '1', '2022-02-23 11:35:34', '2022-07-18 14:29:57', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1496327861351755778', '运费模板新增', 'mall:freighttemplate:add', NULL, '1496327802522447873', NULL, NULL, 1, '1', '2022-02-23 11:35:34', '2022-07-18 14:29:57', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1496327861712465921', '运费模板修改', 'mall:freighttemplate:edit', NULL, '1496327802522447873', NULL, NULL, 1, '1', '2022-02-23 11:35:34', '2022-07-18 14:29:57', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1496327862068981762', '运费模板删除', 'mall:freighttemplate:del', NULL, '1496327802522447873', NULL, NULL, 1, '1', '2022-02-23 11:35:34', '2022-07-18 14:29:58', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1497468294740176898', '商品管理列表', 'mall:goodsspu:page', NULL, '1532620395988029442', '', NULL, 1, '1', '2022-02-26 15:07:17', '2022-07-18 14:29:58', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1521469373525716994', '菜单管理列表', 'upms:sysmenu:page', NULL, '10002', '', NULL, 1, '1', '2022-05-03 20:39:03', '2022-07-18 14:29:58', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1521496866882236418', '订单管理', NULL, '/order', '1493584727114936322', 'icon-caidanguanli', 'Layout', 60, '0', '2022-05-03 22:28:18', '2022-07-18 14:29:58', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1526179827628048385', '素材中心删除', 'mall:material:del', NULL, '1491973212968632322', 'icon-m-fuwenben', NULL, 1, '1', '2022-05-16 20:36:42', '2022-07-18 14:29:58', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1527471479688798209', '商城用户', NULL, '/userinfo', '1493584727114936322', 'icon-yonghuguanli', 'views/mall/userinfo/index', 1, '0', '2022-05-20 10:09:14', '2022-07-18 14:29:58', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1527471918001954818', '商城用户列表', 'mall:userinfo:page', NULL, '1527471479688798209', NULL, NULL, 1, '1', '2022-05-20 10:10:58', '2022-07-18 14:29:58', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1527471918337499138', '商城用户查询', 'mall:userinfo:get', NULL, '1527471479688798209', NULL, NULL, 1, '1', '2022-05-20 10:10:58', '2022-07-18 14:29:58', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1527471918694014977', '商城用户新增', 'mall:userinfo:add', NULL, '1527471479688798209', NULL, NULL, 1, '1', '2022-05-20 10:10:58', '2022-07-18 14:29:58', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1527471919042142209', '商城用户修改', 'mall:userinfo:edit', NULL, '1527471479688798209', NULL, NULL, 1, '1', '2022-05-20 10:10:58', '2022-07-18 14:29:58', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1527471919386075137', '商城用户删除', 'mall:userinfo:del', NULL, '1527471479688798209', NULL, NULL, 1, '1', '2022-05-20 10:10:58', '2022-07-18 14:29:58', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1527835787455164418', '商城装修', NULL, '/theme', '1493584727114936322', 'icon-xitong', 'Layout', 70, '0', '2022-05-21 10:16:50', '2022-07-18 14:29:58', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1527835963171336193', '主题设置', NULL, '/mobiletheme', '1527835787455164418', 'icon-xitong', 'views/mall/mobiletheme/index', 1, '0', '2022-05-21 10:17:32', '2022-07-18 14:29:59', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1527836009900077057', '商城装修列表', 'mall:mobiletheme:page', NULL, '1527835787455164418', NULL, NULL, 1, '1', '2022-05-21 10:17:43', '2022-07-18 14:29:59', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1527836010248204290', '商城装修查询', 'mall:mobiletheme:get', NULL, '1527835787455164418', NULL, NULL, 1, '1', '2022-05-21 10:17:43', '2022-07-18 14:29:59', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1527836010596331522', '商城装修新增', 'mall:mobiletheme:add', NULL, '1527835787455164418', NULL, NULL, 1, '1', '2022-05-21 10:17:43', '2022-07-18 14:29:59', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1527836010944458754', '商城装修修改', 'mall:mobiletheme:edit', NULL, '1527835787455164418', NULL, NULL, 1, '1', '2022-05-21 10:17:43', '2022-07-18 14:29:59', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1527836011300974593', '商城装修删除', 'mall:mobiletheme:del', NULL, '1527835787455164418', NULL, NULL, 1, '1', '2022-05-21 10:17:43', '2022-07-18 14:29:59', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1527928859455168514', '服务监控', NULL, '/server', '10001', 'icon-pingtaiguanli', 'views/upms/sysserver/index', 25, '0', '2022-05-21 16:26:40', '2022-07-18 14:29:59', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1527947365856878593', '服务监控查询', 'upms:sysserver:get', NULL, '1527928859455168514', '', NULL, 1, '1', '2022-05-21 17:40:14', '2022-07-18 14:29:59', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1531445896986435585', '支付配置', NULL, '/payconfig', '10001', 'icon-xitong', 'views/payapi/payconfig/index', 8, '0', '2022-05-31 09:22:09', '2022-07-18 14:29:59', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1531446009649635329', '支付配置列表', 'payapi:payconfig:page', NULL, '1531445896986435585', NULL, NULL, 1, '1', '2022-05-31 09:22:36', '2022-07-18 14:29:59', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1531446009779658754', '支付配置查询', 'payapi:payconfig:get', NULL, '1531445896986435585', NULL, NULL, 1, '1', '2022-05-31 09:22:36', '2022-07-18 14:29:59', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1531446009913876482', '支付配置新增', 'payapi:payconfig:add', NULL, '1531445896986435585', NULL, NULL, 1, '1', '2022-05-31 09:22:36', '2022-07-18 14:29:59', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1531446010039705602', '支付配置修改', 'payapi:payconfig:edit', NULL, '1531445896986435585', NULL, NULL, 1, '1', '2022-05-31 09:22:36', '2022-07-18 14:30:00', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1531446010169729026', '支付配置删除', 'payapi:payconfig:del', NULL, '1531445896986435585', NULL, NULL, 1, '1', '2022-05-31 09:22:36', '2022-07-18 14:30:00', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1531528760525074434', '商城订单', NULL, '/orderinfo', '1521496866882236418', 'icon-caidanguanli', 'views/mall/orderinfo/index', 10, '0', '2022-05-31 14:51:23', '2022-07-18 14:30:00', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1531529196871102466', '商城退单', NULL, '/orderrefund', '1521496866882236418', 'icon-caidanguanli', 'views/mall/orderrefund/index', 20, '0', '2022-05-31 14:53:07', '2022-07-18 14:30:00', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1531536449854517250', '商城订单列表', 'mall:orderinfo:page', NULL, '1531528760525074434', '', NULL, 1, '1', '2022-05-31 15:21:57', '2022-07-18 14:30:00', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1531536545572728833', '商城订单查询', 'mall:orderinfo:get', NULL, '1531528760525074434', '', NULL, 1, '1', '2022-05-31 15:22:19', '2022-07-18 14:30:00', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1531536746446336001', '商城订单删除', 'mall:orderinfo:del', NULL, '1531528760525074434', '', NULL, 1, '1', '2022-05-31 15:23:07', '2022-07-18 14:30:00', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1531536866638311426', '商城订单发货', 'mall:orderinfo:deliver', NULL, '1531528760525074434', '', NULL, 1, '1', '2022-05-31 15:23:36', '2022-07-18 14:30:00', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1531537172243689474', '商城退单列表', 'mall:orderrefund:page', NULL, '1531529196871102466', '', NULL, 1, '1', '2022-05-31 15:24:49', '2022-07-18 14:30:00', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1531537289042472961', '商城退单查询', 'mall:orderrefund:get', NULL, '1531529196871102466', '', NULL, 1, '1', '2022-05-31 15:25:17', '2022-07-18 14:30:00', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1531846396944785409', '微信管理', NULL, '/weixin', '1539129183310196738', 'icon-check', 'Layout', 30, '0', '2022-06-01 11:53:36', '2022-07-18 14:30:00', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1531846725585281025', '微信用户', NULL, '/wxuser', '1531846396944785409', 'icon-yonghuguanli', 'views/miniapp/weixin/wxuser/index', 10, '0', '2022-06-01 11:54:54', '2022-07-18 14:30:00', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1531846893999169537', '微信应用', NULL, '/wxapp', '1531846396944785409', 'icon-shouye', 'views/miniapp/weixin/wxapp/index', 5, '0', '2022-06-01 11:55:34', '2022-07-18 14:30:01', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1531882726605758465', '微信用户列表', 'weixin:wxuser:page', NULL, '1531846725585281025', '', NULL, 1, '1', '2022-06-01 14:17:54', '2022-07-18 14:30:01', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1531882841361915906', '微信应用列表', 'weixin:wxapp:page', NULL, '1531846893999169537', '', NULL, 1, '1', '2022-06-01 14:18:22', '2022-07-18 14:30:01', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1531976380791881730', '0元夺宝', NULL, 'loot', '1493584727114936322', 'icon-dianpu', 'Layout', 80, '0', '2022-06-01 20:30:08', '2022-07-18 14:30:01', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1531976538732593154', '夺宝配置', NULL, '/lootinfo', '1531976380791881730', 'icon-jiaoseguanli', 'views/mall/lootinfo/index', 1, '0', '2022-06-01 20:30:46', '2022-07-18 14:30:01', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1531976761387220994', '夺宝记录', NULL, '/lootuser', '1531976380791881730', 'icon-caidanguanli', 'views/mall/lootuser/index', 10, '0', '2022-06-01 20:31:39', '2022-07-18 14:30:01', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1531977016149245953', '夺宝期', NULL, '/lootstage', '1531976380791881730', 'icon-m-fuwenben', 'views/mall/lootstage/index', 5, '0', '2022-06-01 20:32:40', '2022-07-18 14:30:01', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1531977159263092738', '夺宝配置列表', 'mall:lootinfo:page', NULL, '1531976538732593154', '', NULL, 1, '1', '2022-06-01 20:33:14', '2022-07-18 14:30:01', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1531977248807288834', '夺宝配置查询', 'mall:lootinfo:get', NULL, '1531976538732593154', '', NULL, 1, '1', '2022-06-01 20:33:35', '2022-07-18 14:30:01', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1531977369196396545', '夺宝配置新增', 'mall:lootinfo:add', NULL, '1531976538732593154', '', NULL, 1, '1', '2022-06-01 20:34:04', '2022-07-18 14:30:01', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1531977444341547010', '夺宝配置删除', 'mall:lootinfo:del', NULL, '1531976538732593154', '', NULL, 1, '1', '2022-06-01 20:34:22', '2022-07-18 14:30:01', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1531977518870134785', '夺宝配置删除', 'mall:lootinfo:edit', NULL, '1531976538732593154', '', NULL, 1, '1', '2022-06-01 20:34:39', '2022-07-18 14:30:01', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1532620101065543681', '商品规格', NULL, '/goodsspecs', '1493587429257539586', 'icon-xitong', 'views/mall/goodsspecs/index', 20, '0', '2022-06-03 15:08:01', '2022-07-18 14:30:02', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1532620395988029442', '商品管理', NULL, '/goodsspu', '1493587429257539586', 'icon-shouye1', 'views/mall/goodsspu/index', 10, '0', '2022-06-03 15:09:11', '2022-07-18 14:30:02', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1532620840659750914', '商品规格列表', 'mall:goodsspecs:page', NULL, '1532620101065543681', '', NULL, 1, '1', '2022-06-03 15:10:57', '2022-07-18 14:30:02', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1532620931885862913', '商品规格查询', 'mall:goodsspecs:get', '', '1532620101065543681', '', NULL, 1, '1', '2022-06-03 15:11:19', '2022-07-18 14:30:02', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1532621007333003266', '商品规格新增', 'mall:goodsspecs:add', NULL, '1532620101065543681', '', NULL, 1, '1', '2022-06-03 15:11:37', '2022-07-18 14:30:02', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1532621107589451778', '商品规格修改', 'mall:goodsspecs:edit', NULL, '1532620101065543681', '', NULL, 1, '1', '2022-06-03 15:12:01', '2022-07-18 14:30:02', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1532621196013768706', '商品规格删除', 'mall:goodsspecs:del', NULL, '1532620101065543681', '', NULL, 1, '1', '2022-06-03 15:12:22', '2022-07-18 14:30:02', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1532634358100430850', '商品规格值列表', 'mall:goodsspecsvalue:page', NULL, '1532620101065543681', '', NULL, 2, '1', '2022-06-03 16:04:40', '2022-07-18 14:30:02', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1532634435510505473', '商品规格值查询', 'mall:goodsspecsvalue:get', NULL, '1532620101065543681', '', NULL, 2, '1', '2022-06-03 16:04:59', '2022-07-18 14:30:02', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1532634517374930946', '商品规格值新增', 'mall:goodsspecsvalue:add', NULL, '1532620101065543681', '', NULL, 2, '1', '2022-06-03 16:05:18', '2022-07-18 14:30:02', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1532634586568364034', '商品规格值修改', 'mall:goodsspecsvalue:edit', NULL, '1532620101065543681', '', NULL, 2, '1', '2022-06-03 16:05:35', '2022-07-18 14:30:02', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1532634687902748674', '商品规格值删除', 'mall:goodsspecsvaluedel', NULL, '1532620101065543681', 'icon-ziti', NULL, 2, '1', '2022-06-03 16:05:59', '2022-07-18 14:30:02', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1535633963410956289', '物流信息查询', 'mall:orderLogistics:get', NULL, '1531528760525074434', '', NULL, 1, '1', '2022-06-11 22:44:03', '2022-07-18 14:30:03', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1536174109680214017', '商城配置', NULL, '/mallconfig', '1493584727114936322', 'icon-xitong', 'views/mall/mallconfig/index', 90, '0', '2022-06-13 10:30:21', '2022-07-18 14:30:03', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1536174316656533505', '物流公司', NULL, '/logisticscompanyconfig', '1493584727114936322', 'icon-xitong', 'views/mall/logisticscompanyconfig/index', 100, '0', '2022-06-13 10:31:10', '2022-07-18 14:30:03', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1536174413406543874', '物流公司查询', 'mall:logisticscompanyconfig:get', NULL, '1536174316656533505', '', NULL, 1, '1', '2022-06-13 10:31:33', '2022-07-18 14:30:03', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1536174567983423489', '商城配置查询', 'mall:mallconfig:get', NULL, '1536174109680214017', '', NULL, 1, '1', '2022-06-13 10:32:10', '2022-07-18 14:30:03', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1537048076783616001', '素材分组列表', 'mall:materialgroup:page', NULL, '1491973212968632322', '', NULL, 2, '1', '2022-06-15 20:23:13', '2022-07-18 14:30:03', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1537066492991012865', '素材分组修改', 'mall:materialgroup:edit', NULL, '1491973212968632322', '', NULL, 2, '1', '2022-06-15 21:36:23', '2022-07-18 14:30:03', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1537066557067395074', '素材分组删除', 'mall:materialgroup:del', NULL, '1491973212968632322', '', NULL, 2, '1', '2022-06-15 21:36:39', '2022-07-18 14:30:03', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1539129183310196738', '小程序管理', NULL, '/miniapp', '0', 'icon-pingtaiguanli', 'Layout', 100, '0', '2022-06-21 14:12:46', '2022-07-18 14:30:03', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1539130386861535234', '支付宝管理', NULL, '/alipay', '1539129183310196738', 'icon-dianpu', 'Layout', 50, '0', '2022-06-21 14:17:33', '2022-07-18 14:30:03', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1539130685290459138', '支付宝应用', NULL, '/alipayapp', '1539130386861535234', 'icon-xitong', 'views/miniapp/alipay/alipayapp/index', 10, '0', '2022-06-21 14:18:44', '2022-07-18 14:30:03', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1539130909853495297', '支付宝用户', NULL, '/alipayuser', '1539130386861535234', 'icon-navicon-zhgl', 'views/miniapp/alipay/alipayuser/index', 20, '0', '2022-06-21 14:19:38', '2022-07-18 14:30:03', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1539614105648947202', '支付宝应用列表', 'alipay:alipayapp:page', NULL, '1539130685290459138', '', NULL, 1, '1', '2022-06-22 22:19:43', '2022-07-18 14:30:04', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1539614296187789314', '支付宝用户列表', 'alipay:alipayuser:page', NULL, '1539130909853495297', '', NULL, 1, '1', '2022-06-22 22:20:28', '2022-07-18 14:30:04', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1543116535774879745', '商城退单退款', 'mall:orderrefund:refund', NULL, '1531529196871102466', '', NULL, 1, '1', '2022-07-02 14:17:06', '2022-07-18 14:30:04', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1546742598279147522', '分销管理', NULL, '/distribution', '1493584727114936322', 'icon-xitong', 'Layout', 40, '0', '2022-07-12 14:25:47', '2022-07-18 14:30:04', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1546742814403244033', '分销配置', NULL, '/distributionconfig', '1546742598279147522', 'icon-xitong', 'views/mall/distributionconfig/index', 1, '0', '2022-07-12 14:26:38', '2022-07-18 14:30:04', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1546742996209545218', '分销配置查询', 'mall:distributionconfig:get', NULL, '1546742814403244033', '', NULL, 1, '1', '2022-07-12 14:27:21', '2022-07-18 14:30:04', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1546743121774424065', '分销配置修改', 'mall:distributionconfig:edit', NULL, '1546742814403244033', '', NULL, 1, '1', '2022-07-12 14:27:51', '2022-07-18 14:30:04', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1546752891075997698', '分销员', NULL, '/distributionuser', '1546742598279147522', 'icon-navicon-zhgl', 'views/mall/distributionuser/index', 5, '0', '2022-07-12 15:06:41', '2022-07-18 14:30:04', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1546753066938970114', '分销员列表', 'mall:distributionuser:page', NULL, '1546752891075997698', '', NULL, 1, '1', '2022-07-12 15:07:22', '2022-07-18 14:30:04', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1546775469337391105', '分销订单', NULL, '/distributionorder', '1546742598279147522', 'icon-caidanguanli', 'views/mall/distributionorder/index', 4, '0', '2022-07-12 16:36:24', '2022-07-18 14:30:04', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1546775617249521665', '分销订单列表', 'mall:distributionorder:page', NULL, '1546775469337391105', '', NULL, 1, '1', '2022-07-12 16:36:59', '2022-07-18 14:30:04', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1547110946242326529', '用户修改密码', 'upms:sysuser:password', NULL, '1491752531735490561', '', NULL, 1, '1', '2022-07-13 14:49:27', '2022-07-18 14:30:04', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1547111164111253505', '用户删除', 'upms:sysuser:del', NULL, '1491752531735490561', '', NULL, 1, '1', '2022-07-13 14:50:19', '2022-07-18 14:30:05', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1548148638459703297', '令牌管理', NULL, '/token', '10001', 'icon-yonghuguanli', 'views/upms/token/index', 30, '0', '2022-07-16 11:32:51', '2022-07-18 14:30:05', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1548845472747827202', 'Nacos配置中心', NULL, 'nacos', '10001', 'icon-xitong', 'http://59.110.30.161:8848/nacos', 40, '0', '2022-07-18 09:41:51', '2022-07-18 14:30:05', '0', '1', '0');
INSERT INTO `sys_menu` VALUES ('1548913625646022657', '环兴商城文档', NULL, 'guide', '10001', 'icon-rizhi', 'http://www.huanxing.shop', 100, '0', '2022-07-18 14:12:40', '2022-07-18 15:03:24', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1549359604316659713', '任务调度中心', NULL, 'xxl-job-admin', '10001', 'icon-m-fuwenben', 'http://59.110.30.161:7002/xxl-job-admin', 50, '0', '2022-07-19 19:44:52', '2022-07-19 19:44:52', '0', '1', '0');
INSERT INTO `sys_menu` VALUES ('1549383195510874114', '绑定上级用户', 'mall:userinfo:bind', NULL, '1527471479688798209', '', NULL, 1, '1', '2022-07-19 21:18:34', '2022-07-19 21:18:34', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('1549383285281562625', '解绑上级用户', 'mall:userinfo:unbind', NULL, '1527471479688798209', '', NULL, 1, '1', '2022-07-19 21:18:56', '2022-07-19 21:18:56', '0', '0', '0');
INSERT INTO `sys_menu` VALUES ('999999999999', '菜单管理查询', 'upms:sysmenu:get', NULL, '10002', NULL, NULL, 1, '1', '2022-02-21 16:11:30', '2022-07-18 14:30:05', '0', '0', '0');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
                             `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'PK',
                             `role_name` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色名称',
                             `role_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色编码',
                             `role_desc` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '角色描述',
                             `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                             `update_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
                             `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                             PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '超级管理员', 'ROLE_ADMIN', '系统管理员拥有全部权限', '2021-11-26 11:34:48', '2021-09-01 11:34:48', '0');
INSERT INTO `sys_role` VALUES ('1494300145462980609', '开发者', 'ROLE_DEVELOPER', '开发者', '2022-02-17 21:18:12', NULL, '0');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
                                  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'PK',
                                  `role_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色ID',
                                  `menu_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '菜单ID',
                                  `create_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                                  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色关联菜单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('1546794405654573057', '1', '1493584727114936322', '2022-07-12 17:51:39');
INSERT INTO `sys_role_menu` VALUES ('1546794405776207874', '1', '1493587429257539586', '2022-07-12 17:51:39');
INSERT INTO `sys_role_menu` VALUES ('1546794405897842689', '1', '1532620395988029442', '2022-07-12 17:51:39');
INSERT INTO `sys_role_menu` VALUES ('1546794406019477506', '1', '1493587910381957121', '2022-07-12 17:51:39');
INSERT INTO `sys_role_menu` VALUES ('1546794406136918018', '1', '1493587910721695745', '2022-07-12 17:51:39');
INSERT INTO `sys_role_menu` VALUES ('1546794406254358529', '1', '1493587911057240066', '2022-07-12 17:51:39');
INSERT INTO `sys_role_menu` VALUES ('1546794406371799041', '1', '1496012487833960450', '2022-07-12 17:51:39');
INSERT INTO `sys_role_menu` VALUES ('1546794406493433857', '1', '1497468294740176898', '2022-07-12 17:51:39');
INSERT INTO `sys_role_menu` VALUES ('1546794406610874369', '1', '1532620101065543681', '2022-07-12 17:51:39');
INSERT INTO `sys_role_menu` VALUES ('1546794406728314881', '1', '1532620840659750914', '2022-07-12 17:51:39');
INSERT INTO `sys_role_menu` VALUES ('1546794406849949697', '1', '1532620931885862913', '2022-07-12 17:51:39');
INSERT INTO `sys_role_menu` VALUES ('1546794406967390209', '1', '1532621007333003266', '2022-07-12 17:51:39');
INSERT INTO `sys_role_menu` VALUES ('1546794407084830722', '1', '1532621107589451778', '2022-07-12 17:51:39');
INSERT INTO `sys_role_menu` VALUES ('1546794407206465538', '1', '1532621196013768706', '2022-07-12 17:51:39');
INSERT INTO `sys_role_menu` VALUES ('1546794407323906049', '1', '1532634358100430850', '2022-07-12 17:51:39');
INSERT INTO `sys_role_menu` VALUES ('1546794407441346562', '1', '1532634435510505473', '2022-07-12 17:51:39');
INSERT INTO `sys_role_menu` VALUES ('1546794407562981378', '1', '1532634517374930946', '2022-07-12 17:51:39');
INSERT INTO `sys_role_menu` VALUES ('1546794407680421889', '1', '1532634586568364034', '2022-07-12 17:51:39');
INSERT INTO `sys_role_menu` VALUES ('1546794407797862401', '1', '1532634687902748674', '2022-07-12 17:51:39');
INSERT INTO `sys_role_menu` VALUES ('1546794407915302914', '1', '1527471479688798209', '2022-07-12 17:51:39');
INSERT INTO `sys_role_menu` VALUES ('1546794408032743425', '1', '1527471918001954818', '2022-07-12 17:51:39');
INSERT INTO `sys_role_menu` VALUES ('1546794408150183937', '1', '1527471918337499138', '2022-07-12 17:51:39');
INSERT INTO `sys_role_menu` VALUES ('1546794408267624450', '1', '1527471918694014977', '2022-07-12 17:51:39');
INSERT INTO `sys_role_menu` VALUES ('1546794408385064961', '1', '1527471919042142209', '2022-07-12 17:51:39');
INSERT INTO `sys_role_menu` VALUES ('1546794408502505473', '1', '1527471919386075137', '2022-07-12 17:51:39');
INSERT INTO `sys_role_menu` VALUES ('1546794408619945986', '1', '1496327483721789441', '2022-07-12 17:51:39');
INSERT INTO `sys_role_menu` VALUES ('1546794408737386497', '1', '1496327552973942785', '2022-07-12 17:51:39');
INSERT INTO `sys_role_menu` VALUES ('1546794408854827010', '1', '1496327553334652930', '2022-07-12 17:51:39');
INSERT INTO `sys_role_menu` VALUES ('1546794408976461825', '1', '1496327553699557377', '2022-07-12 17:51:39');
INSERT INTO `sys_role_menu` VALUES ('1546794409093902338', '1', '1496327554068656130', '2022-07-12 17:51:39');
INSERT INTO `sys_role_menu` VALUES ('1546794409211342849', '1', '1496327554433560577', '2022-07-12 17:51:39');
INSERT INTO `sys_role_menu` VALUES ('1546794409328783362', '1', '1496327802522447873', '2022-07-12 17:51:39');
INSERT INTO `sys_role_menu` VALUES ('1546794409446223873', '1', '1496327860647112706', '2022-07-12 17:51:39');
INSERT INTO `sys_role_menu` VALUES ('1546794409563664385', '1', '1496327861003628545', '2022-07-12 17:51:39');
INSERT INTO `sys_role_menu` VALUES ('1546794409676910593', '1', '1496327861351755778', '2022-07-12 17:51:39');
INSERT INTO `sys_role_menu` VALUES ('1546794409794351105', '1', '1496327861712465921', '2022-07-12 17:51:40');
INSERT INTO `sys_role_menu` VALUES ('1546794409915985922', '1', '1496327862068981762', '2022-07-12 17:51:40');
INSERT INTO `sys_role_menu` VALUES ('1546794410033426433', '1', '1546742598279147522', '2022-07-12 17:51:40');
INSERT INTO `sys_role_menu` VALUES ('1546794410150866946', '1', '1546742814403244033', '2022-07-12 17:51:40');
INSERT INTO `sys_role_menu` VALUES ('1546794410268307457', '1', '1546742996209545218', '2022-07-12 17:51:40');
INSERT INTO `sys_role_menu` VALUES ('1546794410385747970', '1', '1546743121774424065', '2022-07-12 17:51:40');
INSERT INTO `sys_role_menu` VALUES ('1546794410507382786', '1', '1546775469337391105', '2022-07-12 17:51:40');
INSERT INTO `sys_role_menu` VALUES ('1546794410624823298', '1', '1546775617249521665', '2022-07-12 17:51:40');
INSERT INTO `sys_role_menu` VALUES ('1546794410746458114', '1', '1546752891075997698', '2022-07-12 17:51:40');
INSERT INTO `sys_role_menu` VALUES ('1546794410863898625', '1', '1546753066938970114', '2022-07-12 17:51:40');
INSERT INTO `sys_role_menu` VALUES ('1546794410981339138', '1', '1491973212968632322', '2022-07-12 17:51:40');
INSERT INTO `sys_role_menu` VALUES ('1546794411098779650', '1', '1493578977630121986', '2022-07-12 17:51:40');
INSERT INTO `sys_role_menu` VALUES ('1546794411216220161', '1', '1493578977978249218', '2022-07-12 17:51:40');
INSERT INTO `sys_role_menu` VALUES ('1546794411329466369', '1', '1493578978313793537', '2022-07-12 17:51:40');
INSERT INTO `sys_role_menu` VALUES ('1546794411446906882', '1', '1493578978649337858', '2022-07-12 17:51:40');
INSERT INTO `sys_role_menu` VALUES ('1546794411568541698', '1', '1526179827628048385', '2022-07-12 17:51:40');
INSERT INTO `sys_role_menu` VALUES ('1546794411685982209', '1', '1493596733666652162', '2022-07-12 17:51:40');
INSERT INTO `sys_role_menu` VALUES ('1546794411803422722', '1', '1493766204259942401', '2022-07-12 17:51:40');
INSERT INTO `sys_role_menu` VALUES ('1546794411920863233', '1', '1537048076783616001', '2022-07-12 17:51:40');
INSERT INTO `sys_role_menu` VALUES ('1546794412042498049', '1', '1537066492991012865', '2022-07-12 17:51:40');
INSERT INTO `sys_role_menu` VALUES ('1546794412159938561', '1', '1537066557067395074', '2022-07-12 17:51:40');
INSERT INTO `sys_role_menu` VALUES ('1546794412277379073', '1', '1521496866882236418', '2022-07-12 17:51:40');
INSERT INTO `sys_role_menu` VALUES ('1546794412403208194', '1', '1531528760525074434', '2022-07-12 17:51:40');
INSERT INTO `sys_role_menu` VALUES ('1546794412524843009', '1', '1531536449854517250', '2022-07-12 17:51:40');
INSERT INTO `sys_role_menu` VALUES ('1546794412642283521', '1', '1531536545572728833', '2022-07-12 17:51:40');
INSERT INTO `sys_role_menu` VALUES ('1546794412763918337', '1', '1531536746446336001', '2022-07-12 17:51:40');
INSERT INTO `sys_role_menu` VALUES ('1546794412881358849', '1', '1531536866638311426', '2022-07-12 17:51:40');
INSERT INTO `sys_role_menu` VALUES ('1546794412998799361', '1', '1535633963410956289', '2022-07-12 17:51:40');
INSERT INTO `sys_role_menu` VALUES ('1546794413116239873', '1', '1531529196871102466', '2022-07-12 17:51:40');
INSERT INTO `sys_role_menu` VALUES ('1546794413233680386', '1', '1531537172243689474', '2022-07-12 17:51:40');
INSERT INTO `sys_role_menu` VALUES ('1546794413355315201', '1', '1531537289042472961', '2022-07-12 17:51:40');
INSERT INTO `sys_role_menu` VALUES ('1546794413472755713', '1', '1543116535774879745', '2022-07-12 17:51:40');
INSERT INTO `sys_role_menu` VALUES ('1546794413594390530', '1', '1527835787455164418', '2022-07-12 17:51:40');
INSERT INTO `sys_role_menu` VALUES ('1546794413711831041', '1', '1527835963171336193', '2022-07-12 17:51:40');
INSERT INTO `sys_role_menu` VALUES ('1546794413829271553', '1', '1527836009900077057', '2022-07-12 17:51:40');
INSERT INTO `sys_role_menu` VALUES ('1546794413946712065', '1', '1527836010248204290', '2022-07-12 17:51:40');
INSERT INTO `sys_role_menu` VALUES ('1546794414068346882', '1', '1527836010596331522', '2022-07-12 17:51:41');
INSERT INTO `sys_role_menu` VALUES ('1546794414185787394', '1', '1527836010944458754', '2022-07-12 17:51:41');
INSERT INTO `sys_role_menu` VALUES ('1546794414303227906', '1', '1527836011300974593', '2022-07-12 17:51:41');
INSERT INTO `sys_role_menu` VALUES ('1546794414420668417', '1', '1531976380791881730', '2022-07-12 17:51:41');
INSERT INTO `sys_role_menu` VALUES ('1546794414538108929', '1', '1531976538732593154', '2022-07-12 17:51:41');
INSERT INTO `sys_role_menu` VALUES ('1546794414655549442', '1', '1531977159263092738', '2022-07-12 17:51:41');
INSERT INTO `sys_role_menu` VALUES ('1546794414772989954', '1', '1531977248807288834', '2022-07-12 17:51:41');
INSERT INTO `sys_role_menu` VALUES ('1546794414890430466', '1', '1531977369196396545', '2022-07-12 17:51:41');
INSERT INTO `sys_role_menu` VALUES ('1546794415003676674', '1', '1531977444341547010', '2022-07-12 17:51:41');
INSERT INTO `sys_role_menu` VALUES ('1546794415125311490', '1', '1531977518870134785', '2022-07-12 17:51:41');
INSERT INTO `sys_role_menu` VALUES ('1546794415242752002', '1', '1531977016149245953', '2022-07-12 17:51:41');
INSERT INTO `sys_role_menu` VALUES ('1546794415360192513', '1', '1531976761387220994', '2022-07-12 17:51:41');
INSERT INTO `sys_role_menu` VALUES ('1546794415477633025', '1', '1536174109680214017', '2022-07-12 17:51:41');
INSERT INTO `sys_role_menu` VALUES ('1546794415595073537', '1', '1536174567983423489', '2022-07-12 17:51:41');
INSERT INTO `sys_role_menu` VALUES ('1546794415712514050', '1', '1536174316656533505', '2022-07-12 17:51:41');
INSERT INTO `sys_role_menu` VALUES ('1546794415829954561', '1', '1536174413406543874', '2022-07-12 17:51:41');
INSERT INTO `sys_role_menu` VALUES ('1546794415947395073', '1', '10001', '2022-07-12 17:51:41');
INSERT INTO `sys_role_menu` VALUES ('1546794416064835585', '1', '10002', '2022-07-12 17:51:41');
INSERT INTO `sys_role_menu` VALUES ('1546794416178081793', '1', '100001', '2022-07-12 17:51:41');
INSERT INTO `sys_role_menu` VALUES ('1546794416295522306', '1', '1495687621054353410', '2022-07-12 17:51:41');
INSERT INTO `sys_role_menu` VALUES ('1546794416417157121', '1', '1495687858816864257', '2022-07-12 17:51:41');
INSERT INTO `sys_role_menu` VALUES ('1546794416530403329', '1', '1521469373525716994', '2022-07-12 17:51:41');
INSERT INTO `sys_role_menu` VALUES ('1546794416647843842', '1', '999999999999', '2022-07-12 17:51:41');
INSERT INTO `sys_role_menu` VALUES ('1546794416765284353', '1', '1491684226094198786', '2022-07-12 17:51:41');
INSERT INTO `sys_role_menu` VALUES ('1546794416886919169', '1', '1491690996678021121', '2022-07-12 17:51:41');
INSERT INTO `sys_role_menu` VALUES ('1546794417008553986', '1', '1493836091183411202', '2022-07-12 17:51:41');
INSERT INTO `sys_role_menu` VALUES ('1546794417125994497', '1', '1493836209106268161', '2022-07-12 17:51:41');
INSERT INTO `sys_role_menu` VALUES ('1546794417243435010', '1', '1493836280589791233', '2022-07-12 17:51:41');
INSERT INTO `sys_role_menu` VALUES ('1546794417360875521', '1', '1493841029473042434', '2022-07-12 17:51:41');
INSERT INTO `sys_role_menu` VALUES ('1546794417478316034', '1', '1491663510904893441', '2022-07-12 17:51:41');
INSERT INTO `sys_role_menu` VALUES ('1546794417595756546', '1', '1491752531735490561', '2022-07-12 17:51:41');
INSERT INTO `sys_role_menu` VALUES ('1546794417713197057', '1', '1491756888363307009', '2022-07-12 17:51:41');
INSERT INTO `sys_role_menu` VALUES ('1546794417830637569', '1', '1491757020773289986', '2022-07-12 17:51:41');
INSERT INTO `sys_role_menu` VALUES ('1546794417952272385', '1', '1491757382771085313', '2022-07-12 17:51:41');
INSERT INTO `sys_role_menu` VALUES ('1546794418069712897', '1', '1494514119857180674', '2022-07-12 17:51:41');
INSERT INTO `sys_role_menu` VALUES ('1546794418187153410', '1', '1491948958826921986', '2022-07-12 17:51:42');
INSERT INTO `sys_role_menu` VALUES ('1546794418304593922', '1', '1491949315883827201', '2022-07-12 17:51:42');
INSERT INTO `sys_role_menu` VALUES ('1546794418417840130', '1', '1491969633293729794', '2022-07-12 17:51:42');
INSERT INTO `sys_role_menu` VALUES ('1546794418539474945', '1', '1531445896986435585', '2022-07-12 17:51:42');
INSERT INTO `sys_role_menu` VALUES ('1546794418661109762', '1', '1531446009649635329', '2022-07-12 17:51:42');
INSERT INTO `sys_role_menu` VALUES ('1546794418782744578', '1', '1531446009779658754', '2022-07-12 17:51:42');
INSERT INTO `sys_role_menu` VALUES ('1546794418904379393', '1', '1531446009913876482', '2022-07-12 17:51:42');
INSERT INTO `sys_role_menu` VALUES ('1546794419021819905', '1', '1531446010039705602', '2022-07-12 17:51:42');
INSERT INTO `sys_role_menu` VALUES ('1546794419139260417', '1', '1531446010169729026', '2022-07-12 17:51:42');
INSERT INTO `sys_role_menu` VALUES ('1546794419256700929', '1', '1494216988311183361', '2022-07-12 17:51:42');
INSERT INTO `sys_role_menu` VALUES ('1546794419374141442', '1', '1494217080162246658', '2022-07-12 17:51:42');
INSERT INTO `sys_role_menu` VALUES ('1546794419491581954', '1', '1494217080510373890', '2022-07-12 17:51:42');
INSERT INTO `sys_role_menu` VALUES ('1546794419609022465', '1', '1494217080858501121', '2022-07-12 17:51:42');
INSERT INTO `sys_role_menu` VALUES ('1546794419726462977', '1', '1494217081206628354', '2022-07-12 17:51:42');
INSERT INTO `sys_role_menu` VALUES ('1546794419848097794', '1', '1494217081558949890', '2022-07-12 17:51:42');
INSERT INTO `sys_role_menu` VALUES ('1546794419965538306', '1', '1493884088730529793', '2022-07-12 17:51:42');
INSERT INTO `sys_role_menu` VALUES ('1546794420078784514', '1', '1493884379760701442', '2022-07-12 17:51:42');
INSERT INTO `sys_role_menu` VALUES ('1546794420196225026', '1', '1494192758630694913', '2022-07-12 17:51:42');
INSERT INTO `sys_role_menu` VALUES ('1546794420317859841', '1', '1494192758974627842', '2022-07-12 17:51:42');
INSERT INTO `sys_role_menu` VALUES ('1546794420435300353', '1', '1494192759318560770', '2022-07-12 17:51:42');
INSERT INTO `sys_role_menu` VALUES ('1546794420552740865', '1', '1494192759662493698', '2022-07-12 17:51:42');
INSERT INTO `sys_role_menu` VALUES ('1546794420674375682', '1', '1494192760010620930', '2022-07-12 17:51:42');
INSERT INTO `sys_role_menu` VALUES ('1546794420791816194', '1', '1494151991157673985', '2022-07-12 17:51:42');
INSERT INTO `sys_role_menu` VALUES ('1546794420913451009', '1', '1494153372996255746', '2022-07-12 17:51:42');
INSERT INTO `sys_role_menu` VALUES ('1546794421030891522', '1', '1494153373352771586', '2022-07-12 17:51:42');
INSERT INTO `sys_role_menu` VALUES ('1546794421148332034', '1', '1494153373696704514', '2022-07-12 17:51:42');
INSERT INTO `sys_role_menu` VALUES ('1546794421269966849', '1', '1494153374053220354', '2022-07-12 17:51:42');
INSERT INTO `sys_role_menu` VALUES ('1546794421399990273', '1', '1494153374401347585', '2022-07-12 17:51:42');
INSERT INTO `sys_role_menu` VALUES ('1546794421517430786', '1', '1527928859455168514', '2022-07-12 17:51:42');
INSERT INTO `sys_role_menu` VALUES ('1546794421634871297', '1', '1527947365856878593', '2022-07-12 17:51:42');
INSERT INTO `sys_role_menu` VALUES ('1546794421756506113', '1', '10003', '2022-07-12 17:51:42');
INSERT INTO `sys_role_menu` VALUES ('1546794421878140930', '1', '1539129183310196738', '2022-07-12 17:51:42');
INSERT INTO `sys_role_menu` VALUES ('1546794421995581442', '1', '1531846396944785409', '2022-07-12 17:51:42');
INSERT INTO `sys_role_menu` VALUES ('1546794422117216258', '1', '1531846893999169537', '2022-07-12 17:51:42');
INSERT INTO `sys_role_menu` VALUES ('1546794422234656769', '1', '1531882841361915906', '2022-07-12 17:51:42');
INSERT INTO `sys_role_menu` VALUES ('1546794422352097281', '1', '1531846725585281025', '2022-07-12 17:51:43');
INSERT INTO `sys_role_menu` VALUES ('1546794422469537794', '1', '1531882726605758465', '2022-07-12 17:51:43');
INSERT INTO `sys_role_menu` VALUES ('1546794422586978305', '1', '1539130386861535234', '2022-07-12 17:51:43');
INSERT INTO `sys_role_menu` VALUES ('1546794422704418817', '1', '1539130685290459138', '2022-07-12 17:51:43');
INSERT INTO `sys_role_menu` VALUES ('1546794422821859329', '1', '1539614105648947202', '2022-07-12 17:51:43');
INSERT INTO `sys_role_menu` VALUES ('1546794422939299841', '1', '1539130909853495297', '2022-07-12 17:51:43');
INSERT INTO `sys_role_menu` VALUES ('1546794423060934657', '1', '1539614296187789314', '2022-07-12 17:51:43');
INSERT INTO `sys_role_menu` VALUES ('1549383324485722113', '1494300145462980609', '1493584727114936322', '2022-07-19 21:19:05');
INSERT INTO `sys_role_menu` VALUES ('1549383324833849345', '1494300145462980609', '1493587429257539586', '2022-07-19 21:19:05');
INSERT INTO `sys_role_menu` VALUES ('1549383325186170882', '1494300145462980609', '1532620395988029442', '2022-07-19 21:19:05');
INSERT INTO `sys_role_menu` VALUES ('1549383325538492418', '1494300145462980609', '1493587910381957121', '2022-07-19 21:19:05');
INSERT INTO `sys_role_menu` VALUES ('1549383325886619650', '1494300145462980609', '1493587910721695745', '2022-07-19 21:19:05');
INSERT INTO `sys_role_menu` VALUES ('1549383326243135489', '1494300145462980609', '1493587911057240066', '2022-07-19 21:19:05');
INSERT INTO `sys_role_menu` VALUES ('1549383326582874114', '1494300145462980609', '1496012487833960450', '2022-07-19 21:19:05');
INSERT INTO `sys_role_menu` VALUES ('1549383326918418434', '1494300145462980609', '1497468294740176898', '2022-07-19 21:19:06');
INSERT INTO `sys_role_menu` VALUES ('1549383327279128578', '1494300145462980609', '1532620101065543681', '2022-07-19 21:19:06');
INSERT INTO `sys_role_menu` VALUES ('1549383327627255809', '1494300145462980609', '1532620840659750914', '2022-07-19 21:19:06');
INSERT INTO `sys_role_menu` VALUES ('1549383327975383042', '1494300145462980609', '1532620931885862913', '2022-07-19 21:19:06');
INSERT INTO `sys_role_menu` VALUES ('1549383328327704577', '1494300145462980609', '1532621007333003266', '2022-07-19 21:19:06');
INSERT INTO `sys_role_menu` VALUES ('1549383328684220418', '1494300145462980609', '1532621107589451778', '2022-07-19 21:19:06');
INSERT INTO `sys_role_menu` VALUES ('1549383329036541953', '1494300145462980609', '1532621196013768706', '2022-07-19 21:19:06');
INSERT INTO `sys_role_menu` VALUES ('1549383329384669185', '1494300145462980609', '1532634358100430850', '2022-07-19 21:19:06');
INSERT INTO `sys_role_menu` VALUES ('1549383329736990721', '1494300145462980609', '1532634435510505473', '2022-07-19 21:19:06');
INSERT INTO `sys_role_menu` VALUES ('1549383330085117953', '1494300145462980609', '1532634517374930946', '2022-07-19 21:19:06');
INSERT INTO `sys_role_menu` VALUES ('1549383330437439490', '1494300145462980609', '1532634586568364034', '2022-07-19 21:19:06');
INSERT INTO `sys_role_menu` VALUES ('1549383330785566721', '1494300145462980609', '1532634687902748674', '2022-07-19 21:19:06');
INSERT INTO `sys_role_menu` VALUES ('1549383331133693954', '1494300145462980609', '1527471479688798209', '2022-07-19 21:19:07');
INSERT INTO `sys_role_menu` VALUES ('1549383331486015489', '1494300145462980609', '1527471918001954818', '2022-07-19 21:19:07');
INSERT INTO `sys_role_menu` VALUES ('1549383331834142721', '1494300145462980609', '1527471918337499138', '2022-07-19 21:19:07');
INSERT INTO `sys_role_menu` VALUES ('1549383332186464257', '1494300145462980609', '1527471918694014977', '2022-07-19 21:19:07');
INSERT INTO `sys_role_menu` VALUES ('1549383332534591490', '1494300145462980609', '1527471919042142209', '2022-07-19 21:19:07');
INSERT INTO `sys_role_menu` VALUES ('1549383332882718721', '1494300145462980609', '1527471919386075137', '2022-07-19 21:19:07');
INSERT INTO `sys_role_menu` VALUES ('1549383333235040258', '1494300145462980609', '1549383195510874114', '2022-07-19 21:19:07');
INSERT INTO `sys_role_menu` VALUES ('1549383333578973186', '1494300145462980609', '1549383285281562625', '2022-07-19 21:19:07');
INSERT INTO `sys_role_menu` VALUES ('1549383333931294721', '1494300145462980609', '1496327483721789441', '2022-07-19 21:19:07');
INSERT INTO `sys_role_menu` VALUES ('1549383334283616258', '1494300145462980609', '1496327552973942785', '2022-07-19 21:19:07');
INSERT INTO `sys_role_menu` VALUES ('1549383334635937794', '1494300145462980609', '1496327553334652930', '2022-07-19 21:19:07');
INSERT INTO `sys_role_menu` VALUES ('1549383334979870721', '1494300145462980609', '1496327553699557377', '2022-07-19 21:19:07');
INSERT INTO `sys_role_menu` VALUES ('1549383335332192257', '1494300145462980609', '1496327554068656130', '2022-07-19 21:19:08');
INSERT INTO `sys_role_menu` VALUES ('1549383335680319489', '1494300145462980609', '1496327554433560577', '2022-07-19 21:19:08');
INSERT INTO `sys_role_menu` VALUES ('1549383336028446721', '1494300145462980609', '1496327802522447873', '2022-07-19 21:19:08');
INSERT INTO `sys_role_menu` VALUES ('1549383336380768258', '1494300145462980609', '1496327860647112706', '2022-07-19 21:19:08');
INSERT INTO `sys_role_menu` VALUES ('1549383336724701185', '1494300145462980609', '1496327861003628545', '2022-07-19 21:19:08');
INSERT INTO `sys_role_menu` VALUES ('1549383337077022721', '1494300145462980609', '1496327861351755778', '2022-07-19 21:19:08');
INSERT INTO `sys_role_menu` VALUES ('1549383337425149954', '1494300145462980609', '1496327861712465921', '2022-07-19 21:19:08');
INSERT INTO `sys_role_menu` VALUES ('1549383337777471489', '1494300145462980609', '1496327862068981762', '2022-07-19 21:19:08');
INSERT INTO `sys_role_menu` VALUES ('1549383338125598721', '1494300145462980609', '1546742598279147522', '2022-07-19 21:19:08');
INSERT INTO `sys_role_menu` VALUES ('1549383338473725953', '1494300145462980609', '1546742814403244033', '2022-07-19 21:19:08');
INSERT INTO `sys_role_menu` VALUES ('1549383338826047490', '1494300145462980609', '1546742996209545218', '2022-07-19 21:19:08');
INSERT INTO `sys_role_menu` VALUES ('1549383339174174722', '1494300145462980609', '1546743121774424065', '2022-07-19 21:19:08');
INSERT INTO `sys_role_menu` VALUES ('1549383339522301953', '1494300145462980609', '1546775469337391105', '2022-07-19 21:19:09');
INSERT INTO `sys_role_menu` VALUES ('1549383339874623489', '1494300145462980609', '1546775617249521665', '2022-07-19 21:19:09');
INSERT INTO `sys_role_menu` VALUES ('1549383340218556418', '1494300145462980609', '1546752891075997698', '2022-07-19 21:19:09');
INSERT INTO `sys_role_menu` VALUES ('1549383340570877953', '1494300145462980609', '1546753066938970114', '2022-07-19 21:19:09');
INSERT INTO `sys_role_menu` VALUES ('1549383340919005186', '1494300145462980609', '1491973212968632322', '2022-07-19 21:19:09');
INSERT INTO `sys_role_menu` VALUES ('1549383341267132418', '1494300145462980609', '1493578977630121986', '2022-07-19 21:19:09');
INSERT INTO `sys_role_menu` VALUES ('1549383341619453953', '1494300145462980609', '1493578977978249218', '2022-07-19 21:19:09');
INSERT INTO `sys_role_menu` VALUES ('1549383341971775489', '1494300145462980609', '1493578978313793537', '2022-07-19 21:19:09');
INSERT INTO `sys_role_menu` VALUES ('1549383342319902722', '1494300145462980609', '1493578978649337858', '2022-07-19 21:19:09');
INSERT INTO `sys_role_menu` VALUES ('1549383342668029954', '1494300145462980609', '1526179827628048385', '2022-07-19 21:19:09');
INSERT INTO `sys_role_menu` VALUES ('1549383343016157186', '1494300145462980609', '1493596733666652162', '2022-07-19 21:19:09');
INSERT INTO `sys_role_menu` VALUES ('1549383343364284417', '1494300145462980609', '1493766204259942401', '2022-07-19 21:19:09');
INSERT INTO `sys_role_menu` VALUES ('1549383343708217346', '1494300145462980609', '1537048076783616001', '2022-07-19 21:19:10');
INSERT INTO `sys_role_menu` VALUES ('1549383344064733186', '1494300145462980609', '1537066492991012865', '2022-07-19 21:19:10');
INSERT INTO `sys_role_menu` VALUES ('1549383344408666113', '1494300145462980609', '1537066557067395074', '2022-07-19 21:19:10');
INSERT INTO `sys_role_menu` VALUES ('1549383344760987650', '1494300145462980609', '1521496866882236418', '2022-07-19 21:19:10');
INSERT INTO `sys_role_menu` VALUES ('1549383345109114881', '1494300145462980609', '1531528760525074434', '2022-07-19 21:19:10');
INSERT INTO `sys_role_menu` VALUES ('1549383345461436418', '1494300145462980609', '1531536449854517250', '2022-07-19 21:19:10');
INSERT INTO `sys_role_menu` VALUES ('1549383345813757953', '1494300145462980609', '1531536545572728833', '2022-07-19 21:19:10');
INSERT INTO `sys_role_menu` VALUES ('1549383346161885186', '1494300145462980609', '1531536746446336001', '2022-07-19 21:19:10');
INSERT INTO `sys_role_menu` VALUES ('1549383346505818114', '1494300145462980609', '1531536866638311426', '2022-07-19 21:19:10');
INSERT INTO `sys_role_menu` VALUES ('1549383346862333954', '1494300145462980609', '1535633963410956289', '2022-07-19 21:19:10');
INSERT INTO `sys_role_menu` VALUES ('1549383347210461186', '1494300145462980609', '1531529196871102466', '2022-07-19 21:19:10');
INSERT INTO `sys_role_menu` VALUES ('1549383347562782721', '1494300145462980609', '1531537172243689474', '2022-07-19 21:19:10');
INSERT INTO `sys_role_menu` VALUES ('1549383347906715649', '1494300145462980609', '1531537289042472961', '2022-07-19 21:19:11');
INSERT INTO `sys_role_menu` VALUES ('1549383348263231490', '1494300145462980609', '1543116535774879745', '2022-07-19 21:19:11');
INSERT INTO `sys_role_menu` VALUES ('1549383348611358722', '1494300145462980609', '1527835787455164418', '2022-07-19 21:19:11');
INSERT INTO `sys_role_menu` VALUES ('1549383348955291650', '1494300145462980609', '1527835963171336193', '2022-07-19 21:19:11');
INSERT INTO `sys_role_menu` VALUES ('1549383349307613186', '1494300145462980609', '1527836009900077057', '2022-07-19 21:19:11');
INSERT INTO `sys_role_menu` VALUES ('1549383349655740417', '1494300145462980609', '1527836010248204290', '2022-07-19 21:19:11');
INSERT INTO `sys_role_menu` VALUES ('1549383350003867649', '1494300145462980609', '1527836010596331522', '2022-07-19 21:19:11');
INSERT INTO `sys_role_menu` VALUES ('1549383350351994881', '1494300145462980609', '1527836010944458754', '2022-07-19 21:19:11');
INSERT INTO `sys_role_menu` VALUES ('1549383350704316418', '1494300145462980609', '1527836011300974593', '2022-07-19 21:19:11');
INSERT INTO `sys_role_menu` VALUES ('1549383351056637954', '1494300145462980609', '1531976380791881730', '2022-07-19 21:19:11');
INSERT INTO `sys_role_menu` VALUES ('1549383351404765186', '1494300145462980609', '1531976538732593154', '2022-07-19 21:19:11');
INSERT INTO `sys_role_menu` VALUES ('1549383351752892418', '1494300145462980609', '1531977159263092738', '2022-07-19 21:19:11');
INSERT INTO `sys_role_menu` VALUES ('1549383352096825346', '1494300145462980609', '1531977248807288834', '2022-07-19 21:19:12');
INSERT INTO `sys_role_menu` VALUES ('1549383352453341186', '1494300145462980609', '1531977369196396545', '2022-07-19 21:19:12');
INSERT INTO `sys_role_menu` VALUES ('1549383352801468418', '1494300145462980609', '1531977444341547010', '2022-07-19 21:19:12');
INSERT INTO `sys_role_menu` VALUES ('1549383353149595650', '1494300145462980609', '1531977518870134785', '2022-07-19 21:19:12');
INSERT INTO `sys_role_menu` VALUES ('1549383353501917186', '1494300145462980609', '1531977016149245953', '2022-07-19 21:19:12');
INSERT INTO `sys_role_menu` VALUES ('1549383353850044417', '1494300145462980609', '1531976761387220994', '2022-07-19 21:19:12');
INSERT INTO `sys_role_menu` VALUES ('1549383354193977346', '1494300145462980609', '1536174109680214017', '2022-07-19 21:19:12');
INSERT INTO `sys_role_menu` VALUES ('1549383354546298881', '1494300145462980609', '1536174567983423489', '2022-07-19 21:19:12');
INSERT INTO `sys_role_menu` VALUES ('1549383354894426114', '1494300145462980609', '1536174316656533505', '2022-07-19 21:19:12');
INSERT INTO `sys_role_menu` VALUES ('1549383355246747650', '1494300145462980609', '1536174413406543874', '2022-07-19 21:19:12');
INSERT INTO `sys_role_menu` VALUES ('1549383355594874881', '1494300145462980609', '10001', '2022-07-19 21:19:12');
INSERT INTO `sys_role_menu` VALUES ('1549383355947196418', '1494300145462980609', '10002', '2022-07-19 21:19:12');
INSERT INTO `sys_role_menu` VALUES ('1549383356291129346', '1494300145462980609', '100001', '2022-07-19 21:19:13');
INSERT INTO `sys_role_menu` VALUES ('1549383356643450881', '1494300145462980609', '1495687621054353410', '2022-07-19 21:19:13');
INSERT INTO `sys_role_menu` VALUES ('1549383356995772417', '1494300145462980609', '1495687858816864257', '2022-07-19 21:19:13');
INSERT INTO `sys_role_menu` VALUES ('1549383357339705346', '1494300145462980609', '1521469373525716994', '2022-07-19 21:19:13');
INSERT INTO `sys_role_menu` VALUES ('1549383357692026882', '1494300145462980609', '999999999999', '2022-07-19 21:19:13');
INSERT INTO `sys_role_menu` VALUES ('1549383358040154114', '1494300145462980609', '1491684226094198786', '2022-07-19 21:19:13');
INSERT INTO `sys_role_menu` VALUES ('1549383358388281345', '1494300145462980609', '1491690996678021121', '2022-07-19 21:19:13');
INSERT INTO `sys_role_menu` VALUES ('1549383358740602881', '1494300145462980609', '1493836091183411202', '2022-07-19 21:19:13');
INSERT INTO `sys_role_menu` VALUES ('1549383359088730113', '1494300145462980609', '1493836209106268161', '2022-07-19 21:19:13');
INSERT INTO `sys_role_menu` VALUES ('1549383359436857345', '1494300145462980609', '1493836280589791233', '2022-07-19 21:19:13');
INSERT INTO `sys_role_menu` VALUES ('1549383359784984578', '1494300145462980609', '1493841029473042434', '2022-07-19 21:19:13');
INSERT INTO `sys_role_menu` VALUES ('1549383360137306113', '1494300145462980609', '1491663510904893441', '2022-07-19 21:19:13');
INSERT INTO `sys_role_menu` VALUES ('1549383360481239042', '1494300145462980609', '1491752531735490561', '2022-07-19 21:19:14');
INSERT INTO `sys_role_menu` VALUES ('1549383360833560577', '1494300145462980609', '1491756888363307009', '2022-07-19 21:19:14');
INSERT INTO `sys_role_menu` VALUES ('1549383361181687810', '1494300145462980609', '1491757020773289986', '2022-07-19 21:19:14');
INSERT INTO `sys_role_menu` VALUES ('1549383361529815042', '1494300145462980609', '1491757382771085313', '2022-07-19 21:19:14');
INSERT INTO `sys_role_menu` VALUES ('1549383361877942273', '1494300145462980609', '1494514119857180674', '2022-07-19 21:19:14');
INSERT INTO `sys_role_menu` VALUES ('1549383362230263810', '1494300145462980609', '1547110946242326529', '2022-07-19 21:19:14');
INSERT INTO `sys_role_menu` VALUES ('1549383362578391041', '1494300145462980609', '1547111164111253505', '2022-07-19 21:19:14');
INSERT INTO `sys_role_menu` VALUES ('1549383362926518273', '1494300145462980609', '1491948958826921986', '2022-07-19 21:19:14');
INSERT INTO `sys_role_menu` VALUES ('1549383363278839809', '1494300145462980609', '1491949315883827201', '2022-07-19 21:19:14');
INSERT INTO `sys_role_menu` VALUES ('1549383363622772738', '1494300145462980609', '1491969633293729794', '2022-07-19 21:19:14');
INSERT INTO `sys_role_menu` VALUES ('1549383363975094274', '1494300145462980609', '1531445896986435585', '2022-07-19 21:19:14');
INSERT INTO `sys_role_menu` VALUES ('1549383364323221505', '1494300145462980609', '1531446009649635329', '2022-07-19 21:19:14');
INSERT INTO `sys_role_menu` VALUES ('1549383364667154434', '1494300145462980609', '1531446009779658754', '2022-07-19 21:19:15');
INSERT INTO `sys_role_menu` VALUES ('1549383365011087361', '1494300145462980609', '1531446009913876482', '2022-07-19 21:19:15');
INSERT INTO `sys_role_menu` VALUES ('1549383365363408898', '1494300145462980609', '1531446010039705602', '2022-07-19 21:19:15');
INSERT INTO `sys_role_menu` VALUES ('1549383365711536129', '1494300145462980609', '1531446010169729026', '2022-07-19 21:19:15');
INSERT INTO `sys_role_menu` VALUES ('1549383366059663362', '1494300145462980609', '1494216988311183361', '2022-07-19 21:19:15');
INSERT INTO `sys_role_menu` VALUES ('1549383366411984898', '1494300145462980609', '1494217080162246658', '2022-07-19 21:19:15');
INSERT INTO `sys_role_menu` VALUES ('1549383366755917825', '1494300145462980609', '1494217080510373890', '2022-07-19 21:19:15');
INSERT INTO `sys_role_menu` VALUES ('1549383367108239362', '1494300145462980609', '1494217080858501121', '2022-07-19 21:19:15');
INSERT INTO `sys_role_menu` VALUES ('1549383367456366594', '1494300145462980609', '1494217081206628354', '2022-07-19 21:19:15');
INSERT INTO `sys_role_menu` VALUES ('1549383367808688129', '1494300145462980609', '1494217081558949890', '2022-07-19 21:19:15');
INSERT INTO `sys_role_menu` VALUES ('1549383368156815361', '1494300145462980609', '1493884088730529793', '2022-07-19 21:19:15');
INSERT INTO `sys_role_menu` VALUES ('1549383368500748290', '1494300145462980609', '1493884379760701442', '2022-07-19 21:19:15');
INSERT INTO `sys_role_menu` VALUES ('1549383368848875521', '1494300145462980609', '1494192758630694913', '2022-07-19 21:19:15');
INSERT INTO `sys_role_menu` VALUES ('1549383369201197057', '1494300145462980609', '1494192758974627842', '2022-07-19 21:19:16');
INSERT INTO `sys_role_menu` VALUES ('1549383369553518593', '1494300145462980609', '1494192759318560770', '2022-07-19 21:19:16');
INSERT INTO `sys_role_menu` VALUES ('1549383369897451522', '1494300145462980609', '1494192759662493698', '2022-07-19 21:19:16');
INSERT INTO `sys_role_menu` VALUES ('1549383370249773058', '1494300145462980609', '1494192760010620930', '2022-07-19 21:19:16');
INSERT INTO `sys_role_menu` VALUES ('1549383370593705985', '1494300145462980609', '1494151991157673985', '2022-07-19 21:19:16');
INSERT INTO `sys_role_menu` VALUES ('1549383370941833217', '1494300145462980609', '1494153372996255746', '2022-07-19 21:19:16');
INSERT INTO `sys_role_menu` VALUES ('1549383371294154753', '1494300145462980609', '1494153373352771586', '2022-07-19 21:19:16');
INSERT INTO `sys_role_menu` VALUES ('1549383371638087682', '1494300145462980609', '1494153373696704514', '2022-07-19 21:19:16');
INSERT INTO `sys_role_menu` VALUES ('1549383371986214913', '1494300145462980609', '1494153374053220354', '2022-07-19 21:19:16');
INSERT INTO `sys_role_menu` VALUES ('1549383372338536450', '1494300145462980609', '1494153374401347585', '2022-07-19 21:19:16');
INSERT INTO `sys_role_menu` VALUES ('1549383372682469378', '1494300145462980609', '1527928859455168514', '2022-07-19 21:19:16');
INSERT INTO `sys_role_menu` VALUES ('1549383373030596610', '1494300145462980609', '1527947365856878593', '2022-07-19 21:19:16');
INSERT INTO `sys_role_menu` VALUES ('1549383373382918146', '1494300145462980609', '1548148638459703297', '2022-07-19 21:19:17');
INSERT INTO `sys_role_menu` VALUES ('1549383373731045378', '1494300145462980609', '1548845472747827202', '2022-07-19 21:19:17');
INSERT INTO `sys_role_menu` VALUES ('1549383374074978305', '1494300145462980609', '1549359604316659713', '2022-07-19 21:19:17');
INSERT INTO `sys_role_menu` VALUES ('1549383374427299841', '1494300145462980609', '1548913625646022657', '2022-07-19 21:19:17');
INSERT INTO `sys_role_menu` VALUES ('1549383374775427073', '1494300145462980609', '10003', '2022-07-19 21:19:17');
INSERT INTO `sys_role_menu` VALUES ('1549383375123554306', '1494300145462980609', '1539129183310196738', '2022-07-19 21:19:17');
INSERT INTO `sys_role_menu` VALUES ('1549383375475875841', '1494300145462980609', '1531846396944785409', '2022-07-19 21:19:17');
INSERT INTO `sys_role_menu` VALUES ('1549383375819808769', '1494300145462980609', '1531846893999169537', '2022-07-19 21:19:17');
INSERT INTO `sys_role_menu` VALUES ('1549383376167936002', '1494300145462980609', '1531882841361915906', '2022-07-19 21:19:17');
INSERT INTO `sys_role_menu` VALUES ('1549383376516063233', '1494300145462980609', '1531846725585281025', '2022-07-19 21:19:17');
INSERT INTO `sys_role_menu` VALUES ('1549383376868384769', '1494300145462980609', '1531882726605758465', '2022-07-19 21:19:17');
INSERT INTO `sys_role_menu` VALUES ('1549383377216512002', '1494300145462980609', '1539130386861535234', '2022-07-19 21:19:17');
INSERT INTO `sys_role_menu` VALUES ('1549383377560444930', '1494300145462980609', '1539130685290459138', '2022-07-19 21:19:18');
INSERT INTO `sys_role_menu` VALUES ('1549383377908572162', '1494300145462980609', '1539614105648947202', '2022-07-19 21:19:18');
INSERT INTO `sys_role_menu` VALUES ('1549383378256699393', '1494300145462980609', '1539130909853495297', '2022-07-19 21:19:18');
INSERT INTO `sys_role_menu` VALUES ('1549383378604826626', '1494300145462980609', '1539614296187789314', '2022-07-19 21:19:18');

-- ----------------------------
-- Table structure for sys_storage_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_storage_config`;
CREATE TABLE `sys_storage_config`  (
                                       `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'PK',
                                       `access_key` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'access_key',
                                       `access_secret` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'access_secret',
                                       `endpoint` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '地域节点',
                                       `bucket` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '域名',
                                       `type` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '存储类型1、阿里OSS；2、七牛云；3、腾讯云',
                                       `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '新增时间',
                                       `update_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
                                       `dir` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT 'material' COMMENT '指定文件夹',
                                       `is_https` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                                       `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                                       PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '文件存储配置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_storage_config
-- ----------------------------
INSERT INTO `sys_storage_config` VALUES ('1491967331820404738', 'xxxxxx', 'xxxxxxxxx', 'oss-cn-beijing.aliyuncs.com', 'huanxing', '1', '2022-02-11 10:48:25', '2022-02-11 21:10:28', 'lijx', NULL, '0');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
                             `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'PK',
                             `username` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户名',
                             `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '密码',
                             `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '邮箱',
                             `nike_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '昵称',
                             `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '头像',
                             `dept_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '部门ID',
                             `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '手机号',
                             `enable_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '启用状态：0、否；1、是',
                             `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                             `create_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建ID',
                             `update_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改ID',
                             `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                             `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
                             PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '系统用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'admin', 'e10adc3949ba59abbe56e057f20f883e', '806@163.com', 'lijiax', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/3da3d902-ec1c-4107-b8e7-0614bbc1b89b.jpg', '2', '17615123399', '1', '0', NULL, NULL, '2022-05-20 17:33:24', '2022-07-08 11:51:12');
INSERT INTO `sys_user` VALUES ('1494511884091822082', 'lijx', '0192023a7bbd73250516f069df18b500', NULL, '环兴商城', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/5137138c-1436-412c-9fb7-74e80b357061.jpg', '1', '17615123397', '1', '0', NULL, NULL, '2022-05-20 17:33:24', '2022-07-01 14:28:24');
INSERT INTO `sys_user` VALUES ('1538797328970452993', 'hxmall', 'e10adc3949ba59abbe56e057f20f883e', NULL, '环兴商城', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/5137138c-1436-412c-9fb7-74e80b357061.jpg', '1', '17615123345', '1', '0', NULL, NULL, '2022-06-20 16:14:07', '2022-06-29 22:06:26');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
                                  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'PK',
                                  `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户ID',
                                  `role_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色ID',
                                  `create_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                                  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '系统用户关联角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1538797329087893506', '1538797328970452993', '1', '2022-06-20 16:14:07');
INSERT INTO `sys_user_role` VALUES ('1547043549640683522', '1', '1', '2022-07-13 10:21:38');
INSERT INTO `sys_user_role` VALUES ('1547059542190485506', '1494511884091822082', '1494300145462980609', '2022-07-13 11:25:11');
INSERT INTO `sys_user_role` VALUES ('1547059542530224129', '1494511884091822082', '1', '2022-07-13 11:25:11');

SET FOREIGN_KEY_CHECKS = 1;
