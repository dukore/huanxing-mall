/*
 Navicat Premium Data Transfer

 Source Server         : 环兴商城
 Source Server Type    : MySQL
 Source Schema         : huanxing_nacos

 Target Server Type    : MySQL
 Target Server Version : 80018
 File Encoding         : 65001

 Date: 25/10/2022 23:28:59
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for config_info
-- ----------------------------
DROP TABLE IF EXISTS `config_info`;
CREATE TABLE `config_info`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'content',
  `md5` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'md5',
  `gmt_create` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `gmt_modified` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `src_user` text CHARACTER SET utf8 COLLATE utf8_bin NULL COMMENT 'source user',
  `src_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'source ip',
  `app_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '租户字段',
  `c_desc` varchar(256) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `c_use` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `effect` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `type` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `c_schema` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `encrypted_data_key` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '秘钥',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_configinfo_datagrouptenant`(`data_id`, `group_id`, `tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'config_info' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of config_info
-- ----------------------------
INSERT INTO `config_info` VALUES (2, 'application-dev.yml', 'DEFAULT_GROUP', 'spring:\n  redis:\n    host: huanxing-redis\n    port: 6379\n    password: ljx19970310.\n    database: 10\n  servlet:\n      multipart:\n          location: /data/tmp   \nrocketmq:\n  # name-server: 59.110.30.161:9876\n  name-server: 127.0.0.1:9876\n  producer:\n    group: producer_group\n    retry-times-when-send-failed: 5\n    send-message-timeout: 5000     \n  jackson:\n    default-property-inclusion: always\n    date-format: yyyy-mm-dd hh:mm:ss   \n# Sa-Token配置\nsa-token: \n    # token名称 (同时也是cookie名称)\n    token-name: satoken\n    # token有效期，单位s 默认30天, -1代表永不过期 \n    timeout: 2592000\n    # token临时有效期 (指定时间内无操作就视为token过期) 单位: 秒\n    activity-timeout: -1\n    # 是否允许同一账号并发登录 (为true时允许一起登录, 为false时新登录挤掉旧登录) \n    is-concurrent: true\n    # 在多人登录同一账号时，是否共用一个token (为true时所有登录共用一个token, 为false时每次登录新建一个token) \n    is-share: false\n    # 是否从cookie中读取token\n    is-read-cookie: false\n    # token风格\n    token-style: uuid\n    # 是否输出操作日志 \n    is-log: true     \n      # jwt秘钥 \n    jwt-secret-key: asdasdasifhueuiwyurfewbfjsdafjksdadad         \nsecure:\n  ignore:\n    urls:\n    - /actuator/**\n    - /mallapi/**\n    - /auth/token/login\n    - /auth/token/phone/login\n    - /auth/token/page\n    - /code\n    - /auth/phone/code\n    - /upms/user/check/phone\n         \nmybatis-plus:\n  mapper-locations: classpath:/mapper/*Mapper.xml\n  type-handlers-package: com.huanxing.cloud.common.myabtis.handler\n  global-config:\n    sql-parser-cache: true\n    banner: false\n    db-config:\n      id-type: auto\n      select-strategy: not_empty\n\n#  endpoints config\nmanagement:\n  endpoints:\n    web:\n      exposure:\n        include: \'*\'\n    enabled-by-default: true\n\nhx:\n  mall:\n    notifyDomain: \'https://hx-test.suokelian.com\'\n    orderTimeOut: 20\n    defaultReceiverTime: 7\n    defaultAppraiseTime: 7\n    logisticsKey: \'xxxxxx\'\n    logisticsCompanyInfos:\n    - code: yuantong\n      name: 圆通速递    \n    - code: yunda\n      name: 韵达快递 \n    - code: zhongtong\n      name: 中通快递\n    - code: jtexpress\n      name: 极兔速递 \n    - code: shunfeng\n      name: 顺丰速运 \n    - code: ems\n      name: EMS \n    - code: jd\n      name: 京东物流  \n    - code: debangkuaidi\n      name: 德邦快递 \n    - code: huitongkuaidi\n      name: 百世快递     \nsms:\n  ali:\n    accessKeyId: LTAI5tEW9V1CF1cN2FJP8Rzb\n    accessKeySecret: lI3tqfFf8z9kI8sG1T7518XN7vAu6b\n    loginTemplateCode: SMS_244665173\n    loginSignName: 环兴商城                                   ', '0eaaa61dd271c29773ae38278f435c85', '2022-10-21 09:51:10', '2022-10-25 14:49:47', 'nacos', '192.168.2.7', '', '', '', '', '', 'yaml', '', '');
INSERT INTO `config_info` VALUES (7, 'huanxing-gateway-dev.yml', 'DEFAULT_GROUP', 'spring:  \n  cloud: \n    gateway:\n      httpclient:\n        max-header-size: 65565\n      routes:\n        - id: huanxing-auth  #指定唯一标识\n          #          uri: http://localhost:6666/ #指定路由服务的地址\n          uri: lb://huanxing-auth   #服务名，实现负载均衡\n          filters:\n            - StripPrefix=1\n            - name: ValidateCodeGatewayFilter\n              args: {}\n            - name: PasswordDecoderGatewayFilter\n            - name: SmsValidateCodeGatewayFilter\n          predicates:\n            - Path=/auth/** #指定路由规则     \n\n        - id: huanxing-upms-admin  #指定唯一标识\n          #          uri: http://localhost:6666/ #指定路由服务的地址\n          uri: lb://huanxing-upms-admin   #服务名，实现负载均衡\n          filters:\n            - StripPrefix=1\n          predicates:\n            - Path=/upms/** #指定路由规则\n\n        - id: huanxing-mall-admin  #指定唯一标识\n          uri: lb://huanxing-mall-admin #指定路由服务的地址\n          #      uri: lb://huanxing-mall   # lb：服务名，实现负载均衡\n          filters:\n            - StripPrefix=1\n          predicates:\n            - Path=/mall/** #指定路由规则\n\n        - id: huanxing-mall-api  #指定唯一标识\n          uri: lb://huanxing-mall-api #指定路由服务的地址\n          #      uri: lb://huanxing-mall   # lb：服务名，实现负载均衡\n          filters:\n            - StripPrefix=1\n          predicates:\n            - Path=/mallapi/** #指定路由规则    \n\n        - id: huanxing-miniapp-weixin  #指定唯一标识\n          uri: lb://huanxing-miniapp-weixin #指定路由服务的地址\n          #      uri: lb://huanxing-weixin-system   # lb：服务名，实现负载均衡\n          filters:\n            - StripPrefix=1\n          predicates:\n            - Path=/weixin/** #指定路由规则    \n\n        - id: huanxing-miniapp-alipay  #指定唯一标识\n          uri: lb://huanxing-miniapp-alipay #指定路由服务的地址\n          #      uri: lb://huanxing-weixin-system   # lb：服务名，实现负载均衡\n          filters:\n            - StripPrefix=1\n          predicates:\n            - Path=/alipay/** #指定路由规则                \n\n        - id: huanxing-pay-api  #指定唯一标识\n          uri: lb://huanxing-pay-api #指定路由服务的地址\n          #      uri: lb://huanxing-pay-api   # lb：服务名，实现负载均衡\n          filters:\n            - StripPrefix=1\n          predicates:\n            - Path=/payapi/** #指定路由规则                   \n      discovery:\n        locator:\n          enabled: true  #开启动态服务名动态获取路由地址\nencode:\n  key: VyBcekSelErhMYN4          ', '2bbc221f10f9b10e88f9a51374501a93', '2022-10-24 06:50:12', '2022-10-25 14:00:29', 'nacos', '192.168.2.7', '', '', '网关', '', '', 'yaml', '', '');
INSERT INTO `config_info` VALUES (10, 'huanxing-auth-dev.yml', 'DEFAULT_GROUP', 'spring:  \r\n  datasource:\r\n    type: com.alibaba.druid.pool.DruidDataSource\r\n    druid:\r\n      driver-class-name: com.mysql.cj.jdbc.Driver\r\n      username: root\r\n      password: Ljx19970310\r\n      url: jdbc:mysql://rm-bp11z9kl01o5qt4453o.mysql.rds.aliyuncs.com:65534/huanxing_upms?characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=GMT%2B8&allowMultiQueries=true&allowPublicKeyRetrieval=true\r\n', '1906ab283505c79ecd4a4101191d9e64', '2022-10-25 13:58:20', '2022-10-25 13:58:20', 'nacos', '192.168.2.7', '', '', '授权', NULL, NULL, 'yaml', NULL, '');
INSERT INTO `config_info` VALUES (11, 'huanxing-mall-admin-dev.yml', 'DEFAULT_GROUP', '\r\nspring:  \r\n  datasource:\r\n    type: com.alibaba.druid.pool.DruidDataSource\r\n    druid:\r\n      driver-class-name: com.mysql.cj.jdbc.Driver\r\n      username: root\r\n      password: Ljx19970310\r\n      url: jdbc:mysql://rm-bp11z9kl01o5qt4453o.mysql.rds.aliyuncs.com:65534/huanxing_mall?characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=GMT%2B8&allowMultiQueries=true&allowPublicKeyRetrieval=true\r\nlogging:\r\n  level:\r\n    com.huanxing.cloud.mall.admin.mapper: debug\r\n#  endpoints config\r\nmanagement:\r\n  endpoint:\r\n    health:\r\n      show-details: ALWAYS\r\n    # 日志记录\r\n    logfile:\r\n      external-file: E:/hx-mall-admin-log/debug.log', '7afbdc6f439eef04a4ae1fad38d86f66', '2022-10-25 13:58:42', '2022-10-25 13:58:42', 'nacos', '192.168.2.7', '', '', '商城admin', NULL, NULL, 'yaml', NULL, '');
INSERT INTO `config_info` VALUES (12, 'huanxing-upms-admin-dev.yml', 'DEFAULT_GROUP', '\nspring:  \n  datasource:\n    type: com.alibaba.druid.pool.DruidDataSource\n    druid:\n      driver-class-name: com.mysql.cj.jdbc.Driver\n      username: root\n      password: Ljx19970310\n      url: jdbc:mysql://rm-bp11z9kl01o5qt4453o.mysql.rds.aliyuncs.com:65534/huanxing_upms?characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=GMT%2B8&allowMultiQueries=true&allowPublicKeyRetrieval=true\nlogging:\n  level:\n    com.huanxing.cloud.upms.admin.mapper: debug\n#  endpoints config\nmanagement:\n  endpoint:\n    health:\n      show-details: ALWAYS\n    # 日志记录\n    logfile:\n      external-file: C:/hx-log/debug.log', '24894ccff4a963d766fe53ab3c5b1689', '2022-10-25 13:58:57', '2022-10-25 14:52:39', 'nacos', '192.168.2.7', '', '', '权限系统', '', '', 'yaml', '', '');
INSERT INTO `config_info` VALUES (13, 'huanxing-mall-api-dev.yml', 'DEFAULT_GROUP', '\r\nlogging:\r\n  level:\r\n    com.huanxing.cloud.mall.api.mapper: debug            \r\nspring:  \r\n  datasource:\r\n    type: com.alibaba.druid.pool.DruidDataSource\r\n    druid:\r\n      driver-class-name: com.mysql.cj.jdbc.Driver\r\n      username: root\r\n      password: Ljx19970310\r\n      url: jdbc:mysql://rm-bp11z9kl01o5qt4453o.mysql.rds.aliyuncs.com:65534/huanxing_mall?characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=GMT%2B8&allowMultiQueries=true&allowPublicKeyRetrieval=true\r\nxxl: \r\n job: \r\n   adminAddresses: http://124.223.202.234:7002/xxl-job-admin\r\n   appName: mall-api\r\n   ip: 120.46.176.236\r\n   port: 7890\r\n   accessToken: \r\n   logPath: /root/huanxing/xxl-job/jobhandler\r\n   logRetentionDays: 30', '6e363fe9852f6adb265ef86d56b62ae4', '2022-10-25 13:59:09', '2022-10-25 13:59:09', 'nacos', '192.168.2.7', '', '', '商城api', NULL, NULL, 'yaml', NULL, '');
INSERT INTO `config_info` VALUES (14, 'huanxing-pay-api-dev.yml', 'DEFAULT_GROUP', 'cert-dir: \r\n   windows: D:\\\r\n   linux: /root/huanxing/cert\r\nlogging:\r\n  level:\r\n    com.huanxing.cloud.pay.api.mapper: debug          \r\nspring:  \r\n  datasource:\r\n    type: com.alibaba.druid.pool.DruidDataSource\r\n    druid:\r\n      driver-class-name: com.mysql.cj.jdbc.Driver\r\n      username: root\r\n      password: Ljx19970310\r\n      url: jdbc:mysql://rm-bp11z9kl01o5qt4453o.mysql.rds.aliyuncs.com:65534/huanxing_pay?characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=GMT%2B8&allowMultiQueries=true&allowPublicKeyRetrieval=true', '7e0cf89981939505e423fbd1741996cb', '2022-10-25 13:59:30', '2022-10-25 13:59:30', 'nacos', '192.168.2.7', '', '', '支付', NULL, NULL, 'yaml', NULL, '');
INSERT INTO `config_info` VALUES (15, 'huanxing-monitor-dev.yml', 'DEFAULT_GROUP', 'spring:\r\n  security:\r\n    user:\r\n      name: \"admin\"\r\n      password: \"ljx19970310\"', '4358b2debd1c621f0691e1cde84dd437', '2022-10-25 13:59:41', '2022-10-25 13:59:41', 'nacos', '192.168.2.7', '', '', '监控', NULL, NULL, 'yaml', NULL, '');
INSERT INTO `config_info` VALUES (16, 'huanxing-miniapp-weixin-dev.yml', 'DEFAULT_GROUP', '\r\nlogging:\r\n  level:\r\n    com.huanxing.cloud.weixin.api.mapper: debug        \r\nspring:  \r\n  datasource:\r\n    type: com.alibaba.druid.pool.DruidDataSource\r\n    druid:\r\n      driver-class-name: com.mysql.cj.jdbc.Driver\r\n      username: root\r\n      password: Ljx19970310\r\n      url: jdbc:mysql://rm-bp11z9kl01o5qt4453o.mysql.rds.aliyuncs.com:65534/huanxing_miniapp?characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=GMT%2B8&allowMultiQueries=true&allowPublicKeyRetrieval=true\r\n', 'a75dce5ad1692b13e387a55531eb4d03', '2022-10-25 13:59:56', '2022-10-25 13:59:56', 'nacos', '192.168.2.7', '', '', '小程序-微信', NULL, NULL, 'yaml', NULL, '');
INSERT INTO `config_info` VALUES (17, 'huanxing-miniapp-alipay-dev.yml', 'DEFAULT_GROUP', '\r\nlogging:\r\n  level:\r\n    com.huanxing.cloud.weixin.api.mapper: debug        \r\nspring:  \r\n  datasource:\r\n    type: com.alibaba.druid.pool.DruidDataSource\r\n    druid:\r\n      driver-class-name: com.mysql.cj.jdbc.Driver\r\n      username: root\r\n      password: Ljx19970310\r\n      url: jdbc:mysql://rm-bp11z9kl01o5qt4453o.mysql.rds.aliyuncs.com:65534/huanxing_miniapp?characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=GMT%2B8&allowMultiQueries=true&allowPublicKeyRetrieval=true\r\n', 'a75dce5ad1692b13e387a55531eb4d03', '2022-10-25 14:00:10', '2022-10-25 14:00:10', 'nacos', '192.168.2.7', '', '', '小程序-支付宝', NULL, NULL, 'yaml', NULL, '');

-- ----------------------------
-- Table structure for config_info_aggr
-- ----------------------------
DROP TABLE IF EXISTS `config_info_aggr`;
CREATE TABLE `config_info_aggr`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'group_id',
  `datum_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'datum_id',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '内容',
  `gmt_modified` datetime(0) NOT NULL COMMENT '修改时间',
  `app_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '租户字段',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_configinfoaggr_datagrouptenantdatum`(`data_id`, `group_id`, `tenant_id`, `datum_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '增加租户字段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of config_info_aggr
-- ----------------------------

-- ----------------------------
-- Table structure for config_info_beta
-- ----------------------------
DROP TABLE IF EXISTS `config_info_beta`;
CREATE TABLE `config_info_beta`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'group_id',
  `app_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'app_name',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'content',
  `beta_ips` varchar(1024) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'betaIps',
  `md5` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'md5',
  `gmt_create` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `gmt_modified` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `src_user` text CHARACTER SET utf8 COLLATE utf8_bin NULL COMMENT 'source user',
  `src_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'source ip',
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '租户字段',
  `encrypted_data_key` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '秘钥',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_configinfobeta_datagrouptenant`(`data_id`, `group_id`, `tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'config_info_beta' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of config_info_beta
-- ----------------------------

-- ----------------------------
-- Table structure for config_info_tag
-- ----------------------------
DROP TABLE IF EXISTS `config_info_tag`;
CREATE TABLE `config_info_tag`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'group_id',
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT 'tenant_id',
  `tag_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'tag_id',
  `app_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'app_name',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'content',
  `md5` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'md5',
  `gmt_create` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `gmt_modified` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `src_user` text CHARACTER SET utf8 COLLATE utf8_bin NULL COMMENT 'source user',
  `src_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'source ip',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_configinfotag_datagrouptenanttag`(`data_id`, `group_id`, `tenant_id`, `tag_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'config_info_tag' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of config_info_tag
-- ----------------------------

-- ----------------------------
-- Table structure for config_tags_relation
-- ----------------------------
DROP TABLE IF EXISTS `config_tags_relation`;
CREATE TABLE `config_tags_relation`  (
  `id` bigint(20) NOT NULL COMMENT 'id',
  `tag_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'tag_name',
  `tag_type` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'tag_type',
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'group_id',
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT 'tenant_id',
  `nid` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`nid`) USING BTREE,
  UNIQUE INDEX `uk_configtagrelation_configidtag`(`id`, `tag_name`, `tag_type`) USING BTREE,
  INDEX `idx_tenant_id`(`tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'config_tag_relation' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of config_tags_relation
-- ----------------------------

-- ----------------------------
-- Table structure for group_capacity
-- ----------------------------
DROP TABLE IF EXISTS `group_capacity`;
CREATE TABLE `group_capacity`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT 'Group ID，空字符表示整个集群',
  `quota` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '配额，0表示使用默认值',
  `usage` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '使用量',
  `max_size` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单个配置大小上限，单位为字节，0表示使用默认值',
  `max_aggr_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '聚合子配置最大个数，，0表示使用默认值',
  `max_aggr_size` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单个聚合数据的子配置大小上限，单位为字节，0表示使用默认值',
  `max_history_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '最大变更历史数量',
  `gmt_create` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `gmt_modified` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_group_id`(`group_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '集群、各Group容量信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of group_capacity
-- ----------------------------

-- ----------------------------
-- Table structure for his_config_info
-- ----------------------------
DROP TABLE IF EXISTS `his_config_info`;
CREATE TABLE `his_config_info`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nid` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `app_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'app_name',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `md5` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `gmt_create` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `gmt_modified` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `src_user` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `src_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `op_type` char(10) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '租户字段',
  `encrypted_data_key` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '秘钥',
  PRIMARY KEY (`nid`) USING BTREE,
  INDEX `idx_gmt_create`(`gmt_create`) USING BTREE,
  INDEX `idx_gmt_modified`(`gmt_modified`) USING BTREE,
  INDEX `idx_did`(`data_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '多租户改造' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of his_config_info
-- ----------------------------
INSERT INTO `his_config_info` VALUES (0, 2, 'application-dev.yml', 'DEFAULT_GROUP', '', 'spring:\r\n  redis:\r\n   host: huanxing-redis\r\n   port: 6379\r\n   database: 10\r\nservlet:\r\n  multipart:\r\n   location: /data/tmp\r\njackson:\r\n  default-property-inclusion: always\r\n  date-format: yyyy-mm-dd hh:mm:ss \r\n# Sa-Token配置\r\nsa-token:\r\n  # token名称 (同时也是cookie名称)\r\n  token-name: satoken\r\n\r\n', 'c05cb171e2c877c7669f9df2f07d0917', '2022-10-21 17:51:10', '2022-10-21 09:51:10', 'nacos', '192.168.101.93', 'I', '', '');
INSERT INTO `his_config_info` VALUES (2, 3, 'application-dev.yml', 'DEFAULT_GROUP', '', 'spring:\r\n  redis:\r\n   host: huanxing-redis\r\n   port: 6379\r\n   database: 10\r\nservlet:\r\n  multipart:\r\n   location: /data/tmp\r\njackson:\r\n  default-property-inclusion: always\r\n  date-format: yyyy-mm-dd hh:mm:ss \r\n# Sa-Token配置\r\nsa-token:\r\n  # token名称 (同时也是cookie名称)\r\n  token-name: satoken\r\n\r\n', 'c05cb171e2c877c7669f9df2f07d0917', '2022-10-24 14:12:25', '2022-10-24 06:12:24', 'nacos', '192.168.101.96', 'U', '', '');
INSERT INTO `his_config_info` VALUES (2, 4, 'application-dev.yml', 'DEFAULT_GROUP', '', 'spring:\n  redis:\n    host: huanxing-redis\n    port: 6379\n    database: 10\n    password: ljx19970310.\nservlet:\n  multipart:\n    location: /data/tmp\nsecure:\n  ignore:\n    urls:\n      - /actuator/**\n      - /mallapi/**\n      - /auth/token/login\n      - /auth/token/phone/login\n      - /auth/token/page\n      - /code\n      - /auth/phone/code    \n      - /upms/user/check/phone   \njackson:\n  default-property-inclusion: always\n  date-format: yyyy-mm-dd hh:mm:ss \nrocketmq:\n# name-server: 59.110.30.161:9876    \n  name-server: 127.0.0.1:9876\n  producer: \n  group: producer_group\n  retry-times-when-send-failed: 5\n  send-message-timeout: 5000\n# Sa-Token配置\nsa-token:\n  # token名称 (同时也是cookie名称)\n  token-name: satoken\n  # token有效期，单位s 默认30天, -1代表永不过期\n  timeout: 2592000\n  # token临时有效期 (指定时间内无操作就视为token过期) 单位: 秒\n  activity-timeout: -1\n  # 是否允许同一账号并发登录 (为true时允许一起登录, 为false时新登录挤掉旧登录) \n  is-concurrent: true\n  # 在多人登录同一账号时，是否共用一个token (为true时所有登录共用一个token, 为false时每次登录新建一个token)\n  is-share: false\n  # 是否从cookie中读取token\n  is-read-cookie: false\n  # token风格\n  token-style: uuid\n  # 是否输出操作日志 \n  is-log: true \n   # jwt秘钥 \n  jwt-secret-key: asdasdasifhueuiwyurfewbfjsdafjksdadad   \n\n\n\n\n\n', '510ab3240b951cf5169037b6fa2f6d9a', '2022-10-24 14:45:04', '2022-10-24 06:45:04', 'nacos', '192.168.101.96', 'U', '', '');
INSERT INTO `his_config_info` VALUES (2, 5, 'application-dev.yml', 'DEFAULT_GROUP', '', 'spring:\n  redis:\n    host: huanxing-redis\n    port: 6379\n    database: 10\n    password: ljx19970310.\nservlet:\n  multipart:\n    location: /data/tmp\nnmanagement:\n  endpoints:\n    web:\n      exposure:\n        include: *\n  enabled-by-default: true\nsecure:\n  ignore:\n    urls:\n      - /actuator/**\n      - /mallapi/**\n      - /auth/token/login\n      - /auth/token/phone/login\n      - /auth/token/page\n      - /code\n      - /auth/phone/code    \n      - /upms/user/check/phone   \njackson:\n  default-property-inclusion: always\n  date-format: yyyy-mm-dd hh:mm:ss \nmybatis-plus: \n  mapper-locations: classpath:/mapper/*Mapper.xml\n  type-handlers-package: com.joolun.cloud.common.core.mybatis.typehandler\n  global-config: \n  sql-parser-cache: true\n  banner: false\n  db-config:\n  id-type: auto\n  select-strategy: not_empty\n\nrocketmq:\n# name-server: 59.110.30.161:9876    \n  name-server: 127.0.0.1:9876\n  producer: \n  group: producer_group\n  retry-times-when-send-failed: 5\n  send-message-timeout: 5000\n# Sa-Token配置\nsa-token:\n  # token名称 (同时也是cookie名称)\n  token-name: satoken\n  # token有效期，单位s 默认30天, -1代表永不过期\n  timeout: 2592000\n  # token临时有效期 (指定时间内无操作就视为token过期) 单位: 秒\n  activity-timeout: -1\n  # 是否允许同一账号并发登录 (为true时允许一起登录, 为false时新登录挤掉旧登录) \n  is-concurrent: true\n  # 在多人登录同一账号时，是否共用一个token (为true时所有登录共用一个token, 为false时每次登录新建一个token)\n  is-share: false\n  # 是否从cookie中读取token\n  is-read-cookie: false\n  # token风格\n  token-style: uuid\n  # 是否输出操作日志 \n  is-log: true \n   # jwt秘钥 \n  jwt-secret-key: asdasdasifhueuiwyurfewbfjsdafjksdadad   \n\nhx:\n  mall:\n    notifyDomain: https://hx-test.suokelian.com\n    orderTimeOut: 20\n    defaultReceiverTime: 7\n    defaultAppraiseTime: 7\n    logisticsKey: xxxxxx\n    logisticsCompanyInfos: \n      - code: yuantong\n        name: 圆通速递  \n      - code: yunda\n        name: 韵达快递\n      - code: zhongtong\n        name: 中通快递\n      - code: jtexpress\n        name: 极兔速递\n      - code: shunfeng\n        name: 顺丰速运\n      - code: ems\n        name: EMS\n      - code: jd\n        name: 京东物流\n      - code: debangkuaidi\n        name: 德邦快递\n      - code: huitongkuaidi\n        name: 百世快递\nsms:\n  ali:\n    accessKeyId: LTAI5tEW9V1CF1cN2FJP8Rzb\n    accessKeySecret: lI3tqfFf8z9kI8sG1T7518XN7vAu6b\n    loginTemplateCode: SMS_244665173\n    loginSignName: 环兴商城                                   \n\n\n\n     \n\n\n\n\n\n', 'd266676c51a0c1a77cb645dad3ce8647', '2022-10-24 14:46:04', '2022-10-24 06:46:04', 'nacos', '192.168.101.96', 'U', '', '');
INSERT INTO `his_config_info` VALUES (2, 6, 'application-dev.yml', 'DEFAULT_GROUP', '', 'spring:\n  redis:\n    host: huanxing-redis\n    port: 6379\n    database: 10\n    password: ljx19970310.\nservlet:\n  multipart:\n    location: /data/tmp\nnmanagement:\n  endpoints:\n    web:\n      exposure:\n        include: *\n  enabled-by-default: true\nsecure:\n  ignore:\n    urls:\n      - /actuator/**\n      - /mallapi/**\n      - /auth/token/login\n      - /auth/token/phone/login\n      - /auth/token/page\n      - /code\n      - /auth/phone/code    \n      - /upms/user/check/phone\n         \njackson:\n  default-property-inclusion: always\n  date-format: yyyy-mm-dd hh:mm:ss \n\nmybatis-plus: \n  mapper-locations: classpath:/mapper/*Mapper.xml\n  type-handlers-package: com.joolun.cloud.common.core.mybatis.typehandler\n  global-config: \n  sql-parser-cache: true\n  banner: false\n  db-config:\n  id-type: auto\n  select-strategy: not_empty\n\nrocketmq:\n# name-server: 59.110.30.161:9876    \n  name-server: 127.0.0.1:9876\n  producer: \n  group: producer_group\n  retry-times-when-send-failed: 5\n  send-message-timeout: 5000\n# Sa-Token配置\nsa-token:\n  # token名称 (同时也是cookie名称)\n  token-name: satoken\n  # token有效期，单位s 默认30天, -1代表永不过期\n  timeout: 2592000\n  # token临时有效期 (指定时间内无操作就视为token过期) 单位: 秒\n  activity-timeout: -1\n  # 是否允许同一账号并发登录 (为true时允许一起登录, 为false时新登录挤掉旧登录) \n  is-concurrent: true\n  # 在多人登录同一账号时，是否共用一个token (为true时所有登录共用一个token, 为false时每次登录新建一个token)\n  is-share: false\n  # 是否从cookie中读取token\n  is-read-cookie: false\n  # token风格\n  token-style: uuid\n  # 是否输出操作日志 \n  is-log: true \n   # jwt秘钥 \n  jwt-secret-key: asdasdasifhueuiwyurfewbfjsdafjksdadad   \n\nhx:\n  mall:\n    notifyDomain: https://hx-test.suokelian.com\n    orderTimeOut: 20\n    defaultReceiverTime: 7\n    defaultAppraiseTime: 7\n    logisticsKey: xxxxxx\n    logisticsCompanyInfos: \n      - code: yuantong\n        name: 圆通速递  \n      - code: yunda\n        name: 韵达快递\n      - code: zhongtong\n        name: 中通快递\n      - code: jtexpress\n        name: 极兔速递\n      - code: shunfeng\n        name: 顺丰速运\n      - code: ems\n        name: EMS\n      - code: jd\n        name: 京东物流\n      - code: debangkuaidi\n        name: 德邦快递\n      - code: huitongkuaidi\n        name: 百世快递\nsms:\n  ali:\n    accessKeyId: LTAI5tEW9V1CF1cN2FJP8Rzb\n    accessKeySecret: lI3tqfFf8z9kI8sG1T7518XN7vAu6b\n    loginTemplateCode: SMS_244665173\n    loginSignName: 环兴商城                                   \n\n\n\n     \n\n\n\n\n\n', '092a6d39fac6792c56f873187288482a', '2022-10-24 14:47:50', '2022-10-24 06:47:50', 'nacos', '192.168.101.96', 'U', '', '');
INSERT INTO `his_config_info` VALUES (0, 7, 'huanxing-gateway-dev.yml', 'DEFAULT_GROUP', '', 'huanxing-gateway-dev.yml\', \'DEFAULT_GROUP\', \'spring:  \\n  cloud: \\n    gateway:\\n      httpclient:\\n        max-header-size: 65565\\n      routes:\\n        - id: huanxing-auth  #指定唯一标识\\n          #          uri: http://localhost:6666/ #指定路由服务的地址\\n          uri: lb://huanxing-auth   #服务名，实现负载均衡\\n          filters:\\n            - StripPrefix=1\\n            - name: ValidateCodeGatewayFilter\\n              args: {}\\n            - name: PasswordDecoderGatewayFilter\\n            - name: SmsValidateCodeGatewayFilter\\n          predicates:\\n            - Path=/auth/** #指定路由规则     \\n\\n        - id: huanxing-upms-admin  #指定唯一标识\\n          #          uri: http://localhost:6666/ #指定路由服务的地址\\n          uri: lb://huanxing-upms-admin   #服务名，实现负载均衡\\n          filters:\\n            - StripPrefix=1\\n          predicates:\\n            - Path=/upms/** #指定路由规则\\n\\n        - id: huanxing-mall-admin  #指定唯一标识\\n          uri: lb://huanxing-mall-admin #指定路由服务的地址\\n          #      uri: lb://huanxing-mall   # lb：服务名，实现负载均衡\\n          filters:\\n            - StripPrefix=1\\n          predicates:\\n            - Path=/mall/** #指定路由规则\\n\\n        - id: huanxing-mall-api  #指定唯一标识\\n          uri: lb://huanxing-mall-api #指定路由服务的地址\\n          #      uri: lb://huanxing-mall   # lb：服务名，实现负载均衡\\n          filters:\\n            - StripPrefix=1\\n          predicates:\\n            - Path=/mallapi/** #指定路由规则    \\n\\n        - id: huanxing-miniapp-weixin  #指定唯一标识\\n          uri: lb://huanxing-miniapp-weixin #指定路由服务的地址\\n          #      uri: lb://huanxing-weixin-system   # lb：服务名，实现负载均衡\\n          filters:\\n            - StripPrefix=1\\n          predicates:\\n            - Path=/weixin/** #指定路由规则    \\n\\n        - id: huanxing-miniapp-alipay  #指定唯一标识\\n          uri: lb://huanxing-miniapp-alipay #指定路由服务的地址\\n          #      uri: lb://huanxing-weixin-system   # lb：服务名，实现负载均衡\\n          filters:\\n            - StripPrefix=1\\n          predicates:\\n            - Path=/alipay/** #指定路由规则                \\n\\n        - id: huanxing-pay-api  #指定唯一标识\\n          uri: lb://huanxing-pay-api #指定路由服务的地址\\n          #      uri: lb://huanxing-pay-api   # lb：服务名，实现负载均衡\\n          filters:\\n            - StripPrefix=1\\n          predicates:\\n            - Path=/payapi/** #指定路由规则                   \\n      discovery:\\n        locator:\\n          enabled: true  #开启动态服务名动态获取路由地址\\nencode:\\n  key: VyBcekSelErhMYN4          ', 'd7cf2f9b284d060b4d5161687fd33af6', '2022-10-24 14:50:13', '2022-10-24 06:50:12', 'nacos', '192.168.101.96', 'I', '', '');
INSERT INTO `his_config_info` VALUES (7, 8, 'huanxing-gateway-dev.yml', 'DEFAULT_GROUP', '', 'huanxing-gateway-dev.yml\', \'DEFAULT_GROUP\', \'spring:  \\n  cloud: \\n    gateway:\\n      httpclient:\\n        max-header-size: 65565\\n      routes:\\n        - id: huanxing-auth  #指定唯一标识\\n          #          uri: http://localhost:6666/ #指定路由服务的地址\\n          uri: lb://huanxing-auth   #服务名，实现负载均衡\\n          filters:\\n            - StripPrefix=1\\n            - name: ValidateCodeGatewayFilter\\n              args: {}\\n            - name: PasswordDecoderGatewayFilter\\n            - name: SmsValidateCodeGatewayFilter\\n          predicates:\\n            - Path=/auth/** #指定路由规则     \\n\\n        - id: huanxing-upms-admin  #指定唯一标识\\n          #          uri: http://localhost:6666/ #指定路由服务的地址\\n          uri: lb://huanxing-upms-admin   #服务名，实现负载均衡\\n          filters:\\n            - StripPrefix=1\\n          predicates:\\n            - Path=/upms/** #指定路由规则\\n\\n        - id: huanxing-mall-admin  #指定唯一标识\\n          uri: lb://huanxing-mall-admin #指定路由服务的地址\\n          #      uri: lb://huanxing-mall   # lb：服务名，实现负载均衡\\n          filters:\\n            - StripPrefix=1\\n          predicates:\\n            - Path=/mall/** #指定路由规则\\n\\n        - id: huanxing-mall-api  #指定唯一标识\\n          uri: lb://huanxing-mall-api #指定路由服务的地址\\n          #      uri: lb://huanxing-mall   # lb：服务名，实现负载均衡\\n          filters:\\n            - StripPrefix=1\\n          predicates:\\n            - Path=/mallapi/** #指定路由规则    \\n\\n        - id: huanxing-miniapp-weixin  #指定唯一标识\\n          uri: lb://huanxing-miniapp-weixin #指定路由服务的地址\\n          #      uri: lb://huanxing-weixin-system   # lb：服务名，实现负载均衡\\n          filters:\\n            - StripPrefix=1\\n          predicates:\\n            - Path=/weixin/** #指定路由规则    \\n\\n        - id: huanxing-miniapp-alipay  #指定唯一标识\\n          uri: lb://huanxing-miniapp-alipay #指定路由服务的地址\\n          #      uri: lb://huanxing-weixin-system   # lb：服务名，实现负载均衡\\n          filters:\\n            - StripPrefix=1\\n          predicates:\\n            - Path=/alipay/** #指定路由规则                \\n\\n        - id: huanxing-pay-api  #指定唯一标识\\n          uri: lb://huanxing-pay-api #指定路由服务的地址\\n          #      uri: lb://huanxing-pay-api   # lb：服务名，实现负载均衡\\n          filters:\\n            - StripPrefix=1\\n          predicates:\\n            - Path=/payapi/** #指定路由规则                   \\n      discovery:\\n        locator:\\n          enabled: true  #开启动态服务名动态获取路由地址\\nencode:\\n  key: VyBcekSelErhMYN4          ', 'd7cf2f9b284d060b4d5161687fd33af6', '2022-10-24 15:31:49', '2022-10-24 07:31:48', 'nacos', '192.168.101.96', 'U', '', '');
INSERT INTO `his_config_info` VALUES (7, 9, 'huanxing-gateway-dev.yml', 'DEFAULT_GROUP', '', 'spring:  \\n  cloud: \\n    gateway:\\n      httpclient:\\n        max-header-size: 65565\\n      routes:\\n        - id: huanxing-auth  #指定唯一标识\\n          #          uri: http://localhost:6666/ #指定路由服务的地址\\n          uri: lb://huanxing-auth   #服务名，实现负载均衡\\n          filters:\\n            - StripPrefix=1\\n            - name: ValidateCodeGatewayFilter\\n              args: {}\\n            - name: PasswordDecoderGatewayFilter\\n            - name: SmsValidateCodeGatewayFilter\\n          predicates:\\n            - Path=/auth/** #指定路由规则     \\n\\n        - id: huanxing-upms-admin  #指定唯一标识\\n          #          uri: http://localhost:6666/ #指定路由服务的地址\\n          uri: lb://huanxing-upms-admin   #服务名，实现负载均衡\\n          filters:\\n            - StripPrefix=1\\n          predicates:\\n            - Path=/upms/** #指定路由规则\\n\\n        - id: huanxing-mall-admin  #指定唯一标识\\n          uri: lb://huanxing-mall-admin #指定路由服务的地址\\n          #      uri: lb://huanxing-mall   # lb：服务名，实现负载均衡\\n          filters:\\n            - StripPrefix=1\\n          predicates:\\n            - Path=/mall/** #指定路由规则\\n\\n        - id: huanxing-mall-api  #指定唯一标识\\n          uri: lb://huanxing-mall-api #指定路由服务的地址\\n          #      uri: lb://huanxing-mall   # lb：服务名，实现负载均衡\\n          filters:\\n            - StripPrefix=1\\n          predicates:\\n            - Path=/mallapi/** #指定路由规则    \\n\\n        - id: huanxing-miniapp-weixin  #指定唯一标识\\n          uri: lb://huanxing-miniapp-weixin #指定路由服务的地址\\n          #      uri: lb://huanxing-weixin-system   # lb：服务名，实现负载均衡\\n          filters:\\n            - StripPrefix=1\\n          predicates:\\n            - Path=/weixin/** #指定路由规则    \\n\\n        - id: huanxing-miniapp-alipay  #指定唯一标识\\n          uri: lb://huanxing-miniapp-alipay #指定路由服务的地址\\n          #      uri: lb://huanxing-weixin-system   # lb：服务名，实现负载均衡\\n          filters:\\n            - StripPrefix=1\\n          predicates:\\n            - Path=/alipay/** #指定路由规则                \\n\\n        - id: huanxing-pay-api  #指定唯一标识\\n          uri: lb://huanxing-pay-api #指定路由服务的地址\\n          #      uri: lb://huanxing-pay-api   # lb：服务名，实现负载均衡\\n          filters:\\n            - StripPrefix=1\\n          predicates:\\n            - Path=/payapi/** #指定路由规则                   \\n      discovery:\\n        locator:\\n          enabled: true  #开启动态服务名动态获取路由地址\\nencode:\\n  key: VyBcekSelErhMYN4          ', 'd7f879b22a620ba5a12363b531b53d05', '2022-10-25 21:57:57', '2022-10-25 13:57:57', 'nacos', '192.168.2.7', 'U', '', '');
INSERT INTO `his_config_info` VALUES (0, 10, 'huanxing-auth-dev.yml', 'DEFAULT_GROUP', '', 'spring:  \r\n  datasource:\r\n    type: com.alibaba.druid.pool.DruidDataSource\r\n    druid:\r\n      driver-class-name: com.mysql.cj.jdbc.Driver\r\n      username: root\r\n      password: Ljx19970310\r\n      url: jdbc:mysql://rm-bp11z9kl01o5qt4453o.mysql.rds.aliyuncs.com:65534/huanxing_upms?characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=GMT%2B8&allowMultiQueries=true&allowPublicKeyRetrieval=true\r\n', '1906ab283505c79ecd4a4101191d9e64', '2022-10-25 21:58:20', '2022-10-25 13:58:20', 'nacos', '192.168.2.7', 'I', '', '');
INSERT INTO `his_config_info` VALUES (0, 11, 'huanxing-mall-admin-dev.yml', 'DEFAULT_GROUP', '', '\r\nspring:  \r\n  datasource:\r\n    type: com.alibaba.druid.pool.DruidDataSource\r\n    druid:\r\n      driver-class-name: com.mysql.cj.jdbc.Driver\r\n      username: root\r\n      password: Ljx19970310\r\n      url: jdbc:mysql://rm-bp11z9kl01o5qt4453o.mysql.rds.aliyuncs.com:65534/huanxing_mall?characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=GMT%2B8&allowMultiQueries=true&allowPublicKeyRetrieval=true\r\nlogging:\r\n  level:\r\n    com.huanxing.cloud.mall.admin.mapper: debug\r\n#  endpoints config\r\nmanagement:\r\n  endpoint:\r\n    health:\r\n      show-details: ALWAYS\r\n    # 日志记录\r\n    logfile:\r\n      external-file: E:/hx-mall-admin-log/debug.log', '7afbdc6f439eef04a4ae1fad38d86f66', '2022-10-25 21:58:42', '2022-10-25 13:58:42', 'nacos', '192.168.2.7', 'I', '', '');
INSERT INTO `his_config_info` VALUES (0, 12, 'huanxing-upms-admin-dev.yml', 'DEFAULT_GROUP', '', '\r\nspring:  \r\n  datasource:\r\n    type: com.alibaba.druid.pool.DruidDataSource\r\n    druid:\r\n      driver-class-name: com.mysql.cj.jdbc.Driver\r\n      username: root\r\n      password: Ljx19970310\r\n      url: jdbc:mysql://rm-bp11z9kl01o5qt4453o.mysql.rds.aliyuncs.com:65534/huanxing_upms?characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=GMT%2B8&allowMultiQueries=true&allowPublicKeyRetrieval=true\r\nlogging:\r\n  level:\r\n    com.huanxing.cloud.upms.admin.mapper: debug\r\n#  endpoints config\r\nmanagement:\r\n  endpoint:\r\n    health:\r\n      show-details: ALWAYS\r\n    # 日志记录\r\n    logfile:\r\n      external-file: E:/hx-log/debug.log', 'ebbd7d6f040e39eae2b77a58474439dd', '2022-10-25 21:58:57', '2022-10-25 13:58:57', 'nacos', '192.168.2.7', 'I', '', '');
INSERT INTO `his_config_info` VALUES (0, 13, 'huanxing-mall-api-dev.yml', 'DEFAULT_GROUP', '', '\r\nlogging:\r\n  level:\r\n    com.huanxing.cloud.mall.api.mapper: debug            \r\nspring:  \r\n  datasource:\r\n    type: com.alibaba.druid.pool.DruidDataSource\r\n    druid:\r\n      driver-class-name: com.mysql.cj.jdbc.Driver\r\n      username: root\r\n      password: Ljx19970310\r\n      url: jdbc:mysql://rm-bp11z9kl01o5qt4453o.mysql.rds.aliyuncs.com:65534/huanxing_mall?characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=GMT%2B8&allowMultiQueries=true&allowPublicKeyRetrieval=true\r\nxxl: \r\n job: \r\n   adminAddresses: http://124.223.202.234:7002/xxl-job-admin\r\n   appName: mall-api\r\n   ip: 120.46.176.236\r\n   port: 7890\r\n   accessToken: \r\n   logPath: /root/huanxing/xxl-job/jobhandler\r\n   logRetentionDays: 30', '6e363fe9852f6adb265ef86d56b62ae4', '2022-10-25 21:59:09', '2022-10-25 13:59:09', 'nacos', '192.168.2.7', 'I', '', '');
INSERT INTO `his_config_info` VALUES (0, 14, 'huanxing-pay-api-dev.yml', 'DEFAULT_GROUP', '', 'cert-dir: \r\n   windows: D:\\\r\n   linux: /root/huanxing/cert\r\nlogging:\r\n  level:\r\n    com.huanxing.cloud.pay.api.mapper: debug          \r\nspring:  \r\n  datasource:\r\n    type: com.alibaba.druid.pool.DruidDataSource\r\n    druid:\r\n      driver-class-name: com.mysql.cj.jdbc.Driver\r\n      username: root\r\n      password: Ljx19970310\r\n      url: jdbc:mysql://rm-bp11z9kl01o5qt4453o.mysql.rds.aliyuncs.com:65534/huanxing_pay?characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=GMT%2B8&allowMultiQueries=true&allowPublicKeyRetrieval=true', '7e0cf89981939505e423fbd1741996cb', '2022-10-25 21:59:30', '2022-10-25 13:59:30', 'nacos', '192.168.2.7', 'I', '', '');
INSERT INTO `his_config_info` VALUES (0, 15, 'huanxing-monitor-dev.yml', 'DEFAULT_GROUP', '', 'spring:\r\n  security:\r\n    user:\r\n      name: \"admin\"\r\n      password: \"ljx19970310\"', '4358b2debd1c621f0691e1cde84dd437', '2022-10-25 21:59:41', '2022-10-25 13:59:41', 'nacos', '192.168.2.7', 'I', '', '');
INSERT INTO `his_config_info` VALUES (0, 16, 'huanxing-miniapp-weixin-dev.yml', 'DEFAULT_GROUP', '', '\r\nlogging:\r\n  level:\r\n    com.huanxing.cloud.weixin.api.mapper: debug        \r\nspring:  \r\n  datasource:\r\n    type: com.alibaba.druid.pool.DruidDataSource\r\n    druid:\r\n      driver-class-name: com.mysql.cj.jdbc.Driver\r\n      username: root\r\n      password: Ljx19970310\r\n      url: jdbc:mysql://rm-bp11z9kl01o5qt4453o.mysql.rds.aliyuncs.com:65534/huanxing_miniapp?characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=GMT%2B8&allowMultiQueries=true&allowPublicKeyRetrieval=true\r\n', 'a75dce5ad1692b13e387a55531eb4d03', '2022-10-25 21:59:55', '2022-10-25 13:59:56', 'nacos', '192.168.2.7', 'I', '', '');
INSERT INTO `his_config_info` VALUES (0, 17, 'huanxing-miniapp-alipay-dev.yml', 'DEFAULT_GROUP', '', '\r\nlogging:\r\n  level:\r\n    com.huanxing.cloud.weixin.api.mapper: debug        \r\nspring:  \r\n  datasource:\r\n    type: com.alibaba.druid.pool.DruidDataSource\r\n    druid:\r\n      driver-class-name: com.mysql.cj.jdbc.Driver\r\n      username: root\r\n      password: Ljx19970310\r\n      url: jdbc:mysql://rm-bp11z9kl01o5qt4453o.mysql.rds.aliyuncs.com:65534/huanxing_miniapp?characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=GMT%2B8&allowMultiQueries=true&allowPublicKeyRetrieval=true\r\n', 'a75dce5ad1692b13e387a55531eb4d03', '2022-10-25 22:00:10', '2022-10-25 14:00:10', 'nacos', '192.168.2.7', 'I', '', '');
INSERT INTO `his_config_info` VALUES (7, 18, 'huanxing-gateway-dev.yml', 'DEFAULT_GROUP', '', '           - StripPrefix=1\n          predicates:\n            - Path=/payapi/** #指定路由规则                   \n      discovery:\n        locator:\n          enabled: true  #开启动态服务名动态获取路由地址\nencode:\n  key: VyBcekSelErhMYN4          ', 'a040dfa578d06b5854b8c09442c07869', '2022-10-25 22:00:29', '2022-10-25 14:00:29', 'nacos', '192.168.2.7', 'U', '', '');
INSERT INTO `his_config_info` VALUES (2, 19, 'application-dev.yml', 'DEFAULT_GROUP', '', 'spring:\n  redis:\n    host: huanxing-redis\n    port: 6379\n    database: 10\n    password: ljx19970310.\nservlet:\n  multipart:\n    location: /data/tmp\nnmanagement:\n  endpoints:\n    web:\n      exposure:\n        include: *\n  enabled-by-default: true\nsecure:\n  ignore:\n    urls:\n      - /actuator/**\n      - /mallapi/**\n      - /auth/token/login\n      - /auth/token/phone/login\n      - /auth/token/page\n      - /code\n      - /auth/phone/code    \n      - /upms/user/check/phone\n         \njackson:\n  default-property-inclusion: always\n  date-format: yyyy-mm-dd hh:mm:ss \n\nmybatis-plus: \n  mapper-locations: classpath:/mapper/*Mapper.xml\n  type-handlers-package: com.joolun.cloud.common.core.mybatis.typehandler\n  global-config: \n  sql-parser-cache: true\n  banner: false\n  db-config:\n  id-type: auto\n  select-strategy: not_empty\n\nrocketmq:\n# name-server: 59.110.30.161:9876    \n  name-server: 127.0.0.1:9876\n  producer: \n  group: producer_group\n  retry-times-when-send-failed: 5\n  send-message-timeout: 5000\n# Sa-Token配置\nsa-token:\n  # token名称 (同时也是cookie名称)\n  token-name: satoken\n  # token有效期，单位s 默认30天, -1代表永不过期\n  timeout: 2592000\n  # token临时有效期 (指定时间内无操作就视为token过期) 单位: 秒\n  activity-timeout: -1\n  # 是否允许同一账号并发登录 (为true时允许一起登录, 为false时新登录挤掉旧登录) \n  is-concurrent: true\n  # 在多人登录同一账号时，是否共用一个token (为true时所有登录共用一个token, 为false时每次登录新建一个token)\n  is-share: false\n  # 是否从cookie中读取token\n  is-read-cookie: false\n  # token风格\n  token-style: uuid\n  # 是否输出操作日志 \n  is-log: true \n  # jwt秘钥 \n  jwt-secret-key: asdasdasifhueuiwyurfewbfjsdafjksdadad   \n\nhx:\n  mall:\n    notifyDomain: https://hx-test.suokelian.com\n    orderTimeOut: 20\n    defaultReceiverTime: 7\n    defaultAppraiseTime: 7\n    logisticsKey: xxxxxx\n    logisticsCompanyInfos: \n      - code: yuantong\n        name: 圆通速递  \n      - code: yunda\n        name: 韵达快递\n      - code: zhongtong\n        name: 中通快递\n      - code: jtexpress\n        name: 极兔速递\n      - code: shunfeng\n        name: 顺丰速运\n      - code: ems\n        name: EMS\n      - code: jd\n        name: 京东物流\n      - code: debangkuaidi\n        name: 德邦快递\n      - code: huitongkuaidi\n        name: 百世快递\nsms:\n  ali:\n    accessKeyId: LTAI5tEW9V1CF1cN2FJP8Rzb\n    accessKeySecret: lI3tqfFf8z9kI8sG1T7518XN7vAu6b\n    loginTemplateCode: SMS_244665173\n    loginSignName: 环兴商城                                   \n\n\n\n     \n\n\n\n\n\n', 'b0549a41c5fa479c27702a1d24bc111d', '2022-10-25 22:01:27', '2022-10-25 14:01:28', 'nacos', '192.168.2.7', 'U', '', '');
INSERT INTO `his_config_info` VALUES (2, 20, 'application-dev.yml', 'DEFAULT_GROUP', '', 'spring:\n  redis:\n    host: huanxing-redis\n    port: 6379\n    password: ljx19970310.\n    database: 10\n  servlet:\n      multipart:\n          location: /data/tmp   \nrocketmq:\n  # name-server: 59.110.30.161:9876\n  name-server: 127.0.0.1:9876\n  producer:\n    group: producer_group\n    retry-times-when-send-failed: 5\n    send-message-timeout: 5000     \n  jackson:\n    default-property-inclusion: always\n    date-format: yyyy-mm-dd hh:mm:ss   \n# Sa-Token配置\nsa-token: \n    # token名称 (同时也是cookie名称)\n    token-name: satoken\n    # token有效期，单位s 默认30天, -1代表永不过期 \n    timeout: 2592000\n    # token临时有效期 (指定时间内无操作就视为token过期) 单位: 秒\n    activity-timeout: -1\n    # 是否允许同一账号并发登录 (为true时允许一起登录, 为false时新登录挤掉旧登录) \n    is-concurrent: true\n    # 在多人登录同一账号时，是否共用一个token (为true时所有登录共用一个token, 为false时每次登录新建一个token) \n    is-share: false\n    # 是否从cookie中读取token\n    is-read-cookie: false\n    # token风格\n    token-style: uuid\n    # 是否输出操作日志 \n    is-log: true     \n      # jwt秘钥 \n    jwt-secret-key: asdasdasifhueuiwyurfewbfjsdafjksdadad         \nsecure:\n  ignore:\n    urls:\n    - /actuator/**\n    - /mallapi/**\n    - /auth/token/login\n    - /auth/token/phone/login\n    - /auth/token/page\n    - /code\n    - /auth/phone/code\n    - /upms/user/check/phone\n         \nmybatis-plus:\n  mapper-locations: classpath:/mapper/*Mapper.xml\n  type-handlers-package: com.joolun.cloud.common.core.mybatis.typehandler\n  global-config:\n    sql-parser-cache: true\n    banner: false\n    db-config:\n      id-type: auto\n      select-strategy: not_empty\n\n#  endpoints config\nmanagement:\n  endpoints:\n    web:\n      exposure:\n        include: \'*\'\n    enabled-by-default: true\n\nhx:\n  mall:\n    notifyDomain: \'https://hx-test.suokelian.com\'\n    orderTimeOut: 20\n    defaultReceiverTime: 7\n    defaultAppraiseTime: 7\n    logisticsKey: \'xxxxxx\'\n    logisticsCompanyInfos:\n    - code: yuantong\n      name: 圆通速递    \n    - code: yunda\n      name: 韵达快递 \n    - code: zhongtong\n      name: 中通快递\n    - code: jtexpress\n      name: 极兔速递 \n    - code: shunfeng\n      name: 顺丰速运 \n    - code: ems\n      name: EMS \n    - code: jd\n      name: 京东物流  \n    - code: debangkuaidi\n      name: 德邦快递 \n    - code: huitongkuaidi\n      name: 百世快递     \nsms:\n  ali:\n    accessKeyId: LTAI5tEW9V1CF1cN2FJP8Rzb\n    accessKeySecret: lI3tqfFf8z9kI8sG1T7518XN7vAu6b\n    loginTemplateCode: SMS_244665173\n    loginSignName: 环兴商城                                   ', '920ed7e40f563256823a4d120e40e926', '2022-10-25 22:49:12', '2022-10-25 14:49:12', 'nacos', '192.168.2.7', 'U', '', '');
INSERT INTO `his_config_info` VALUES (2, 21, 'application-dev.yml', 'DEFAULT_GROUP', '', 'spring:\n  redis:\n    host: huanxing-redis\n    port: 6379\n    password: ljx19970310.\n    database: 10\n  servlet:\n      multipart:\n          location: /data/tmp   \nrocketmq:\n  # name-server: 59.110.30.161:9876\n  name-server: 127.0.0.1:9876\n  producer:\n    group: producer_group\n    retry-times-when-send-failed: 5\n    send-message-timeout: 5000     \n  jackson:\n    default-property-inclusion: always\n    date-format: yyyy-mm-dd hh:mm:ss   \n# Sa-Token配置\nsa-token: \n    # token名称 (同时也是cookie名称)\n    token-name: satoken\n    # token有效期，单位s 默认30天, -1代表永不过期 \n    timeout: 2592000\n    # token临时有效期 (指定时间内无操作就视为token过期) 单位: 秒\n    activity-timeout: -1\n    # 是否允许同一账号并发登录 (为true时允许一起登录, 为false时新登录挤掉旧登录) \n    is-concurrent: true\n    # 在多人登录同一账号时，是否共用一个token (为true时所有登录共用一个token, 为false时每次登录新建一个token) \n    is-share: false\n    # 是否从cookie中读取token\n    is-read-cookie: false\n    # token风格\n    token-style: uuid\n    # 是否输出操作日志 \n    is-log: true     \n      # jwt秘钥 \n    jwt-secret-key: asdasdasifhueuiwyurfewbfjsdafjksdadad         \nsecure:\n  ignore:\n    urls:\n    - /actuator/**\n    - /mallapi/**\n    - /auth/token/login\n    - /auth/token/phone/login\n    - /auth/token/page\n    - /code\n    - /auth/phone/code\n    - /upms/user/check/phone\n         \nmybatis-plus:\n  mapper-locations: classpath:/mapper/*Mapper.xml\n  type-handlers-package: com.huanxing.cloud.common.core.mybatis.typehandler\n  global-config:\n    sql-parser-cache: true\n    banner: false\n    db-config:\n      id-type: auto\n      select-strategy: not_empty\n\n#  endpoints config\nmanagement:\n  endpoints:\n    web:\n      exposure:\n        include: \'*\'\n    enabled-by-default: true\n\nhx:\n  mall:\n    notifyDomain: \'https://hx-test.suokelian.com\'\n    orderTimeOut: 20\n    defaultReceiverTime: 7\n    defaultAppraiseTime: 7\n    logisticsKey: \'xxxxxx\'\n    logisticsCompanyInfos:\n    - code: yuantong\n      name: 圆通速递    \n    - code: yunda\n      name: 韵达快递 \n    - code: zhongtong\n      name: 中通快递\n    - code: jtexpress\n      name: 极兔速递 \n    - code: shunfeng\n      name: 顺丰速运 \n    - code: ems\n      name: EMS \n    - code: jd\n      name: 京东物流  \n    - code: debangkuaidi\n      name: 德邦快递 \n    - code: huitongkuaidi\n      name: 百世快递     \nsms:\n  ali:\n    accessKeyId: LTAI5tEW9V1CF1cN2FJP8Rzb\n    accessKeySecret: lI3tqfFf8z9kI8sG1T7518XN7vAu6b\n    loginTemplateCode: SMS_244665173\n    loginSignName: 环兴商城                                   ', '2a102dbd0f54f170d6a662655744e482', '2022-10-25 22:49:47', '2022-10-25 14:49:47', 'nacos', '192.168.2.7', 'U', '', '');
INSERT INTO `his_config_info` VALUES (12, 22, 'huanxing-upms-admin-dev.yml', 'DEFAULT_GROUP', '', '\r\nspring:  \r\n  datasource:\r\n    type: com.alibaba.druid.pool.DruidDataSource\r\n    druid:\r\n      driver-class-name: com.mysql.cj.jdbc.Driver\r\n      username: root\r\n      password: Ljx19970310\r\n      url: jdbc:mysql://rm-bp11z9kl01o5qt4453o.mysql.rds.aliyuncs.com:65534/huanxing_upms?characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=GMT%2B8&allowMultiQueries=true&allowPublicKeyRetrieval=true\r\nlogging:\r\n  level:\r\n    com.huanxing.cloud.upms.admin.mapper: debug\r\n#  endpoints config\r\nmanagement:\r\n  endpoint:\r\n    health:\r\n      show-details: ALWAYS\r\n    # 日志记录\r\n    logfile:\r\n      external-file: E:/hx-log/debug.log', 'ebbd7d6f040e39eae2b77a58474439dd', '2022-10-25 22:52:39', '2022-10-25 14:52:39', 'nacos', '192.168.2.7', 'U', '', '');

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions`  (
  `role` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `resource` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `action` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  UNIQUE INDEX `uk_role_permission`(`role`, `resource`, `action`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of permissions
-- ----------------------------

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`  (
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `role` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  UNIQUE INDEX `idx_user_role`(`username`, `role`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('nacos', 'ROLE_ADMIN');

-- ----------------------------
-- Table structure for tenant_capacity
-- ----------------------------
DROP TABLE IF EXISTS `tenant_capacity`;
CREATE TABLE `tenant_capacity`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT 'Tenant ID',
  `quota` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '配额，0表示使用默认值',
  `usage` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '使用量',
  `max_size` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单个配置大小上限，单位为字节，0表示使用默认值',
  `max_aggr_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '聚合子配置最大个数',
  `max_aggr_size` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单个聚合数据的子配置大小上限，单位为字节，0表示使用默认值',
  `max_history_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '最大变更历史数量',
  `gmt_create` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `gmt_modified` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_tenant_id`(`tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '租户容量信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tenant_capacity
-- ----------------------------

-- ----------------------------
-- Table structure for tenant_info
-- ----------------------------
DROP TABLE IF EXISTS `tenant_info`;
CREATE TABLE `tenant_info`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `kp` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'kp',
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT 'tenant_id',
  `tenant_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT 'tenant_name',
  `tenant_desc` varchar(256) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'tenant_desc',
  `create_source` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'create_source',
  `gmt_create` bigint(20) NOT NULL COMMENT '创建时间',
  `gmt_modified` bigint(20) NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_tenant_info_kptenantid`(`kp`, `tenant_id`) USING BTREE,
  INDEX `idx_tenant_id`(`tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'tenant_info' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tenant_info
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `password` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`username`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('nacos', '$2a$10$EuWPZHzz32dJN7jexM34MOeYirDdFAZm2kuWj7VEOJhhZkDrxfvUu', 1);

SET FOREIGN_KEY_CHECKS = 1;
