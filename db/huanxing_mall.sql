/*
 Navicat Premium Data Transfer

 Source Server         : 环兴商城
 Source Server Type    : MySQL
 Source Server Version : 80018
 Source Schema         : huanxing_mall

 Target Server Type    : MySQL
 Target Server Version : 80018
 File Encoding         : 65001

 Date: 30/07/2022 13:25:55
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for distribution_config
-- ----------------------------
DROP TABLE IF EXISTS `distribution_config`;
CREATE TABLE `distribution_config`  (
                                        `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'PK',
                                        `status` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '1' COMMENT '分销状态：0.关闭；1.开启；',
                                        `distribution_model` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '分销模式：1.所有人；2.会员分销；3.满额分销；',
                                        `frozen_time` int(11) NULL DEFAULT NULL COMMENT '佣金冻结天数',
                                        `full_amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '满额分销金额',
                                        `pic_urls` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '分销图片',
                                        `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                                        `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
                                        `create_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建者ID',
                                        `update_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改者ID',
                                        `del_flag` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                                        PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '分销配置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of distribution_config
-- ----------------------------
INSERT INTO `distribution_config` VALUES ('1', '1', '1', 7, NULL, 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/41b0da67-9da6-4a97-a3d8-a9cdbccf3149.png', '2022-07-11 18:46:01', '2022-07-11 20:45:30', NULL, NULL, '0');

-- ----------------------------
-- Table structure for distribution_order
-- ----------------------------
DROP TABLE IF EXISTS `distribution_order`;
CREATE TABLE `distribution_order`  (
                                       `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'PK',
                                       `distribution_user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '分销用户主键',
                                       `distribution_level` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '分销级别：1.一级分销；2.二级分销；',
                                       `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户主键',
                                       `order_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '订单主键',
                                       `total_amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '佣金总金额（元）',
                                       `status` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '1' COMMENT '佣金状态：1.冻结中；2.已解冻',
                                       `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                                       `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
                                       `create_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建者ID',
                                       `update_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改者ID',
                                       `del_flag` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                                       PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '分销订单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of distribution_order
-- ----------------------------
INSERT INTO `distribution_order` VALUES ('1', '1499374465621065729', '1', '1504785344059039746', '1542720634446610433', 0.12, '1', '2022-07-11 13:44:04', '2022-07-11 13:49:06', NULL, NULL, '0');

-- ----------------------------
-- Table structure for distribution_order_item
-- ----------------------------
DROP TABLE IF EXISTS `distribution_order_item`;
CREATE TABLE `distribution_order_item`  (
                                            `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'PK',
                                            `distribution_order_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '分销订单主键',
                                            `order_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '订单主键',
                                            `order_item_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '子订单主键',
                                            `amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '佣金金额（元）',
                                            `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                                            `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
                                            `create_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建者ID',
                                            `update_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改者ID',
                                            `del_flag` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                                            PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '分销子订单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of distribution_order_item
-- ----------------------------

-- ----------------------------
-- Table structure for distribution_user
-- ----------------------------
DROP TABLE IF EXISTS `distribution_user`;
CREATE TABLE `distribution_user`  (
                                      `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '分销用户主键',
                                      `total_amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '累计分销总金额（元）',
                                      `total_withdrawal` decimal(10, 2) NULL DEFAULT NULL COMMENT '累计提现总金额（元）',
                                      `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                                      `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
                                      `create_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建者ID',
                                      `update_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改者ID',
                                      `del_flag` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                                      PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '分销用户' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of distribution_user
-- ----------------------------
INSERT INTO `distribution_user` VALUES ('1499374465621065729', 0.12, 0.00, '2022-07-11 13:45:07', '2022-07-11 13:49:10', NULL, NULL, '0');

-- ----------------------------
-- Table structure for freight_incl_postage
-- ----------------------------
DROP TABLE IF EXISTS `freight_incl_postage`;
CREATE TABLE `freight_incl_postage`  (
                                         `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'PK',
                                         `freight_template_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '运费模板id',
                                         PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '指定包邮条件表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of freight_incl_postage
-- ----------------------------

-- ----------------------------
-- Table structure for freight_template
-- ----------------------------
DROP TABLE IF EXISTS `freight_template`;
CREATE TABLE `freight_template`  (
                                     `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'PK',
                                     `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '运费模板名称',
                                     `send_time` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '发货时间',
                                     `address` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '发货地址',
                                     `is_incl_postage` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否包邮：0.否、1.是',
                                     `is_incl_postage_by_if` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否指定条件包邮：0.否、1.是',
                                     `pricing_type` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '计价方式：1.按件数、2.按重量、3.按体积',
                                     `first_num` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '首件（个）首重（kg）',
                                     `continue_num` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '续件（个）续重（kg）',
                                     `first_freight` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '首费（元）',
                                     `continue_freight` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '续费（元）',
                                     `full_amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '满包邮（元）',
                                     `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                                     `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
                                     `create_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建者ID',
                                     `update_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改者ID',
                                     `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                                     PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '运费模板' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of freight_template
-- ----------------------------
INSERT INTO `freight_template` VALUES ('1496736152821198850', '自定义包邮模板', '30天之内发货', '辽宁大连', '0', '1', '1', 1.00, 1.00, 1.00, 1.00, 50.00, '2022-02-24 14:38:01', '2022-02-24 14:38:01', NULL, NULL, '0');
INSERT INTO `freight_template` VALUES ('1498636653225537537', '自定义运费模版', '3天内发货', '辽宁沈阳', '0', '0', '1', 1.00, 1.00, 6.00, 0.00, NULL, '2022-03-01 20:29:57', '2022-03-01 20:29:57', NULL, NULL, '0');
INSERT INTO `freight_template` VALUES ('1501942131653238786', '按件付运费', '3天内发货', '辽宁沈阳', '0', '0', '1', 1.00, 1.00, 200.00, 200.00, NULL, '2022-03-10 23:24:46', '2022-03-10 23:24:46', NULL, NULL, '0');
INSERT INTO `freight_template` VALUES ('1536996605278457857', '包邮', '4天', '辽宁沈阳', '1', '1', NULL, 0.00, 0.00, 0.00, 0.00, NULL, '2022-06-15 16:58:41', '2022-06-15 16:58:41', NULL, NULL, '0');

-- ----------------------------
-- Table structure for goods_appraise
-- ----------------------------
DROP TABLE IF EXISTS `goods_appraise`;
CREATE TABLE `goods_appraise`  (
                                   `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'PK',
                                   `spu_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'spu主键',
                                   `order_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '订单主键',
                                   `order_item_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '子订单主键',
                                   `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户ID',
                                   `avatar_url` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '头像',
                                   `nick_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '昵称',
                                   `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                                   `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
                                   `create_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建者ID',
                                   `update_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改者ID',
                                   `del_flag` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                                   `pic_urls` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '图片',
                                   `goods_score` int(1) NULL DEFAULT NULL COMMENT '商品评分',
                                   `logistics_score` int(1) NULL DEFAULT NULL COMMENT '物流评分',
                                   `service_score` int(1) NULL DEFAULT NULL COMMENT '服务评分',
                                   `business_reply` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '商家回复',
                                   `reply_time` datetime(0) NULL DEFAULT NULL COMMENT '回复时间',
                                   `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '评论内容',
                                   PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '商品评价' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of goods_appraise
-- ----------------------------
INSERT INTO `goods_appraise` VALUES ('1', '1496394935734403073', '1', '1', '1499374465621065729', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/d72c878f-f226-43f6-b34a-9bea1a37e429.jpg', '环兴商城', '2022-03-10 21:35:06', '2022-05-27 09:36:11', NULL, NULL, '0', '[\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/0a69731e-cb13-49d9-950f-0649bf32d044.jpg\"]', 5, 5, 5, '感谢一路支持', '2022-03-10 22:09:54', '好东西！！！值得购买好东西！！！值得购买好东西！！！值得购买好东西！！！值得购买好东西！！！值得购买');
INSERT INTO `goods_appraise` VALUES ('2', '1496394935734403073', '1', '1', '1499374465621065729', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/d72c878f-f226-43f6-b34a-9bea1a37e429.jpg', '环兴商城', '2022-03-10 21:35:06', '2022-05-27 09:38:05', NULL, NULL, '0', '[\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/b640cc30-c999-46e8-b9c2-677e3ce61b9c.jpg\",\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/d2f9b018-d304-4654-aa94-bafa746bf822.jpg\",\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/0a69731e-cb13-49d9-950f-0649bf32d044.jpg\",\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/0a69731e-cb13-49d9-950f-0649bf32d044.jpg\"]', 3, 3, 3, '感谢一路支持', '2022-03-10 22:09:54', '好东西！！！值得购买好东西！！！值得购买好东西！！！值得购买好东西！！！值得购买好东西！！！值得购买');

-- ----------------------------
-- Table structure for goods_category
-- ----------------------------
DROP TABLE IF EXISTS `goods_category`;
CREATE TABLE `goods_category`  (
                                   `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'PK',
                                   `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '类目名称',
                                   `parent_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '上级类目（0.顶级类目）',
                                   `category_pic` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '类目图片',
                                   `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '类目描述',
                                   `status` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '1' COMMENT '状态：0.禁用；1.启用',
                                   `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                                   `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
                                   `create_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建者ID',
                                   `update_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改者ID',
                                   `del_flag` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                                   `sort` int(11) NULL DEFAULT NULL COMMENT '排序序号',
                                   PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '商品类目' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of goods_category
-- ----------------------------
INSERT INTO `goods_category` VALUES ('1496672394983804930', '小米', '4', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/bb60e527-092c-4fc5-974e-2697404c5d8f.jpg', NULL, '1', '2022-02-24 10:24:36', '2022-02-24 10:36:58', NULL, NULL, '0', 1);
INSERT INTO `goods_category` VALUES ('1496677018553872385', '家用电器', '0', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/f1201f1f-5c36-4468-a1f8-a634c894685d.jpg', '家用电器', '1', '2022-02-24 10:42:58', '2022-02-24 10:42:58', NULL, NULL, '0', 2);
INSERT INTO `goods_category` VALUES ('1496686626970333185', '电视', '1496677018553872385', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/bb60e527-092c-4fc5-974e-2697404c5d8f.jpg', NULL, '1', '2022-02-24 11:21:13', '2022-05-27 11:50:37', NULL, NULL, '0', 1);
INSERT INTO `goods_category` VALUES ('1498510244406673410', '数码', '0', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/0a69731e-cb13-49d9-950f-0649bf32d044.jpg', '数码', '1', '2022-03-01 12:07:37', '2022-03-01 12:07:37', NULL, NULL, '0', 10);
INSERT INTO `goods_category` VALUES ('1498510397314220034', '耳机/耳麦', '1498510244406673410', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/bb60e527-092c-4fc5-974e-2697404c5d8f.jpg', '耳机/耳麦', '1', '2022-03-01 12:08:13', '2022-05-27 11:50:38', NULL, NULL, '0', 1);
INSERT INTO `goods_category` VALUES ('1498864835983876097', 'OPPO', '4', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/f1201f1f-5c36-4468-a1f8-a634c894685d.jpg', NULL, '1', '2022-03-02 11:36:37', '2022-03-02 11:36:37', NULL, NULL, '0', 11);
INSERT INTO `goods_category` VALUES ('1499342326879887362', '手表', '1498510244406673410', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/bb60e527-092c-4fc5-974e-2697404c5d8f.jpg', NULL, '1', '2022-03-03 19:14:04', '2022-05-27 11:50:41', NULL, NULL, '0', 5);
INSERT INTO `goods_category` VALUES ('1504307346357383170', '男装', '0', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/3da3d902-ec1c-4107-b8e7-0614bbc1b89b.jpg', NULL, '1', '2022-03-17 12:03:14', '2022-03-17 12:03:14', NULL, NULL, '0', 11);
INSERT INTO `goods_category` VALUES ('1504307475378368514', '新品推荐', '1504307346357383170', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/bb60e527-092c-4fc5-974e-2697404c5d8f.jpg', NULL, '1', '2022-03-17 12:03:44', '2022-05-27 11:50:42', NULL, NULL, '0', 1);
INSERT INTO `goods_category` VALUES ('1504307581011914753', '商场同款', '1504307346357383170', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/bb60e527-092c-4fc5-974e-2697404c5d8f.jpg', NULL, '1', '2022-03-17 12:04:09', '2022-05-27 11:50:44', NULL, NULL, '0', 3);
INSERT INTO `goods_category` VALUES ('1504307713140879362', '女装', '0', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/ea86c408-21eb-47af-81df-6367cb398075.jpg', NULL, '1', '2022-03-17 12:04:41', '2022-03-17 12:04:41', NULL, NULL, '0', 15);
INSERT INTO `goods_category` VALUES ('1504307782850211841', '新品推荐', '1504307713140879362', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/bb60e527-092c-4fc5-974e-2697404c5d8f.jpg', NULL, '1', '2022-03-17 12:04:58', '2022-05-27 11:50:46', NULL, NULL, '0', 1);
INSERT INTO `goods_category` VALUES ('1504307835576807425', '商场同款', '1504307713140879362', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/bb60e527-092c-4fc5-974e-2697404c5d8f.jpg', NULL, '1', '2022-03-17 12:05:10', '2022-05-27 11:50:48', NULL, NULL, '0', 2);
INSERT INTO `goods_category` VALUES ('1528663744649547778', '家具', '0', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/5137138c-1436-412c-9fb7-74e80b357061.jpg', '家具', '1', '2022-05-23 17:06:52', '2022-05-23 17:06:52', NULL, NULL, '0', 12);
INSERT INTO `goods_category` VALUES ('1528947067057786882', '锤子', '4', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/ea86c408-21eb-47af-81df-6367cb398075.jpg', '锤子', '1', '2022-05-24 11:52:41', '2022-05-24 11:52:41', NULL, NULL, '0', 10);
INSERT INTO `goods_category` VALUES ('1528947151174553602', '联想', '4', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/5137138c-1436-412c-9fb7-74e80b357061.jpg', '联想', '1', '2022-05-24 11:53:01', '2022-05-24 11:53:01', NULL, NULL, '0', 20);
INSERT INTO `goods_category` VALUES ('1528947222288977921', '海尔', '4', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/5137138c-1436-412c-9fb7-74e80b357061.jpg', '海尔', '1', '2022-05-24 11:53:18', '2022-05-24 11:53:18', NULL, NULL, '0', 22);
INSERT INTO `goods_category` VALUES ('1528947336764116993', '洗衣机', '1496677018553872385', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/5137138c-1436-412c-9fb7-74e80b357061.jpg', '洗衣机', '1', '2022-05-24 11:53:46', '2022-05-24 11:53:46', NULL, NULL, '0', 10);
INSERT INTO `goods_category` VALUES ('1528947409455599617', '电视', '1496677018553872385', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/5137138c-1436-412c-9fb7-74e80b357061.jpg', '电视', '1', '2022-05-24 11:54:03', '2022-05-24 11:54:03', NULL, NULL, '0', 20);
INSERT INTO `goods_category` VALUES ('1528947472009449473', '电脑', '1496677018553872385', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/ea86c408-21eb-47af-81df-6367cb398075.jpg', '电脑', '1', '2022-05-24 11:54:18', '2022-05-24 11:54:18', NULL, NULL, '0', 25);
INSERT INTO `goods_category` VALUES ('1528947743129260034', '母婴', '0', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/3da3d902-ec1c-4107-b8e7-0614bbc1b89b.jpg', '母婴', '1', '2022-05-24 11:55:23', '2022-05-24 11:55:23', NULL, NULL, '0', 100);
INSERT INTO `goods_category` VALUES ('1528947872280268802', '汽车', '0', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/3da3d902-ec1c-4107-b8e7-0614bbc1b89b.jpg', NULL, '1', '2022-05-24 11:55:53', '2022-05-24 11:55:53', NULL, NULL, '0', 110);
INSERT INTO `goods_category` VALUES ('1528948092074381314', '礼品', '0', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/3da3d902-ec1c-4107-b8e7-0614bbc1b89b.jpg', NULL, '1', '2022-05-24 11:56:46', '2022-05-24 11:56:46', NULL, NULL, '0', 122);
INSERT INTO `goods_category` VALUES ('1528948268147068930', '玩具乐器', '0', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/3da3d902-ec1c-4107-b8e7-0614bbc1b89b.jpg', NULL, '1', '2022-05-24 11:57:28', '2022-05-24 11:57:28', NULL, NULL, '0', 120);
INSERT INTO `goods_category` VALUES ('1529425418176352257', '分类1', '0', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/3da3d902-ec1c-4107-b8e7-0614bbc1b89b.jpg', '分类1', '1', '2022-05-25 19:33:29', '2022-05-25 19:33:29', NULL, NULL, '0', 1);
INSERT INTO `goods_category` VALUES ('1529425475764146178', '分类2', '0', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/3da3d902-ec1c-4107-b8e7-0614bbc1b89b.jpg', NULL, '1', '2022-05-25 19:33:43', '2022-05-25 19:33:43', NULL, NULL, '0', 12);
INSERT INTO `goods_category` VALUES ('1529425539655979009', '分类3', '0', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/3da3d902-ec1c-4107-b8e7-0614bbc1b89b.jpg', NULL, '1', '2022-05-25 19:33:58', '2022-05-25 19:33:58', NULL, NULL, '0', 13);
INSERT INTO `goods_category` VALUES ('1529425842321149953', '分类4', '0', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/3da3d902-ec1c-4107-b8e7-0614bbc1b89b.jpg', NULL, '1', '2022-05-25 19:35:10', '2022-05-25 19:35:10', NULL, NULL, '0', 14);
INSERT INTO `goods_category` VALUES ('1529425895035162625', '分类5', '0', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/3da3d902-ec1c-4107-b8e7-0614bbc1b89b.jpg', NULL, '1', '2022-05-25 19:35:23', '2022-05-25 19:35:23', NULL, NULL, '0', 15);
INSERT INTO `goods_category` VALUES ('4', '手机通讯', '0', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/0a69731e-cb13-49d9-950f-0649bf32d044.jpg', '手机通讯', '1', '2022-02-23 15:42:45', '2022-02-24 10:37:18', NULL, NULL, '0', 1);
INSERT INTO `goods_category` VALUES ('5', 'Apple', '4', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/0a69731e-cb13-49d9-950f-0649bf32d044.jpg', 'Apple手机', '1', '2022-02-23 15:43:02', '2022-02-24 10:37:19', NULL, NULL, '0', 1);
INSERT INTO `goods_category` VALUES ('7', '华为', '4', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/d03d26cc-4c43-4575-a7ff-7f5e500a2ee6.jpg', '华为', '1', '2022-02-23 16:00:58', '2022-02-24 10:37:20', NULL, NULL, '0', 1);

-- ----------------------------
-- Table structure for goods_sku
-- ----------------------------
DROP TABLE IF EXISTS `goods_sku`;
CREATE TABLE `goods_sku`  (
                              `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'PK',
                              `spu_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'spuId',
                              `pic_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '图片',
                              `sales_price` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '销售价格（元）',
                              `original_price` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '原价（元）',
                              `cost_price` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '成本价（元）',
                              `stock` int(11) NOT NULL DEFAULT 0 COMMENT '库存',
                              `enable` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '1' COMMENT '是否启用：0.否；1.是；',
                              `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                              `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
                              `create_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建者ID',
                              `update_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改者ID',
                              `del_flag` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                              `first_rate` int(11) NOT NULL DEFAULT 0 COMMENT '一级分销比例（%）',
                              `second_rate` int(11) NOT NULL DEFAULT 0 COMMENT '二级分销比例（%）',
                              PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '商品SKU' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of goods_sku
-- ----------------------------
INSERT INTO `goods_sku` VALUES ('1533298173649502209', '1533298170302447618', NULL, 100.00, 100.00, 100.00, 100, '1', '2022-07-05 15:27:06', '2022-07-05 15:27:06', NULL, NULL, '0', 10, 4);
INSERT INTO `goods_sku` VALUES ('1533298174073126913', '1533298170302447618', NULL, 100.00, 100.00, 100.00, 1000, '1', '2022-07-05 15:27:06', '2022-07-05 15:27:06', NULL, NULL, '0', 10, 4);
INSERT INTO `goods_sku` VALUES ('1533298174479974402', '1533298170302447618', NULL, 100.00, 100.00, 100.00, 100, '1', '2022-07-05 15:27:06', '2022-07-05 15:27:06', NULL, NULL, '0', 10, 4);
INSERT INTO `goods_sku` VALUES ('1533379915051880450', '1496390644407762945', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/0a69731e-cb13-49d9-950f-0649bf32d044.jpg', 99999.00, 99999.00, 99999.00, 99999, '1', '2022-06-05 17:24:53', '2022-06-12 23:07:09', NULL, NULL, '0', 0, 0);
INSERT INTO `goods_sku` VALUES ('1533380695737655297', '1533380695339196418', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/251d8c72-fcdb-4576-897a-2f46025c2bf4.jpg', 12245.00, 12245.00, 12245.00, 11, '1', '2022-06-05 17:30:23', '2022-06-05 17:30:23', NULL, NULL, '0', 0, 0);
INSERT INTO `goods_sku` VALUES ('1533792035451080705', '1533792035048427522', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/3dd8f6ba-cd27-42f6-9de8-47008fd98fcd.jpg', 18699.00, 30000.00, 12000.00, 4, '1', '2022-06-06 20:44:53', '2022-06-12 23:07:10', NULL, NULL, '0', 0, 0);
INSERT INTO `goods_sku` VALUES ('1533792035811790849', '1533792035048427522', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/3dd8f6ba-cd27-42f6-9de8-47008fd98fcd.jpg', 28699.00, 30000.00, 12000.00, 10, '1', '2022-06-06 20:44:53', '2022-06-12 23:07:10', NULL, NULL, '0', 0, 0);
INSERT INTO `goods_sku` VALUES ('1533792036172500994', '1533792035048427522', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/3dd8f6ba-cd27-42f6-9de8-47008fd98fcd.jpg', 17699.00, 30000.00, 12000.00, 9, '1', '2022-06-06 20:44:53', '2022-06-12 23:07:10', NULL, NULL, '0', 0, 0);
INSERT INTO `goods_sku` VALUES ('1533792036533211137', '1533792035048427522', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/3dd8f6ba-cd27-42f6-9de8-47008fd98fcd.jpg', 18699.00, 30000.00, 12000.00, 10, '1', '2022-06-06 20:44:53', '2022-06-12 23:07:10', NULL, NULL, '0', 0, 0);
INSERT INTO `goods_sku` VALUES ('1533792036889726977', '1533792035048427522', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/3dd8f6ba-cd27-42f6-9de8-47008fd98fcd.jpg', 28699.00, 30000.00, 12000.00, 10, '1', '2022-06-06 20:44:53', '2022-06-12 23:07:10', NULL, NULL, '0', 0, 0);
INSERT INTO `goods_sku` VALUES ('1533792037250437122', '1533792035048427522', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/3dd8f6ba-cd27-42f6-9de8-47008fd98fcd.jpg', 17699.00, 30000.00, 12000.00, 100, '1', '2022-06-06 20:44:53', '2022-06-12 23:07:10', NULL, NULL, '0', 0, 0);
INSERT INTO `goods_sku` VALUES ('1535187344563052546', '1535187344403668993', NULL, 4399.00, 5699.00, 1299.00, 0, '1', '2022-07-07 14:19:57', '2022-07-07 14:19:57', NULL, NULL, '0', 0, 0);
INSERT INTO `goods_sku` VALUES ('1535187344667910145', '1535187344403668993', NULL, 4399.00, 5699.00, 1299.00, 0, '1', '2022-07-07 14:19:57', '2022-07-07 14:19:57', NULL, NULL, '0', 0, 0);
INSERT INTO `goods_sku` VALUES ('1535191486941569026', '1535191486845100033', NULL, 0.10, 4188.10, 4185.10, 985, '1', '2022-07-26 11:20:13', '2022-07-26 11:20:13', NULL, NULL, '0', 10, 5);
INSERT INTO `goods_sku` VALUES ('1535191487033843713', '1535191486845100033', NULL, 4888.10, 4888.10, 4888.10, 986, '1', '2022-07-26 11:20:13', '2022-07-26 11:20:13', NULL, NULL, '0', 10, 5);
INSERT INTO `goods_sku` VALUES ('1535191487126118402', '1535191486845100033', NULL, 4188.10, 4188.10, 4188.10, 1000, '1', '2022-07-26 11:20:13', '2022-07-26 11:20:13', NULL, NULL, '0', 10, 5);
INSERT INTO `goods_sku` VALUES ('1535191487222587394', '1535191486845100033', NULL, 4888.10, 4888.10, 4888.10, 0, '1', '2022-07-26 11:20:13', '2022-07-26 11:20:13', NULL, NULL, '0', 10, 5);
INSERT INTO `goods_sku` VALUES ('1535999718430232577', '1535999718077911042', NULL, 1.00, 1.00, 1.00, 1, '1', '2022-06-12 22:57:26', '2022-06-12 23:07:11', NULL, NULL, '0', 0, 0);
INSERT INTO `goods_sku` VALUES ('1536003714071023617', '1498511063457775618', NULL, 99999.00, 99999.00, 99999.00, 11, '1', '2022-06-12 23:13:18', '2022-06-12 23:13:18', NULL, NULL, '0', 0, 0);
INSERT INTO `goods_sku` VALUES ('1536003782480121857', '1496687219931672578', NULL, 99999.00, 99999.00, 99999.00, 123, '1', '2022-06-12 23:13:34', '2022-06-12 23:13:34', NULL, NULL, '0', 0, 0);
INSERT INTO `goods_sku` VALUES ('1536003831662530562', '1496394935734403073', NULL, 99999.00, 99999.00, 99999.00, 213, '1', '2022-06-12 23:13:46', '2022-06-12 23:13:46', NULL, NULL, '0', 0, 0);
INSERT INTO `goods_sku` VALUES ('1544221247647547394', '1533298170302447618', NULL, 100.00, 100.00, 100.00, 100, '1', '2022-07-05 15:27:06', '2022-07-05 15:27:06', NULL, NULL, '0', 10, 5);

-- ----------------------------
-- Table structure for goods_sku_specs_value
-- ----------------------------
DROP TABLE IF EXISTS `goods_sku_specs_value`;
CREATE TABLE `goods_sku_specs_value`  (
                                          `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'PK',
                                          `sku_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'skuId',
                                          `specs_value_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '规格值主键',
                                          `sort` int(11) NOT NULL COMMENT '排序字段',
                                          `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                                          `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
                                          `create_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建者ID',
                                          `update_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改者ID',
                                          `del_flag` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                                          `spu_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'spuId',
                                          `pic_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '图片',
                                          PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '商品sku关联规格值' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of goods_sku_specs_value
-- ----------------------------
INSERT INTO `goods_sku_specs_value` VALUES ('1533298220822839298', '1533298174903599106', '1532635928418504706', 0, '2022-06-05 12:02:39', '2022-06-05 12:02:39', NULL, NULL, '0', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1533298221233881090', '1533298174903599106', '1532637684972048386', 1, '2022-06-05 12:02:40', '2022-06-05 12:02:40', NULL, NULL, '0', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1533792066228883457', '1533792035451080705', '1533790877215002625', 0, '2022-06-06 20:45:00', '2022-06-06 20:57:36', NULL, NULL, '0', '1533792035048427522', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/3dd8f6ba-cd27-42f6-9de8-47008fd98fcd.jpg');
INSERT INTO `goods_sku_specs_value` VALUES ('1533792066581204993', '1533792035451080705', '1533790357578485761', 1, '2022-06-06 20:45:00', '2022-06-06 20:50:12', NULL, NULL, '0', '1533792035048427522', NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1533792066937720834', '1533792035811790849', '1533790877215002625', 0, '2022-06-06 20:45:00', '2022-06-06 20:57:35', NULL, NULL, '0', '1533792035048427522', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/3dd8f6ba-cd27-42f6-9de8-47008fd98fcd.jpg');
INSERT INTO `goods_sku_specs_value` VALUES ('1533792067302625282', '1533792035811790849', '1533790579952095234', 1, '2022-06-06 20:45:00', '2022-06-06 20:50:14', NULL, NULL, '0', '1533792035048427522', NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1533792067659141121', '1533792036172500994', '1533790877215002625', 0, '2022-06-06 20:45:00', '2022-06-06 20:57:37', NULL, NULL, '0', '1533792035048427522', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/3dd8f6ba-cd27-42f6-9de8-47008fd98fcd.jpg');
INSERT INTO `goods_sku_specs_value` VALUES ('1533792068015656961', '1533792036172500994', '1533790772885884930', 1, '2022-06-06 20:45:01', '2022-06-06 20:50:18', NULL, NULL, '0', '1533792035048427522', NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1533792068384755714', '1533792036533211137', '1533790905182621697', 0, '2022-06-06 20:45:01', '2022-06-06 22:06:55', NULL, NULL, '0', '1533792035048427522', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/d6ac601c-90fe-49bb-9531-8b7c441a0782.jpg');
INSERT INTO `goods_sku_specs_value` VALUES ('1533792068741271554', '1533792036533211137', '1533790357578485761', 1, '2022-06-06 20:45:01', '2022-06-06 20:50:19', NULL, NULL, '0', '1533792035048427522', NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1533792069101981698', '1533792036889726977', '1533790905182621697', 0, '2022-06-06 20:45:01', '2022-06-06 22:06:56', NULL, NULL, '0', '1533792035048427522', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/d6ac601c-90fe-49bb-9531-8b7c441a0782.jpg');
INSERT INTO `goods_sku_specs_value` VALUES ('1533792069458497538', '1533792036889726977', '1533790579952095234', 1, '2022-06-06 20:45:01', '2022-06-06 20:50:21', NULL, NULL, '0', '1533792035048427522', NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1533792069823401986', '1533792037250437122', '1533790905182621697', 0, '2022-06-06 20:45:01', '2022-06-06 22:06:56', NULL, NULL, '0', '1533792035048427522', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/d6ac601c-90fe-49bb-9531-8b7c441a0782.jpg');
INSERT INTO `goods_sku_specs_value` VALUES ('1533792070179917826', '1533792037250437122', '1533790772885884930', 1, '2022-06-06 20:45:01', '2022-06-06 20:50:22', NULL, NULL, '0', '1533792035048427522', NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1544221324701106177', '1533298173649502209', '1532635376146108418', 0, '2022-07-05 15:27:06', '2022-07-05 15:27:06', NULL, NULL, '0', '1533298170302447618', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/0a69731e-cb13-49d9-950f-0649bf32d044.jpg');
INSERT INTO `goods_sku_specs_value` VALUES ('1544221325049233409', '1533298173649502209', '1532637661773352962', 1, '2022-07-05 15:27:06', '2022-07-05 15:27:06', NULL, NULL, '0', '1533298170302447618', '');
INSERT INTO `goods_sku_specs_value` VALUES ('1544221325405749249', '1533298174073126913', '1532635376146108418', 0, '2022-07-05 15:27:06', '2022-07-05 15:27:06', NULL, NULL, '0', '1533298170302447618', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/0a69731e-cb13-49d9-950f-0649bf32d044.jpg');
INSERT INTO `goods_sku_specs_value` VALUES ('1544221325762265090', '1533298174073126913', '1532637684972048386', 1, '2022-07-05 15:27:07', '2022-07-05 15:27:07', NULL, NULL, '0', '1533298170302447618', '');
INSERT INTO `goods_sku_specs_value` VALUES ('1544221326118780929', '1533298174479974402', '1532635928418504706', 0, '2022-07-05 15:27:07', '2022-07-05 15:27:07', NULL, NULL, '0', '1533298170302447618', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/3da3d902-ec1c-4107-b8e7-0614bbc1b89b.jpg');
INSERT INTO `goods_sku_specs_value` VALUES ('1544221326466908161', '1533298174479974402', '1532637661773352962', 1, '2022-07-05 15:27:07', '2022-07-05 15:27:07', NULL, NULL, '0', '1533298170302447618', '');
INSERT INTO `goods_sku_specs_value` VALUES ('1544221326819229697', '1544221247647547394', '1532635928418504706', 0, '2022-07-05 15:27:07', '2022-07-05 15:27:07', NULL, NULL, '0', '1533298170302447618', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/3da3d902-ec1c-4107-b8e7-0614bbc1b89b.jpg');
INSERT INTO `goods_sku_specs_value` VALUES ('1544221327171551234', '1544221247647547394', '1532637684972048386', 1, '2022-07-05 15:27:07', '2022-07-05 15:27:07', NULL, NULL, '0', '1533298170302447618', '');
INSERT INTO `goods_sku_specs_value` VALUES ('1544929191942356994', '1535187344563052546', '1535186400517496833', 0, '2022-07-07 14:19:57', '2022-07-07 14:19:57', NULL, NULL, '0', '1535187344403668993', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/d256cde2-208a-4ec9-84bb-851f5d2a82e0.png');
INSERT INTO `goods_sku_specs_value` VALUES ('1544929192068186113', '1535187344667910145', '1535186511922405378', 0, '2022-07-07 14:19:57', '2022-07-07 14:19:57', NULL, NULL, '0', '1535187344403668993', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/7a63cb22-9657-4579-86e6-bb9a947220cf.png');
INSERT INTO `goods_sku_specs_value` VALUES ('1551769334104498177', '1535191486941569026', '1535190856290213889', 0, '2022-07-26 11:20:13', '2022-07-26 11:20:13', NULL, NULL, '0', '1535191486845100033', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/03c835f5-c9b2-4bfe-bcff-7be4f05461d3.jpg');
INSERT INTO `goods_sku_specs_value` VALUES ('1551769334419070978', '1535191486941569026', '1535190914179997697', 1, '2022-07-26 11:20:13', '2022-07-26 11:20:13', NULL, NULL, '0', '1535191486845100033', NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1551769334742032385', '1535191487033843713', '1535190856290213889', 0, '2022-07-26 11:20:13', '2022-07-26 11:20:13', NULL, NULL, '0', '1535191486845100033', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/03c835f5-c9b2-4bfe-bcff-7be4f05461d3.jpg');
INSERT INTO `goods_sku_specs_value` VALUES ('1551769335060799489', '1535191487033843713', '1535190950188097538', 1, '2022-07-26 11:20:13', '2022-07-26 11:20:13', NULL, NULL, '0', '1535191486845100033', NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1551769335379566594', '1535191487126118402', '1535190779425398785', 0, '2022-07-26 11:20:14', '2022-07-26 11:20:14', NULL, NULL, '0', '1535191486845100033', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/690fba69-9470-4949-90b5-0ff7c20df1ac.jpg');
INSERT INTO `goods_sku_specs_value` VALUES ('1551769335689945089', '1535191487126118402', '1535190914179997697', 1, '2022-07-26 11:20:14', '2022-07-26 11:20:14', NULL, NULL, '0', '1535191486845100033', NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1551769336012906497', '1535191487222587394', '1535190779425398785', 0, '2022-07-26 11:20:14', '2022-07-26 11:20:14', NULL, NULL, '0', '1535191486845100033', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/690fba69-9470-4949-90b5-0ff7c20df1ac.jpg');
INSERT INTO `goods_sku_specs_value` VALUES ('1551769336331673601', '1535191487222587394', '1535190950188097538', 1, '2022-07-26 11:20:14', '2022-07-26 11:20:14', NULL, NULL, '0', '1535191486845100033', NULL);

-- ----------------------------
-- Table structure for goods_specs
-- ----------------------------
DROP TABLE IF EXISTS `goods_specs`;
CREATE TABLE `goods_specs`  (
                                `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'PK',
                                `name` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '规格名',
                                `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                                `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
                                `create_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建者ID',
                                `update_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改者ID',
                                `del_flag` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                                PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '商品规格' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of goods_specs
-- ----------------------------
INSERT INTO `goods_specs` VALUES ('1532626600345128962', '尺寸', '2022-06-03 15:33:51', '2022-06-03 15:33:51', NULL, NULL, '0');
INSERT INTO `goods_specs` VALUES ('1532637571717451778', '属性', '2022-06-03 16:17:26', '2022-06-03 16:17:26', NULL, NULL, '0');
INSERT INTO `goods_specs` VALUES ('1532637637920346114', '颜色', '2022-06-03 16:17:42', '2022-06-03 16:17:42', NULL, NULL, '0');
INSERT INTO `goods_specs` VALUES ('1533790070751006721', '选择颜色', '2022-06-06 20:37:04', '2022-06-06 20:37:04', NULL, NULL, '0');
INSERT INTO `goods_specs` VALUES ('1533790184030769154', '选择属性', '2022-06-06 20:37:31', '2022-06-06 20:37:31', NULL, NULL, '0');

-- ----------------------------
-- Table structure for goods_specs_value
-- ----------------------------
DROP TABLE IF EXISTS `goods_specs_value`;
CREATE TABLE `goods_specs_value`  (
                                      `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'PK',
                                      `specs_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '规格ID',
                                      `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '规格值',
                                      `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                                      `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
                                      `create_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建者ID',
                                      `update_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改者ID',
                                      `del_flag` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                                      PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '商品规格值' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of goods_specs_value
-- ----------------------------
INSERT INTO `goods_specs_value` VALUES ('1532635376146108418', '1532626600345128962', 'xl', '2022-06-03 16:08:43', '2022-06-03 16:09:18', NULL, NULL, '0');
INSERT INTO `goods_specs_value` VALUES ('1532635600386183170', '', 'xxl', '2022-06-03 16:09:36', '2022-06-03 16:09:36', NULL, NULL, '0');
INSERT INTO `goods_specs_value` VALUES ('1532635928418504706', '1532626600345128962', 'xxl', '2022-06-03 16:10:55', '2022-06-03 16:10:55', NULL, NULL, '0');
INSERT INTO `goods_specs_value` VALUES ('1532635958902706178', '1532626600345128962', 'xxxl', '2022-06-03 16:11:02', '2022-06-03 16:11:02', NULL, NULL, '0');
INSERT INTO `goods_specs_value` VALUES ('1532637661773352962', '1532637637920346114', '白色', '2022-06-03 16:17:48', '2022-06-03 16:17:48', NULL, NULL, '0');
INSERT INTO `goods_specs_value` VALUES ('1532637684972048386', '1532637637920346114', '黑色', '2022-06-03 16:17:53', '2022-06-03 16:17:53', NULL, NULL, '0');
INSERT INTO `goods_specs_value` VALUES ('1533056307653984257', '1532637571717451778', '16kg', '2022-06-04 20:01:22', '2022-06-04 20:01:22', NULL, NULL, '0');
INSERT INTO `goods_specs_value` VALUES ('1533056325844680706', '1532637571717451778', '17kg', '2022-06-04 20:01:26', '2022-06-04 20:01:26', NULL, NULL, '0');
INSERT INTO `goods_specs_value` VALUES ('1533790357578485761', '1533790184030769154', ' i5-12500H 16G 512G', '2022-06-06 20:38:13', '2022-06-06 20:38:13', NULL, NULL, '0');
INSERT INTO `goods_specs_value` VALUES ('1533790579952095234', '1533790184030769154', ' i7-12700H 16G 512G', '2022-06-06 20:39:06', '2022-06-06 20:39:06', NULL, NULL, '0');
INSERT INTO `goods_specs_value` VALUES ('1533790772885884930', '1533790184030769154', ' i5-12500H 16G 512G RTX2050', '2022-06-06 20:39:52', '2022-06-06 20:39:52', NULL, NULL, '0');
INSERT INTO `goods_specs_value` VALUES ('1533790877215002625', '1533790070751006721', '晨雾灰', '2022-06-06 20:40:17', '2022-06-06 20:40:17', NULL, NULL, '0');
INSERT INTO `goods_specs_value` VALUES ('1533790905182621697', '1533790070751006721', '暗夜黑', '2022-06-06 20:40:23', '2022-06-06 20:40:23', NULL, NULL, '0');
INSERT INTO `goods_specs_value` VALUES ('1534908518105264129', '1533790070751006721', '土豪金', '2022-06-09 22:41:22', '2022-06-09 22:41:22', NULL, NULL, '0');
INSERT INTO `goods_specs_value` VALUES ('1534908798779699202', '1533790184030769154', 'i10', '2022-06-09 22:42:29', '2022-06-09 22:42:29', NULL, NULL, '0');
INSERT INTO `goods_specs_value` VALUES ('1535186400517496833', '1532637637920346114', '月亮银 8GB+128GB', '2022-06-10 17:05:34', '2022-06-10 17:05:34', NULL, NULL, '0');
INSERT INTO `goods_specs_value` VALUES ('1535186511922405378', '1532637637920346114', '星际黑 8GB+128GB', '2022-06-10 17:06:01', '2022-06-10 17:06:01', NULL, NULL, '0');
INSERT INTO `goods_specs_value` VALUES ('1535190779425398785', '1532637637920346114', '雪域白', '2022-06-10 17:22:58', '2022-06-10 17:22:58', NULL, NULL, '0');
INSERT INTO `goods_specs_value` VALUES ('1535190856290213889', '1532637637920346114', '曜金黑', '2022-06-10 17:23:17', '2022-06-10 17:23:17', NULL, NULL, '0');
INSERT INTO `goods_specs_value` VALUES ('1535190914179997697', '1532637571717451778', '8+128', '2022-06-10 17:23:31', '2022-06-10 17:23:31', NULL, NULL, '0');
INSERT INTO `goods_specs_value` VALUES ('1535190950188097538', '1532637571717451778', '8+256', '2022-06-10 17:23:39', '2022-06-10 17:23:39', NULL, NULL, '0');

-- ----------------------------
-- Table structure for goods_spu
-- ----------------------------
DROP TABLE IF EXISTS `goods_spu`;
CREATE TABLE `goods_spu`  (
                              `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'PK',
                              `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '商品名称',
                              `sub_title` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '子标题',
                              `spu_urls` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '商品图地址',
                              `status` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '1' COMMENT '状态：0.下架；1.上架',
                              `sales_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '销售价格（元）',
                              `original_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '原价（元）',
                              `cost_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '成本价（元）',
                              `sales_volume` int(11) NOT NULL DEFAULT 0 COMMENT '销量',
                              `freight_template_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '运费模板id',
                              `place_shipment_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '发货地点id',
                              `category_first_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '一级类目id',
                              `category_second_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '二级类目id',
                              `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                              `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
                              `create_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建者ID',
                              `update_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改者ID',
                              `del_flag` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                              `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '描述',
                              `enable_specs` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '多规格：0.否；1.是',
                              `low_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '最低价（元）',
                              `high_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '最高价（元）',
                              PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '商品spu' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of goods_spu
-- ----------------------------
INSERT INTO `goods_spu` VALUES ('1496390644407762945', 'Apple iPhone 13 Pro Max (A2644) 256GB 远峰蓝', '支持移动联通电信5G 双卡双待手机', '[\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/0a69731e-cb13-49d9-950f-0649bf32d044.jpg\"]', '1', 90000.00, 99799.00, 9000.00, 100, '1501942131653238786', NULL, '4', '5', '2022-02-23 15:45:03', '2022-02-23 15:45:03', NULL, NULL, '0', '<p><img src=\"https://img10.360buyimg.com/cms/jfs/t1/34607/36/15934/514434/61410fa8E2cd2eb63/05b406882a48787a.jpg\" alt=\"\" data-href=\"\" style=\"\"/></p>', '0', 99999.00, 99999.00);
INSERT INTO `goods_spu` VALUES ('1496394935734403073', '华为 HUAWEI Mate 40 RS 保时捷设计麒麟9000芯片 超感知徕卡电影五摄 8GB+256GB陶瓷黑5G全网通手机', '超感知徕卡电影五摄 8GB+256GB陶瓷黑5G全网通手机', '[\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/d03d26cc-4c43-4575-a7ff-7f5e500a2ee6.jpg\"]', '1', 110999.00, 110999.00, 110999.00, 111, '1496736152821198850', NULL, '4', '7', '2022-02-23 16:02:05', '2022-02-23 16:02:05', NULL, NULL, '0', '<p><img src=\"//img30.360buyimg.com/sku/jfs/t1/142137/25/11595/481220/5f918368Ec6a933d2/20917dcdfe53f26c.jpg\" alt=\"\" data-href=\"\" style=\"\"/></p>', '0', 99999.00, 99999.00);
INSERT INTO `goods_spu` VALUES ('1496687219931672578', '索尼（SONY）XR-65X95J 65英寸 全面屏电视 4K超高清HDR XR认知芯片 4K 120fps输入 ', '全面屏电视 4K超高清HDR XR认知芯片 4K 120fps输入 ', '[\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/b640cc30-c999-46e8-b9c2-677e3ce61b9c.jpg\",\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/d2f9b018-d304-4654-aa94-bafa746bf822.jpg\"]', '1', 17699.00, 18699.00, 8699.00, 11, '1498636653225537537', NULL, '1496677018553872385', '1496686626970333185', '2022-02-24 11:23:35', '2022-02-24 11:23:35', NULL, NULL, '0', '<p><img src=\"//img30.360buyimg.com/sku/jfs/t1/197296/23/3498/326122/611a2fa1E4cf9fd6d/19141d62f89961b9.jpg\" alt=\"\" data-href=\"\" style=\"\"/></p>', '0', 99999.00, 99999.00);
INSERT INTO `goods_spu` VALUES ('1498511063457775618', 'Apple AirPods (第三代) 配MagSafe无线充电盒 无线蓝牙耳机 Apple耳机 适用iPhone/iPad/Apple Watch', '配MagSafe无线充电盒', '[\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/f1201f1f-5c36-4468-a1f8-a634c894685d.jpg\"]', '1', 11399.00, 11399.00, 11399.00, 12, '1496736152821198850', NULL, '1498510244406673410', '1498510397314220034', '2022-03-01 12:10:53', '2022-03-01 12:10:53', NULL, NULL, '0', '<p><img src=\"https://img30.360buyimg.com/sku/jfs/t1/117805/35/24622/280548/616dd475E3940eed4/a43190fbff6ae1c5.jpg\" alt=\"\" data-href=\"\" style=\"\"/></p>', '0', 99999.00, 99999.00);
INSERT INTO `goods_spu` VALUES ('1533298170302447618', 'Apple MacBook Pro 14英寸 M1 Pro芯片(8核中央处理器 14核图形处理器) ', '16G 512G 深空灰 笔记本 MKGP3CH/A', '[\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/ea86c408-21eb-47af-81df-6367cb398075.jpg\"]', '1', NULL, NULL, NULL, 123, '1496736152821198850', NULL, '1504307346357383170', '1504307475378368514', '2022-06-05 12:02:27', '2022-06-05 23:30:28', NULL, NULL, '0', '<p><img src=\"https://img10.360buyimg.com/cms/jfs/t1/34607/36/15934/514434/61410fa8E2cd2eb63/05b406882a48787a.jpg\" alt=\"\" data-href=\"\" style=\"\"/></p>', '1', 100.00, 100.00);
INSERT INTO `goods_spu` VALUES ('1533380695339196418', 'Apple Watch SE【30天无忧试用套装】智能手表 GPS款 40毫米银色铝金属表壳 深渊蓝', 'Apple Watch SE【30天无忧试用套装】智能手表 GPS款 40毫米银色铝金属表壳 深渊蓝', '[\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/251d8c72-fcdb-4576-897a-2f46025c2bf4.jpg\",\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/291baa92-9273-44b7-bcd9-f4bf2501195c.jpg\",\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/0196ccfd-911a-468e-b7fe-37a089c9627f.jpg\"]', '1', NULL, NULL, NULL, 100, '1496736152821198850', NULL, '4', '5', '2022-06-05 17:30:23', '2022-06-05 17:30:23', NULL, NULL, '0', '<p>123123123123123123</p>', '0', 12245.00, 12245.00);
INSERT INTO `goods_spu` VALUES ('1533792035048427522', '联想ThinkPad neo 14英寸高性能标压商务办公轻薄笔记本电脑', NULL, '[\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/3dd8f6ba-cd27-42f6-9de8-47008fd98fcd.jpg\",\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/0d0b2c3a-1572-48c0-b663-625c2f20689c.jpg\",\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/52d03a9c-0b2e-4ce4-ab80-bbbef377d5d7.jpg\",\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/59fae85b-5f4e-4725-b3c8-6af9fe7d4f98.jpg\"]', '1', NULL, NULL, NULL, 1000, '1496736152821198850', NULL, '4', '1528947151174553602', '2022-06-06 20:44:53', '2022-06-06 20:44:53', NULL, NULL, '0', '<p><img src=\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/6550f400-e789-4e9c-916c-fdd92457f2da.jpg\" alt=\"\" data-href=\"\" style=\"\"/></p>', '1', 17699.00, 28699.00);
INSERT INTO `goods_spu` VALUES ('1535187344403668993', '米Redmi 红米K30S 至尊纪念版 双模5G手机', '', '[\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/d256cde2-208a-4ec9-84bb-851f5d2a82e0.png\",\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/7a63cb22-9657-4579-86e6-bb9a947220cf.png\",\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/6d49438c-a81c-4c1b-afea-be53e6a1d04c.jpg\"]', '1', NULL, NULL, NULL, 1203, '1496736152821198850', NULL, '4', '1496672394983804930', '2022-06-10 17:09:19', '2022-06-10 17:09:19', NULL, NULL, '0', '<p><img src=\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/c9107ec8-3d20-445d-9868-c7a5b426cc74.jpg\" alt=\"\" data-href=\"\" style=\"\"/><img src=\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/c09bbb18-33b0-488d-9b73-46d693e0bf6a.jpg\" alt=\"\" data-href=\"\" style=\"\"/></p>', '1', 4399.00, 4399.00);
INSERT INTO `goods_spu` VALUES ('1535191486845100033', 'HUAWEI P50 原色双影像单元 搭载HarmonyOS 2 万象双环设计 支持66W超级快充', NULL, '[\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/03c835f5-c9b2-4bfe-bcff-7be4f05461d3.jpg\",\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/690fba69-9470-4949-90b5-0ff7c20df1ac.jpg\",\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/6be07b2f-0cc6-4f8c-9dde-35acab18a116.jpg\",\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/6623a234-5562-4b62-9438-aa255f9954bb.jpg\",\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/30357715-2984-40df-8148-709ce9117ce9.jpg\"]', '1', NULL, NULL, NULL, 1236, '1536996605278457857', NULL, '4', '7', '2022-06-10 17:25:47', '2022-06-10 17:25:47', NULL, NULL, '0', '<p><br></p><p><img src=\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/30357715-2984-40df-8148-709ce9117ce9.jpg\" alt=\"\" data-href=\"\" style=\"\"/></p><p><br></p><p><img src=\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/72e4b9a1-9a6a-4ed8-b90e-8595793c9361.jpg\" alt=\"\" data-href=\"\" style=\"\"/></p>', '1', 0.10, 4888.10);

-- ----------------------------
-- Table structure for goods_spu_specs
-- ----------------------------
DROP TABLE IF EXISTS `goods_spu_specs`;
CREATE TABLE `goods_spu_specs`  (
                                    `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'PK',
                                    `spu_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '商品ID',
                                    `specs_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '规格ID',
                                    `sort` int(11) NOT NULL COMMENT '排序字段',
                                    `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                                    `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
                                    `create_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建者ID',
                                    `update_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改者ID',
                                    `del_flag` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                                    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '商品关联规格' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of goods_spu_specs
-- ----------------------------
INSERT INTO `goods_spu_specs` VALUES ('1533792065499074561', '1533792035048427522', '1533790070751006721', 0, '2022-06-06 20:45:00', '2022-06-06 20:45:00', NULL, NULL, '0');
INSERT INTO `goods_spu_specs` VALUES ('1533792065859784705', '1533792035048427522', '1533790184030769154', 1, '2022-06-06 20:45:00', '2022-06-06 20:45:00', NULL, NULL, '0');
INSERT INTO `goods_spu_specs` VALUES ('1544221323992268801', '1533298170302447618', '1532626600345128962', 0, '2022-06-05 12:02:34', '2022-06-05 12:02:34', NULL, NULL, '0');
INSERT INTO `goods_spu_specs` VALUES ('1544221324348784641', '1533298170302447618', '1532637637920346114', 1, '2022-06-05 12:02:36', '2022-06-05 12:02:36', NULL, NULL, '0');
INSERT INTO `goods_spu_specs` VALUES ('1544929191816527874', '1535187344403668993', '1532637637920346114', 0, '2022-06-10 17:09:20', '2022-06-10 17:09:20', NULL, NULL, '0');
INSERT INTO `goods_spu_specs` VALUES ('1551769333458575362', '1535191486845100033', '1532637637920346114', 0, '2022-06-10 17:25:47', '2022-06-10 17:25:47', NULL, NULL, '0');
INSERT INTO `goods_spu_specs` VALUES ('1551769333781536770', '1535191486845100033', '1532637571717451778', 1, '2022-06-10 17:25:47', '2022-06-10 17:25:47', NULL, NULL, '0');

-- ----------------------------
-- Table structure for loot_info
-- ----------------------------
DROP TABLE IF EXISTS `loot_info`;
CREATE TABLE `loot_info`  (
                              `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'PK',
                              `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '名称',
                              `spu_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'spuId',
                              `start_time` datetime(0) NOT NULL COMMENT '开始时间',
                              `end_time` datetime(0) NOT NULL COMMENT '结束时间',
                              `rule_desc` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '规则描述',
                              `share_title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '分享标题',
                              `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                              `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
                              `create_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建者ID',
                              `update_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改者ID',
                              `del_flag` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                              `total_num` int(11) NOT NULL DEFAULT 0 COMMENT '总数量',
                              `remain_num` int(11) NOT NULL COMMENT '剩余数量',
                              `launch_num` int(11) NOT NULL COMMENT '发起人数',
                              `win_num` int(11) NOT NULL COMMENT '中奖人数',
                              `pic_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '图片',
                              PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '夺宝信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of loot_info
-- ----------------------------
INSERT INTO `loot_info` VALUES ('1', '【环兴推荐】0元免费拿支持移动联通电信5G 双卡双待手机', '1496390644407762945', '2022-05-01 13:13:38', '2022-07-30 13:13:42', '一人参与一次', '【环兴推荐】0元免费拿支持移动联通电信5G 双卡双待手机', '2022-05-30 13:14:23', '2022-07-04 13:21:26', NULL, NULL, '0', 10, 9, 50, 1, 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/0a69731e-cb13-49d9-950f-0649bf32d044.jpg');
INSERT INTO `loot_info` VALUES ('1532006025327181826', 'Apple AirPods (第三代) 配MagSafe无线充电盒 无线蓝牙耳机 Apple耳机 适用iPhone/iPad/Apple Watch', '1498511063457775618', '2022-06-01 00:00:00', '2022-07-30 00:00:00', '啊实打实', NULL, '2022-06-01 22:27:56', '2022-07-04 13:21:28', NULL, NULL, '0', 11, 19, 20, 1, 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/3da3d902-ec1c-4107-b8e7-0614bbc1b89b.jpg');
INSERT INTO `loot_info` VALUES ('2', '【环兴推荐】全面屏电视 4K超高清HDR XR认知芯片 4K 120fps输入 ', '1496687219931672578', '2022-05-01 13:32:28', '2022-07-30 13:32:33', '12312313', '123123', '2022-05-30 13:32:56', '2022-07-04 13:21:31', NULL, NULL, '0', 10, 9, 50, 2, 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/b640cc30-c999-46e8-b9c2-677e3ce61b9c.jpg');
INSERT INTO `loot_info` VALUES ('3', '华为 HUAWEI Mate 40 RS 保时捷设计麒麟9000芯片 超感知徕卡电影五摄 8GB+256GB陶瓷黑5G全网通手机', '1496394935734403073', '2022-05-12 21:56:24', '2022-07-30 21:56:27', '123', '13123', '2022-05-30 21:56:48', '2022-07-04 13:21:34', NULL, NULL, '0', 10, 9, 4, 1, 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/d03d26cc-4c43-4575-a7ff-7f5e500a2ee6.jpg');

-- ----------------------------
-- Table structure for loot_stage
-- ----------------------------
DROP TABLE IF EXISTS `loot_stage`;
CREATE TABLE `loot_stage`  (
                               `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'PK',
                               `loot_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '夺宝主键',
                               `open_prize` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '开奖状态：0.未开奖；1.已开奖；',
                               `number_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '夺宝期编号',
                               `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                               `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
                               `create_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建者ID',
                               `update_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改者ID',
                               `del_flag` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                               PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '夺宝期记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of loot_stage
-- ----------------------------
INSERT INTO `loot_stage` VALUES ('1', '1', '0', '202205301', '2022-05-30 14:32:18', '2022-05-30 14:32:18', NULL, NULL, '0');
INSERT INTO `loot_stage` VALUES ('1532006025692086274', '1532006025327181826', '0', '202201', '2022-06-01 22:27:56', '2022-06-01 22:27:56', NULL, NULL, '0');
INSERT INTO `loot_stage` VALUES ('2', '2', '0', '202205301', '2022-05-30 14:31:47', '2022-05-30 14:31:47', NULL, NULL, '0');
INSERT INTO `loot_stage` VALUES ('3', '3', '0', '202205301', '2022-05-30 21:57:29', '2022-05-30 21:57:35', NULL, NULL, '0');

-- ----------------------------
-- Table structure for loot_user
-- ----------------------------
DROP TABLE IF EXISTS `loot_user`;
CREATE TABLE `loot_user`  (
                              `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'PK',
                              `loot_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '夺宝主键',
                              `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户主键',
                              `loot_stage_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '夺宝期主键',
                              `spu_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'spuId',
                              `status` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '状态：1.待开奖；2.开奖中；3.已开奖；4.开奖失败；',
                              `number_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '参与号码',
                              `win_status` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '中奖状态：0.未中奖；1.已中奖；',
                              `order_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单ID',
                              `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                              `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
                              `create_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建者ID',
                              `update_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改者ID',
                              `del_flag` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                              PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '夺宝用户参与记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for mall_config
-- ----------------------------
DROP TABLE IF EXISTS `mall_config`;
CREATE TABLE `mall_config`  (
                                `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'PK',
                                `notify_domain` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '商城回调域名',
                                `order_time_out` int(11) NULL DEFAULT NULL COMMENT '商城订单超时自动取消时间（分钟）',
                                `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                                `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
                                `create_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建者ID',
                                `update_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改者ID',
                                `del_flag` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0、显示；1、隐藏',
                                `express_key` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '快递100 key',
                                PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '商城配置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mall_config
-- ----------------------------
INSERT INTO `mall_config` VALUES ('1', 'https://hx-test.suokelian.com', 20, '2022-06-11 13:34:32', '2022-06-11 21:45:50', NULL, NULL, '0', 'aVlDomOE8022');

-- ----------------------------
-- Table structure for material
-- ----------------------------
DROP TABLE IF EXISTS `material`;
CREATE TABLE `material`  (
                             `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'PK',
                             `type` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '类型1、图片；2、视频',
                             `group_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '-1' COMMENT '分组ID  -1.未分组',
                             `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '素材名',
                             `url` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '素材链接',
                             `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                             `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '最后更新时间',
                             `create_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建者ID',
                             `update_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改者ID',
                             `is_del` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；'
) ENGINE = InnoDB AUTO_INCREMENT = 116 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '素材' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of material
-- ----------------------------
INSERT INTO `material` VALUES ('1504695204515053570', '1', '-1', '苹果mac', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/ea86c408-21eb-47af-81df-6367cb398075.jpg', '2022-03-18 13:44:27', '2022-06-15 21:27:47', NULL, NULL, '0');
INSERT INTO `material` VALUES ('1504696139664490498', '1', '-1', '47eb760c46e65eaf.jpg', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/d03d26cc-4c43-4575-a7ff-7f5e500a2ee6.jpg', '2022-03-18 13:48:10', '2022-03-18 13:48:10', NULL, NULL, '0');
INSERT INTO `material` VALUES ('1504696462470709249', '1', '-1', '苹果12pro', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/0a69731e-cb13-49d9-950f-0649bf32d044.jpg', '2022-03-18 13:49:27', '2022-06-15 21:27:59', NULL, NULL, '0');
INSERT INTO `material` VALUES ('1504700007987183618', '1', '-1', 'hx-weixin.jpg', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/217e60c1-68b3-46c8-aeab-cab272b16958.jpg', '2022-03-18 14:03:32', '2022-03-18 14:03:32', NULL, NULL, '0');
INSERT INTO `material` VALUES ('1504752267085643777', '1', '-1', '1647594069(1).jpg', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/5137138c-1436-412c-9fb7-74e80b357061.jpg', '2022-03-18 17:31:12', '2022-03-18 17:31:12', NULL, NULL, '0');
INSERT INTO `material` VALUES ('1504758894861910018', '1', '-1', 'gh_a9306ef99129_1280_好压看图.jpg', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/3da3d902-ec1c-4107-b8e7-0614bbc1b89b.jpg', '2022-03-18 17:57:32', '2022-03-18 17:57:32', NULL, NULL, '0');
INSERT INTO `material` VALUES ('1528342725464346625', '1', '1528340985293754370', '4132841608869152.jpg', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/d2f9b018-d304-4654-aa94-bafa746bf822.jpg', '2022-05-22 19:51:15', '2022-05-22 19:51:15', NULL, NULL, '0');
INSERT INTO `material` VALUES ('1528342777121394689', '1', '1528340985293754370', '1dea2fcc3736a729.jpg', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/b640cc30-c999-46e8-b9c2-677e3ce61b9c.jpg', '2022-05-22 19:51:27', '2022-05-22 19:51:27', NULL, NULL, '0');
INSERT INTO `material` VALUES ('1528343211701620738', '1', '1528340985293754370', '6e932b6e7023c311.jpg', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/291baa92-9273-44b7-bcd9-f4bf2501195c.jpg', '2022-05-22 19:53:11', '2022-05-22 19:53:11', NULL, NULL, '0');
INSERT INTO `material` VALUES ('1528343232018829314', '1', '1528340985293754370', '6e932b6e7023c311.jpg', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/251d8c72-fcdb-4576-897a-2f46025c2bf4.jpg', '2022-05-22 19:53:16', '2022-05-22 19:53:16', NULL, NULL, '0');
INSERT INTO `material` VALUES ('1528343272846184450', '1', '1528340985293754370', '4bc67fa702b930d1.jpg', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/0196ccfd-911a-468e-b7fe-37a089c9627f.jpg', '2022-05-22 19:53:26', '2022-05-22 19:53:26', NULL, NULL, '0');
INSERT INTO `material` VALUES ('1533791071709073409', '1', '1528340985293754370', '18d21aa7a7d96830.jpg', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/59fae85b-5f4e-4725-b3c8-6af9fe7d4f98.jpg', '2022-06-06 20:41:03', '2022-06-06 20:41:03', NULL, NULL, '0');
INSERT INTO `material` VALUES ('1533791071709073410', '1', '1528340985293754370', '4f4eff4149a85e67.jpg', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/0d0b2c3a-1572-48c0-b663-625c2f20689c.jpg', '2022-06-06 20:41:03', '2022-06-06 20:41:03', NULL, NULL, '0');
INSERT INTO `material` VALUES ('1533791071734239233', '1', '1528340985293754370', '4b505acb172a72fd.jpg', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/52d03a9c-0b2e-4ce4-ab80-bbbef377d5d7.jpg', '2022-06-06 20:41:03', '2022-06-06 20:41:03', NULL, NULL, '0');
INSERT INTO `material` VALUES ('1533791071851679746', '1', '1528340985293754370', 'ac40e62a59ed154b.jpg', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/3dd8f6ba-cd27-42f6-9de8-47008fd98fcd.jpg', '2022-06-06 20:41:03', '2022-06-06 20:41:03', NULL, NULL, '0');
INSERT INTO `material` VALUES ('1533791762414473218', '1', '-1', '58ba8ae6a0453f89.jpg', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/6550f400-e789-4e9c-916c-fdd92457f2da.jpg', '2022-06-06 20:43:48', '2022-06-06 20:43:48', NULL, NULL, '0');
INSERT INTO `material` VALUES ('1533812574471315457', '1', '1528340985293754370', '95eabadbc288bddf.jpg', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/d6ac601c-90fe-49bb-9531-8b7c441a0782.jpg', '2022-06-06 22:06:30', '2022-06-06 22:06:30', NULL, NULL, '0');
INSERT INTO `material` VALUES ('1534800073150111746', '1', '1528343960464576514', 'hx-logo.png', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/41b0da67-9da6-4a97-a3d8-a9cdbccf3149.png', '2022-06-09 15:30:27', '2022-06-09 15:30:27', NULL, NULL, '0');
INSERT INTO `material` VALUES ('1535186997048188930', '1', '1528340985293754370', 'f27f6d43162903d4.jpg', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/6d49438c-a81c-4c1b-afea-be53e6a1d04c.jpg', '2022-06-10 17:07:57', '2022-06-10 17:07:57', NULL, NULL, '0');
INSERT INTO `material` VALUES ('1535186997853495298', '1', '1528340985293754370', 'd4604c48890771ca.png', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/7a63cb22-9657-4579-86e6-bb9a947220cf.png', '2022-06-10 17:07:57', '2022-06-10 17:07:57', NULL, NULL, '0');
INSERT INTO `material` VALUES ('1535187000743370754', '1', '1528340985293754370', 'da83da34aaf7c4b2.png', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/d256cde2-208a-4ec9-84bb-851f5d2a82e0.png', '2022-06-10 17:07:58', '2022-06-10 17:07:58', NULL, NULL, '0');
INSERT INTO `material` VALUES ('1535188315980636161', '1', '1535188276256382978', 'f392108e528ebf9e.jpg', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/c9107ec8-3d20-445d-9868-c7a5b426cc74.jpg', '2022-06-10 17:13:11', '2022-06-10 17:13:11', NULL, NULL, '0');
INSERT INTO `material` VALUES ('1535188318774042626', '1', '1535188276256382978', '2eb5207c6d0a6408.jpg', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/c09bbb18-33b0-488d-9b73-46d693e0bf6a.jpg', '2022-06-10 17:13:12', '2022-06-10 17:13:12', NULL, NULL, '0');
INSERT INTO `material` VALUES ('1535189885065568257', '1', '1528340985293754370', '1120808958f98d7e.jpg', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/6be07b2f-0cc6-4f8c-9dde-35acab18a116.jpg', '2022-06-10 17:19:25', '2022-06-10 17:19:25', NULL, NULL, '0');
INSERT INTO `material` VALUES ('1535189885183008769', '1', '1528340985293754370', 'a1f1171441ba1ed6.jpg', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/690fba69-9470-4949-90b5-0ff7c20df1ac.jpg', '2022-06-10 17:19:25', '2022-06-10 17:19:25', NULL, NULL, '0');
INSERT INTO `material` VALUES ('1535189885283672065', '1', '1528340985293754370', '971e168223058454.jpg', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/03c835f5-c9b2-4bfe-bcff-7be4f05461d3.jpg', '2022-06-10 17:19:25', '2022-06-10 17:19:25', NULL, NULL, '0');
INSERT INTO `material` VALUES ('1535189943873904642', '1', '1528340985293754370', '8b9f9e3ab54946e5.jpg', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/30357715-2984-40df-8148-709ce9117ce9.jpg', '2022-06-10 17:19:39', '2022-06-10 17:19:39', NULL, NULL, '0');
INSERT INTO `material` VALUES ('1535189944029093890', '1', '1528340985293754370', '8c3be86db8a33eb7.jpg', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/6623a234-5562-4b62-9438-aa255f9954bb.jpg', '2022-06-10 17:19:39', '2022-06-10 17:19:39', NULL, NULL, '0');
INSERT INTO `material` VALUES ('1535190343167451137', '1', '1535188276256382978', '0d444c298f7762ec.jpg', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/56728ccd-8aeb-4cef-b506-8cb136ac9b38.jpg', '2022-06-10 17:21:14', '2022-06-10 17:21:14', NULL, NULL, '0');
INSERT INTO `material` VALUES ('1535190367108538369', '1', '1535188276256382978', '9f4578024dbfd379.jpg', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/990ad509-3ae2-4c64-a0dc-12f571307737.jpg', '2022-06-10 17:21:20', '2022-06-10 17:21:20', NULL, NULL, '0');
INSERT INTO `material` VALUES ('1535190380979101698', '1', '1535188276256382978', '54fbe66a9fc128e4.jpg', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/72e4b9a1-9a6a-4ed8-b90e-8595793c9361.jpg', '2022-06-10 17:21:23', '2022-06-10 17:21:23', NULL, NULL, '0');
INSERT INTO `material` VALUES ('1535190412738371586', '1', '1535188276256382978', 'cbcab542c4b4c66a.jpg', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/9da100bd-b7e9-4fa5-9389-2dc620c4ff30.jpg', '2022-06-10 17:21:31', '2022-06-10 17:21:31', NULL, NULL, '0');
INSERT INTO `material` VALUES ('1536957449957445634', '1', '-1', '9f4578024dbfd379.jpg', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/caf43fa7-7764-4cb8-bbdd-748f6e8f43e2.jpg', '2022-06-15 14:23:06', '2022-06-15 14:23:06', NULL, NULL, '0');
INSERT INTO `material` VALUES ('1541715763190796289', '1', '-1', '作者微信', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/733838f2-eaa0-4c2b-ae69-263e4af63cbb.jpg', '2022-06-28 17:30:56', '2022-06-28 17:31:08', NULL, NULL, '0');
INSERT INTO `material` VALUES ('1542433133660180482', '1', '1542429636659220482', '首页.jpg', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/f2dd3764-b12b-45e7-b097-188d386f2184.jpg', '2022-06-30 17:01:30', '2022-06-30 17:01:30', NULL, NULL, '0');
INSERT INTO `material` VALUES ('1542433915365199873', '1', '1542429636659220482', '9f9a640d-e761-4f0f-a7e2-5e8c666472f5 (1).jpg', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/9009ce80-9d48-406b-b782-86bae8c9a624.jpg', '2022-06-30 17:04:37', '2022-06-30 17:04:37', NULL, NULL, '0');
INSERT INTO `material` VALUES ('1542434051197734914', '1', '1542429636659220482', 'e54ffb64e295d5c06e510c7f86b29bd.jpg', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/75882ff1-f274-41df-9098-927498082b6c.jpg', '2022-06-30 17:05:09', '2022-06-30 17:05:09', NULL, NULL, '0');
INSERT INTO `material` VALUES ('1542434065408036866', '1', '1542429636659220482', 'ed02ac9b1c7e0fbe26a1934dfb6057d.jpg', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/06536b60-ee71-4829-acb1-a51ac22d8b2a.jpg', '2022-06-30 17:05:12', '2022-06-30 17:05:12', NULL, NULL, '0');
INSERT INTO `material` VALUES ('1542434080964710402', '1', '1542429636659220482', 'f979216a276604588ac00994f25f1ab.jpg', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/8bbb5338-5117-49db-b0a5-d1f80108baf1.jpg', '2022-06-30 17:05:16', '2022-06-30 17:05:16', NULL, NULL, '0');
INSERT INTO `material` VALUES ('1542434109980905473', '1', '1542429636659220482', '6034a9ca2ff43f5a549a5256abc6daf.jpg', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/5639bb25-7aba-4e5b-b5d7-557107d2fc36.jpg', '2022-06-30 17:05:23', '2022-06-30 17:05:23', NULL, NULL, '0');
INSERT INTO `material` VALUES ('1542435491915993090', '1', '1542429636659220482', '商品详情.jpg', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/8650dd66-5ea0-4311-b6d7-214f04a43ff8.jpg', '2022-06-30 17:10:53', '2022-06-30 17:10:53', NULL, NULL, '0');
INSERT INTO `material` VALUES ('1542435506252124161', '1', '1542429636659220482', '用户信息.jpg', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/8f4c452f-4fad-4e35-ad20-f5d5f554864d.jpg', '2022-06-30 17:10:56', '2022-06-30 17:10:56', NULL, NULL, '0');
INSERT INTO `material` VALUES ('1545642515990519809', '1', '-1', '安卓app下载地址.png', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/0b849f8b-eacb-436d-814b-0ff6e99ad9e4.png', '2022-07-09 13:34:27', '2022-07-09 13:34:27', NULL, NULL, '0');

-- ----------------------------
-- Table structure for material_group
-- ----------------------------
DROP TABLE IF EXISTS `material_group`;
CREATE TABLE `material_group`  (
                                   `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'PK',
                                   `name` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '分组名称',
                                   `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                                   `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '最后更新时间',
                                   `create_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建者ID',
                                   `update_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改者ID',
                                   `is_del` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.否；1.是；',
                                   `sort` int(11) NOT NULL DEFAULT 0 COMMENT '排序',
                                   `type` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '1' COMMENT '分组类型：1.图片；2.视频',
                                   PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '素材分组' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of material_group
-- ----------------------------
INSERT INTO `material_group` VALUES ('1528340985293754370', '商品图', '2022-05-22 19:44:20', '2022-05-22 19:44:20', NULL, NULL, '0', 0, '1');
INSERT INTO `material_group` VALUES ('1528343960464576514', 'LOGO', '2022-05-22 19:56:10', '2022-05-22 19:56:10', NULL, NULL, '0', 0, '1');
INSERT INTO `material_group` VALUES ('1535188276256382978', '商品详情图', '2022-06-10 17:13:02', '2022-06-10 17:13:02', NULL, NULL, '0', 0, '1');
INSERT INTO `material_group` VALUES ('1542429636659220482', '环兴商家版素材', '2022-06-30 16:47:37', '2022-06-30 16:47:37', NULL, NULL, '0', 0, '1');

-- ----------------------------
-- Table structure for mobile_theme
-- ----------------------------
DROP TABLE IF EXISTS `mobile_theme`;
CREATE TABLE `mobile_theme`  (
                                 `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'PK',
                                 `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '主题名称',
                                 `theme_color` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主颜色',
                                 `match_color` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '配色',
                                 `bottom_color` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '底色',
                                 `is_use` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '使用：0.否；1.是',
                                 `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                                 `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
                                 `create_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建者ID',
                                 `update_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改者ID',
                                 `del_flag` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                                 `tabbar_color` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '底部导航颜色',
                                 `tabbar_selected_color` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '底部导航选中颜色',
                                 `tabbar_border_style` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'tabBar上边框的颜色， 仅支持 black/white',
                                 `tabbar_bg_color` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'tabBar背景色，HexColor',
                                 `tabbar_item` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'tabBar明细',
                                 PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '商城移动端主题' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mobile_theme
-- ----------------------------
INSERT INTO `mobile_theme` VALUES ('1', '纯净绿', 'hx-green', 'hx-li-green', '#FFFFFF', '0', '2022-03-17 10:46:59', '2022-07-11 17:55:47', NULL, NULL, '0', NULL, NULL, NULL, NULL, '{\"id\":0,\"info\":[{\"visible\":true,\"selectedIconPath\":\"tabbar-home-hx-red.png\",\"index\":0,\"text\":\"首页\",\"iconPath\":\"home.png\"},{\"visible\":true,\"selectedIconPath\":\"tabbar-category-hx-red.png\",\"index\":1,\"text\":\"分类\",\"iconPath\":\"category.png\"},{\"visible\":true,\"selectedIconPath\":\"tabbar-shoppingcart-hx-red.png\",\"index\":2,\"text\":\"购物车\",\"iconPath\":\"shopping-cart.png\"},{\"visible\":true,\"selectedIconPath\":\"tabbar-user-hx-red.png\",\"index\":3,\"text\":\"我的\",\"iconPath\":\"user-info.png\"}]}');
INSERT INTO `mobile_theme` VALUES ('2', '樱花粉', 'hx-pink', 'hx-li-pink', '#FFFFFF', '0', '2022-03-17 11:28:42', '2022-07-11 17:54:15', NULL, NULL, '0', '#666666', '#FE496E', 'black', '#FFFFFF', '{\"id\":3,\"info\":[{\"visible\":true,\"selectedIconPath\":\"tabbar-home-hx-pink.png\",\"index\":0,\"text\":\"首页\",\"iconPath\":\"home.png\"},{\"visible\":true,\"selectedIconPath\":\"tabbar-category-hx-pink.png\",\"index\":1,\"text\":\"分类\",\"iconPath\":\"category.png\"},{\"visible\":true,\"selectedIconPath\":\"tabbar-shoppingcart-hx-pink.png\",\"index\":2,\"text\":\"购物车\",\"iconPath\":\"shopping-cart.png\"},{\"visible\":true,\"selectedIconPath\":\"tabbar-user-hx-pink.png\",\"index\":3,\"text\":\"我的\",\"iconPath\":\"user-info.png\"}]}');
INSERT INTO `mobile_theme` VALUES ('3', '魅力金', 'hx-golden', 'hx-li-golden', '#FFFFFF', '0', '2022-03-17 11:28:59', '2022-07-11 17:56:55', NULL, NULL, '0', '#000000', '#F9002C', 'black', NULL, '{\"id\":3,\"info\":[{\"visible\":true,\"selectedIconPath\":\"tabbar-home-hx-pink.png\",\"index\":0,\"text\":\"首页\",\"iconPath\":\"home.png\"},{\"visible\":true,\"selectedIconPath\":\"tabbar-category-hx-pink.png\",\"index\":1,\"text\":\"分类\",\"iconPath\":\"category.png\"},{\"visible\":true,\"selectedIconPath\":\"tabbar-shoppingcart-hx-pink.png\",\"index\":2,\"text\":\"购物车\",\"iconPath\":\"shopping-cart.png\"},{\"visible\":true,\"selectedIconPath\":\"tabbar-user-hx-pink.png\",\"index\":3,\"text\":\"我的\",\"iconPath\":\"user-info.png\"}]}');
INSERT INTO `mobile_theme` VALUES ('4', '热情红', 'hx-red', 'hx-li-red', '#FFFFFF\r', '1', '2022-03-17 17:03:30', '2022-06-18 11:04:42', NULL, NULL, '0', NULL, NULL, NULL, NULL, '{\"id\":3,\"info\":[{\"visible\":true,\"selectedIconPath\":\"tabbar-home-hx-pink.png\",\"index\":0,\"text\":\"首页\",\"iconPath\":\"home.png\"},{\"visible\":true,\"selectedIconPath\":\"tabbar-category-hx-pink.png\",\"index\":1,\"text\":\"分类\",\"iconPath\":\"category.png\"},{\"visible\":true,\"selectedIconPath\":\"tabbar-shoppingcart-hx-pink.png\",\"index\":2,\"text\":\"购物车\",\"iconPath\":\"shopping-cart.png\"},{\"visible\":true,\"selectedIconPath\":\"tabbar-user-hx-pink.png\",\"index\":3,\"text\":\"我的\",\"iconPath\":\"user-info.png\"}]}');
INSERT INTO `mobile_theme` VALUES ('5', '商务蓝', 'hx-blue', 'hx-li-blue', '#FFFFFF', '0', '2022-03-17 17:03:55', '2022-06-18 11:05:06', NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for order_info
-- ----------------------------
DROP TABLE IF EXISTS `order_info`;
CREATE TABLE `order_info`  (
                               `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'PK',
                               `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户主键',
                               `delivery_way` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '配送方式：1.普通快递；2.上门自提',
                               `order_no` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '订单单号',
                               `payment_type` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '支付类型：1.微信支付；2.支付宝支付',
                               `trade_type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '交易类型：（预留）',
                               `order_type` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单类型：1.普通订单；',
                               `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
                               `pay_status` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '支付状态：0.未支付；1.已支付;',
                               `status` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单状态：1.待付款；2.待发货；3.已完成',
                               `appraise_status` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '评价状态：0.待评价；1.已平价;',
                               `total_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '订单总金额（元）',
                               `freight_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '运费（元）',
                               `coupon_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '优惠券优惠金额（元）',
                               `payment_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '支付金额（总金额-优惠券优惠金额+运费 = 支付金额）',
                               `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                               `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
                               `update_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改者ID',
                               `create_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建者ID',
                               `del_flag` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                               `payment_time` datetime(0) NULL DEFAULT NULL COMMENT '付款时间',
                               `deliver_time` datetime(0) NULL DEFAULT NULL COMMENT '发货时间',
                               `cancel_time` datetime(0) NULL DEFAULT NULL COMMENT '取消时间',
                               `receiver_time` datetime(0) NULL DEFAULT NULL COMMENT '收货时间',
                               `transaction_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '微信支付单号',
                               `order_logistics_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单物流ID',
                               PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '订单信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_info
-- ----------------------------
INSERT INTO `order_info` VALUES ('1530901848307978241', '1499374465621065729', '1', '1530901847752183808', NULL, NULL, '1', NULL, '0', '11', '0', 110999.00, 1.00, 0.00, 111000.00, '2022-05-29 21:20:18', '2022-05-29 21:20:18', NULL, NULL, '0', NULL, NULL, '2022-05-29 21:50:18', NULL, NULL, NULL);
INSERT INTO `order_info` VALUES ('1530912175942717441', '1499374465621065729', '1', '1530912175420477440', NULL, NULL, '1', NULL, '0', '11', '0', 110999.00, 1.00, 0.00, 111000.00, '2022-05-29 22:01:20', '2022-05-29 22:05:34', NULL, NULL, '0', NULL, NULL, '2022-05-29 22:31:21', NULL, NULL, '1530912175598784513');
INSERT INTO `order_info` VALUES ('1530913806558064642', '1499374465621065729', '1', '1530913806006489088', NULL, NULL, '1', NULL, '0', '11', '0', 110999.00, 1.00, 0.00, 111000.00, '2022-05-29 22:07:49', '2022-05-30 17:42:26', NULL, NULL, '0', NULL, NULL, '2022-05-30 17:42:27', NULL, NULL, '1530913806197354498');
INSERT INTO `order_info` VALUES ('1533769540677304321', '1499374465621065729', '1', '1533769539848900608', NULL, NULL, '1', NULL, '0', '11', '0', 100.00, 1.00, 0.00, 101.00, '2022-06-06 19:15:30', '2022-06-06 19:15:30', NULL, NULL, '0', NULL, NULL, '2022-06-06 19:45:30', NULL, NULL, '1533769540291428354');
INSERT INTO `order_info` VALUES ('1533786839828299778', '1499374465621065729', '1', '1533786839285108736', NULL, NULL, '1', NULL, '0', '11', '0', 12245.00, 1.00, 0.00, 12246.00, '2022-06-06 20:24:14', '2022-06-06 20:24:14', NULL, NULL, '0', NULL, NULL, '2022-06-06 20:54:14', NULL, NULL, '1533786839475978241');
INSERT INTO `order_info` VALUES ('1535543466189410305', '1499374465621065729', '1', '1535543464354254848', '1', 'JSAPI', '1', NULL, '1', '3', '0', 4399.00, 1.00, 0.00, 4400.00, '2022-06-11 16:44:25', '2022-06-12 18:31:24', NULL, NULL, '0', '2022-06-11 16:45:25', '2022-06-11 23:52:01', NULL, NULL, NULL, '1535543465824505857');
INSERT INTO `order_info` VALUES ('1536190217883164674', '1499374465621065729', '1', '1536190217235599360', '1', 'JSAPI', '1', NULL, '1', '3', '0', 4888.00, 1.00, 0.00, 4889.00, '2022-06-13 11:34:23', '2022-06-15 22:17:31', NULL, NULL, '0', '2022-06-13 11:34:43', '2022-06-15 22:17:30', NULL, NULL, NULL, '1536190217782501377');
INSERT INTO `order_info` VALUES ('1542417299558502401', '1499374465621065729', '1', '1542417298814537728', NULL, NULL, '1', NULL, '0', '11', '0', 23587.00, 2.00, 0.00, 23589.00, '2022-06-30 15:58:35', '2022-06-30 15:58:35', NULL, NULL, '0', NULL, NULL, '2022-06-30 16:28:37', NULL, NULL, '1542417299470422018');
INSERT INTO `order_info` VALUES ('1542750553906073602', '1499374465621065729', '1', '1542750553279434752', '1', 'JSAPI', '1', NULL, '1', '11', '0', 0.10, 0.00, 0.00, 0.10, '2022-07-01 14:02:49', '2022-07-02 14:20:53', NULL, NULL, '0', NULL, NULL, NULL, NULL, '4200001482202207018953590376', '1542750553784438785');
INSERT INTO `order_info` VALUES ('1544871403606818818', '1499374465621065729', '1', '1544871401519976448', NULL, NULL, '1', NULL, '0', '11', '0', 4888.00, 0.00, 0.00, 4888.00, '2022-07-07 10:30:19', '2022-07-07 10:30:19', NULL, NULL, '0', NULL, NULL, '2022-07-07 11:00:21', NULL, NULL, '1544871403195777025');
INSERT INTO `order_info` VALUES ('1544872763123351554', '1499374465621065729', '1', '1544872761334304768', NULL, NULL, '1', NULL, '0', '11', '0', 4888.00, 0.00, 0.00, 4888.00, '2022-07-07 10:35:43', '2022-07-07 10:35:43', NULL, NULL, '0', NULL, NULL, '2022-07-07 11:05:44', NULL, NULL, '1544872762771030018');
INSERT INTO `order_info` VALUES ('1544873370676674562', '1499374465621065729', '1', '1544873368891822080', NULL, NULL, '1', NULL, '0', '11', '0', 4888.00, 0.00, 0.00, 4888.00, '2022-07-07 10:38:08', '2022-07-07 10:38:08', NULL, NULL, '0', NULL, NULL, '2022-07-07 11:08:09', NULL, NULL, '1544873370332741633');
INSERT INTO `order_info` VALUES ('1544877119419760641', '1499374465621065729', '1', '1544877117584576512', NULL, NULL, '1', NULL, '0', '11', '0', 0.10, 0.00, 0.00, 0.10, '2022-07-07 10:53:02', '2022-07-07 10:53:02', NULL, NULL, '0', NULL, NULL, '2022-07-07 11:23:03', NULL, NULL, '1544877119067439106');
INSERT INTO `order_info` VALUES ('1544880121438322689', '1499374465621065729', '1', '1544880119590555648', NULL, NULL, '1', NULL, '0', '11', '0', 0.10, 0.00, 0.00, 0.10, '2022-07-07 11:04:58', '2022-07-07 11:04:58', NULL, NULL, '0', NULL, NULL, '2022-07-07 11:34:58', NULL, NULL, '1544880121081806850');
INSERT INTO `order_info` VALUES ('1545649352601112577', '1499374465621065729', '1', '1545649351991365632', NULL, NULL, '1', NULL, '0', '11', '0', 4888.00, 0.00, 0.00, 4888.00, '2022-07-09 14:01:37', '2022-07-09 14:01:37', NULL, NULL, '0', NULL, NULL, '2022-07-09 14:31:38', NULL, NULL, '1545649352483672066');
INSERT INTO `order_info` VALUES ('1545649367537029122', '1499374465621065729', '1', '1545649366923087872', NULL, NULL, '1', NULL, '0', '11', '0', 4888.00, 0.00, 0.00, 4888.00, '2022-07-09 14:01:40', '2022-07-09 14:01:40', NULL, NULL, '0', NULL, NULL, '2022-07-09 14:31:42', NULL, NULL, '1545649367415394305');
INSERT INTO `order_info` VALUES ('1546433879971860482', '1499374465621065729', '1', '1546433879328559104', NULL, NULL, '1', NULL, '0', '11', '0', 0.10, 0.00, 0.00, 0.10, '2022-07-11 17:59:03', '2022-07-11 17:59:03', NULL, NULL, '0', NULL, NULL, '2022-07-11 18:29:04', NULL, NULL, '1546433879846031362');

-- ----------------------------
-- Table structure for order_item
-- ----------------------------
DROP TABLE IF EXISTS `order_item`;
CREATE TABLE `order_item`  (
                               `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'PK',
                               `order_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '订单主键',
                               `spu_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'spuId',
                               `sku_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'skuId',
                               `spu_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'spu名称',
                               `pic_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '商品图',
                               `sales_price` decimal(10, 2) NOT NULL COMMENT '销售价格（元）',
                               `buy_quantity` int(11) NULL DEFAULT NULL COMMENT '购买数量',
                               `total_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '订单金额（元）',
                               `freight_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '运费（元）',
                               `coupon_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '优惠券优惠金额（元）',
                               `payment_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '支付金额（订单金额-优惠券优惠金额+运费 = 支付金额）',
                               `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                               `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
                               `create_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建者ID',
                               `update_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改者ID',
                               `del_flag` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                               `status` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '状态：0.正常订单；1.退款中；2.退货退款中；3.已完成退款；4.已完成退货退款；',
                               `specs_info` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '规格信息',
                               PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '子订单信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_item
-- ----------------------------
INSERT INTO `order_item` VALUES ('1530901848664494081', '1530901848307978241', '1496394935734403073', NULL, '华为 HUAWEI Mate 40 RS 保时捷设计麒麟9000芯片 超感知徕卡电影五摄 8GB+256GB陶瓷黑5G全网通手机', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/d03d26cc-4c43-4575-a7ff-7f5e500a2ee6.jpg', 110999.00, 1, 110999.00, 1.00, 0.00, 111000.00, '2022-05-29 21:20:18', '2022-05-31 17:02:28', NULL, NULL, '0', '0', NULL);
INSERT INTO `order_item` VALUES ('1530912176295038978', '1530912175942717441', '1496394935734403073', NULL, '华为 HUAWEI Mate 40 RS 保时捷设计麒麟9000芯片 超感知徕卡电影五摄 8GB+256GB陶瓷黑5G全网通手机', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/d03d26cc-4c43-4575-a7ff-7f5e500a2ee6.jpg', 110999.00, 1, 110999.00, 1.00, 0.00, 111000.00, '2022-05-29 22:01:21', '2022-05-31 17:02:28', NULL, NULL, '0', '0', NULL);
INSERT INTO `order_item` VALUES ('1530913806922969090', '1530913806558064642', '1496394935734403073', NULL, '华为 HUAWEI Mate 40 RS 保时捷设计麒麟9000芯片 超感知徕卡电影五摄 8GB+256GB陶瓷黑5G全网通手机', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/d03d26cc-4c43-4575-a7ff-7f5e500a2ee6.jpg', 110999.00, 1, 110999.00, 1.00, 0.00, 111000.00, '2022-05-29 22:07:49', '2022-05-31 17:02:29', NULL, NULL, '0', '0', NULL);
INSERT INTO `order_item` VALUES ('1533769541063180289', '1533769540677304321', '1533298170302447618', '1533298173649502209', 'Apple MacBook Pro 14英寸 M1 Pro芯片(8核中央处理器 14核图形处理器) ', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/ea86c408-21eb-47af-81df-6367cb398075.jpg', 100.00, 1, 100.00, 1.00, 0.00, 101.00, '2022-06-06 19:15:30', '2022-06-06 19:15:30', NULL, NULL, '0', '0', 'xl;白色;');
INSERT INTO `order_item` VALUES ('1533786840201592833', '1533786839828299778', '1533380695339196418', '1533380695737655297', 'Apple Watch SE【30天无忧试用套装】智能手表 GPS款 40毫米银色铝金属表壳 深渊蓝', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/251d8c72-fcdb-4576-897a-2f46025c2bf4.jpg', 12245.00, 1, 12245.00, 1.00, 0.00, 12246.00, '2022-06-06 20:24:14', '2022-06-06 20:24:14', NULL, NULL, '0', '0', NULL);
INSERT INTO `order_item` VALUES ('1535104837444313090', '1535104837335261186', '1533792035048427522', '1533792035451080705', '联想ThinkPad neo 14英寸高性能标压商务办公轻薄笔记本电脑', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/3dd8f6ba-cd27-42f6-9de8-47008fd98fcd.jpg', 18699.00, 1, 18699.00, 1.00, 0.00, 18700.00, '2022-06-10 11:41:28', '2022-06-10 11:41:28', NULL, NULL, '0', '0', '晨雾灰; i5-12500H 16G 512G;');
INSERT INTO `order_item` VALUES ('1535543466541731841', '1535543466189410305', '1535187344403668993', '1535187344667910145', '米Redmi 红米K30S 至尊纪念版 双模5G手机', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/7a63cb22-9657-4579-86e6-bb9a947220cf.png', 4399.00, 1, 4399.00, 1.00, 0.00, 4400.00, '2022-06-11 16:44:25', '2022-06-11 16:44:25', NULL, NULL, '0', '0', '星际黑 8GB+128GB；');
INSERT INTO `order_item` VALUES ('1536190217983827970', '1536190217883164674', '1535191486845100033', '1535191487033843713', 'HUAWEI P50 原色双影像单元 搭载HarmonyOS 2 万象双环设计 支持66W超级快充', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/03c835f5-c9b2-4bfe-bcff-7be4f05461d3.jpg', 4888.00, 1, 4888.00, 1.00, 0.00, 4889.00, '2022-06-13 11:34:23', '2022-06-13 11:34:23', NULL, NULL, '0', '0', '曜金黑；8+256；');
INSERT INTO `order_item` VALUES ('1536268343891927042', '1536268343803846658', '1533298170302447618', '1533298174479974402', 'Apple MacBook Pro 14英寸 M1 Pro芯片(8核中央处理器 14核图形处理器) ', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/0a69731e-cb13-49d9-950f-0649bf32d044.jpg', 100.00, 3, 100.00, 3.00, 0.00, 303.00, '2022-06-13 16:44:50', '2022-06-13 16:44:50', NULL, NULL, '0', '0', 'xxl；白色；');
INSERT INTO `order_item` VALUES ('1536268551317037058', '1536268551212179458', '1533298170302447618', '1533298174479974402', 'Apple MacBook Pro 14英寸 M1 Pro芯片(8核中央处理器 14核图形处理器) ', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/0a69731e-cb13-49d9-950f-0649bf32d044.jpg', 100.00, 1, 100.00, 1.00, 0.00, 101.00, '2022-06-13 16:45:39', '2022-06-13 16:45:39', NULL, NULL, '0', '0', 'xxl；白色；');
INSERT INTO `order_item` VALUES ('1539866399895851009', '1539866399778410498', '1535191486845100033', '1535191487222587394', 'HUAWEI P50 原色双影像单元 搭载HarmonyOS 2 万象双环设计 支持66W超级快充', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/690fba69-9470-4949-90b5-0ff7c20df1ac.jpg', 4888.00, 1, 4888.00, 1.00, 0.00, 4889.00, '2022-06-23 15:02:13', '2022-06-23 15:02:13', NULL, NULL, '0', '0', '雪域白；8+256；');
INSERT INTO `order_item` VALUES ('1539866463615717378', '1539866463510859778', '1535191486845100033', '1535191487222587394', 'HUAWEI P50 原色双影像单元 搭载HarmonyOS 2 万象双环设计 支持66W超级快充', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/690fba69-9470-4949-90b5-0ff7c20df1ac.jpg', 4888.00, 1, 4888.00, 1.00, 0.00, 4889.00, '2022-06-23 15:02:28', '2022-06-23 15:02:28', NULL, NULL, '0', '0', '雪域白；8+256；');
INSERT INTO `order_item` VALUES ('1542417299642388481', '1542417299558502401', '1535191486845100033', '1535191487033843713', 'HUAWEI P50 原色双影像单元 搭载HarmonyOS 2 万象双环设计 支持66W超级快充', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/03c835f5-c9b2-4bfe-bcff-7be4f05461d3.jpg', 4888.00, 1, 4888.00, 1.00, 0.00, 4889.00, '2022-06-30 15:58:35', '2022-06-30 15:58:35', NULL, NULL, '0', '0', '曜金黑；8+256；');
INSERT INTO `order_item` VALUES ('1542417299730468865', '1542417299558502401', '1533792035048427522', '1533792035451080705', '联想ThinkPad neo 14英寸高性能标压商务办公轻薄笔记本电脑', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/3dd8f6ba-cd27-42f6-9de8-47008fd98fcd.jpg', 18699.00, 1, 18699.00, 1.00, 0.00, 18700.00, '2022-06-30 15:58:35', '2022-06-30 15:58:35', NULL, NULL, '0', '0', '晨雾灰； i5-12500H 16G 512G；');
INSERT INTO `order_item` VALUES ('1542720634597605378', '1542720634446610433', '1535191486845100033', '1535191486941569026', 'HUAWEI P50 原色双影像单元 搭载HarmonyOS 2 万象双环设计 支持66W超级快充', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/03c835f5-c9b2-4bfe-bcff-7be4f05461d3.jpg', 0.10, 1, 0.10, 0.00, 0.00, 0.10, '2022-07-01 12:03:56', '2022-07-01 12:03:56', NULL, NULL, '0', '1', '曜金黑；8+128；');
INSERT INTO `order_item` VALUES ('1542750554027708418', '1542750553906073602', '1535191486845100033', '1535191486941569026', 'HUAWEI P50 原色双影像单元 搭载HarmonyOS 2 万象双环设计 支持66W超级快充', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/03c835f5-c9b2-4bfe-bcff-7be4f05461d3.jpg', 0.10, 1, 0.10, 0.00, 0.00, 0.10, '2022-07-01 14:02:49', '2022-07-01 14:02:49', NULL, NULL, '0', '3', '曜金黑；8+128；');
INSERT INTO `order_item` VALUES ('1544871403984306178', '1544871403606818818', '1535191486845100033', '1535191487033843713', 'HUAWEI P50 原色双影像单元 搭载HarmonyOS 2 万象双环设计 支持66W超级快充', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/03c835f5-c9b2-4bfe-bcff-7be4f05461d3.jpg', 4888.00, 1, 4888.00, 0.00, 0.00, 4888.00, '2022-07-07 10:30:19', '2022-07-07 10:30:19', NULL, NULL, '0', '0', '曜金黑；8+256；');
INSERT INTO `order_item` VALUES ('1544872763479867393', '1544872763123351554', '1535191486845100033', '1535191487033843713', 'HUAWEI P50 原色双影像单元 搭载HarmonyOS 2 万象双环设计 支持66W超级快充', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/03c835f5-c9b2-4bfe-bcff-7be4f05461d3.jpg', 4888.00, 1, 4888.00, 0.00, 0.00, 4888.00, '2022-07-07 10:35:43', '2022-07-07 10:35:43', NULL, NULL, '0', '0', '曜金黑；8+256；');
INSERT INTO `order_item` VALUES ('1544873371033190402', '1544873370676674562', '1535191486845100033', '1535191487033843713', 'HUAWEI P50 原色双影像单元 搭载HarmonyOS 2 万象双环设计 支持66W超级快充', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/03c835f5-c9b2-4bfe-bcff-7be4f05461d3.jpg', 4888.00, 1, 4888.00, 0.00, 0.00, 4888.00, '2022-07-07 10:38:08', '2022-07-07 10:38:08', NULL, NULL, '0', '0', '曜金黑；8+256；');
INSERT INTO `order_item` VALUES ('1544877119772082178', '1544877119419760641', '1535191486845100033', '1535191486941569026', 'HUAWEI P50 原色双影像单元 搭载HarmonyOS 2 万象双环设计 支持66W超级快充', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/03c835f5-c9b2-4bfe-bcff-7be4f05461d3.jpg', 0.10, 1, 0.10, 0.00, 0.00, 0.10, '2022-07-07 10:53:02', '2022-07-07 10:53:02', NULL, NULL, '0', '0', '曜金黑；8+128；');
INSERT INTO `order_item` VALUES ('1544880121790644226', '1544880121438322689', '1535191486845100033', '1535191486941569026', 'HUAWEI P50 原色双影像单元 搭载HarmonyOS 2 万象双环设计 支持66W超级快充', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/03c835f5-c9b2-4bfe-bcff-7be4f05461d3.jpg', 0.10, 1, 0.10, 0.00, 0.00, 0.10, '2022-07-07 11:04:58', '2022-07-07 11:04:58', NULL, NULL, '0', '0', '曜金黑；8+128；');
INSERT INTO `order_item` VALUES ('1545611247634485250', '1545611247512850434', '1535191486845100033', '1535191486941569026', 'HUAWEI P50 原色双影像单元 搭载HarmonyOS 2 万象双环设计 支持66W超级快充', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/03c835f5-c9b2-4bfe-bcff-7be4f05461d3.jpg', 0.10, 1, 0.10, 0.00, 0.00, 0.10, '2022-07-09 11:30:12', '2022-07-09 11:30:12', NULL, NULL, '0', '0', '曜金黑；8+128；');
INSERT INTO `order_item` VALUES ('1545649352722747393', '1545649352601112577', '1535191486845100033', '1535191487033843713', 'HUAWEI P50 原色双影像单元 搭载HarmonyOS 2 万象双环设计 支持66W超级快充', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/03c835f5-c9b2-4bfe-bcff-7be4f05461d3.jpg', 4888.00, 1, 4888.00, 0.00, 0.00, 4888.00, '2022-07-09 14:01:37', '2022-07-09 14:01:37', NULL, NULL, '0', '0', '曜金黑；8+256；');
INSERT INTO `order_item` VALUES ('1545649367654469633', '1545649367537029122', '1535191486845100033', '1535191487033843713', 'HUAWEI P50 原色双影像单元 搭载HarmonyOS 2 万象双环设计 支持66W超级快充', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/03c835f5-c9b2-4bfe-bcff-7be4f05461d3.jpg', 4888.00, 1, 4888.00, 0.00, 0.00, 4888.00, '2022-07-09 14:01:40', '2022-07-09 14:01:40', NULL, NULL, '0', '0', '曜金黑；8+256；');
INSERT INTO `order_item` VALUES ('1546433880101883905', '1546433879971860482', '1535191486845100033', '1535191486941569026', 'HUAWEI P50 原色双影像单元 搭载HarmonyOS 2 万象双环设计 支持66W超级快充', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/03c835f5-c9b2-4bfe-bcff-7be4f05461d3.jpg', 0.10, 1, 0.10, 0.00, 0.00, 0.10, '2022-07-11 17:59:03', '2022-07-11 17:59:03', NULL, NULL, '0', '0', '曜金黑；8+128；');
INSERT INTO `order_item` VALUES ('1546785499414982657', '1546785499263987713', '1535191486845100033', '1535191487033843713', 'HUAWEI P50 原色双影像单元 搭载HarmonyOS 2 万象双环设计 支持66W超级快充', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/03c835f5-c9b2-4bfe-bcff-7be4f05461d3.jpg', 4888.00, 1, 4888.00, 0.00, 0.00, 4888.00, '2022-07-12 17:16:15', '2022-07-12 17:16:15', NULL, NULL, '0', '0', '曜金黑；8+256；');

-- ----------------------------
-- Table structure for order_logistics
-- ----------------------------
DROP TABLE IF EXISTS `order_logistics`;
CREATE TABLE `order_logistics`  (
                                    `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'PK',
                                    `telephone` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '联系电话',
                                    `addressee_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '收件人姓名',
                                    `detail_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '详细地址',
                                    `postal_code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '邮政编码',
                                    `logistics_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '物流公司编码',
                                    `logistics_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '物流公司名称',
                                    `logistics_no` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '物流单号',
                                    `state` char(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '物流状态：0在途，1揽收，2疑难，3签收，4退签，5派件，8清关，14拒签',
                                    `is_check` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '是否签收标记：0.未签收；1.已签收；',
                                    `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                                    `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
                                    `create_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建者ID',
                                    `update_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改者ID',
                                    `del_flag` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                                    `logistics_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '物流信息描述',
                                    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '订单物流' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_logistics
-- ----------------------------
INSERT INTO `order_logistics` VALUES ('1530901847938879490', '17615123397', '李家兴', '市府广场2201', NULL, NULL, NULL, NULL, NULL, '0', '2022-05-29 21:20:18', '2022-05-29 21:20:18', NULL, NULL, '0', NULL);
INSERT INTO `order_logistics` VALUES ('1530902087928565761', '17615123397', '李家兴', '市府广场2201', NULL, NULL, NULL, NULL, NULL, '0', '2022-05-29 21:21:15', '2022-05-29 21:21:15', NULL, NULL, '0', NULL);
INSERT INTO `order_logistics` VALUES ('1530912175598784513', '17615123397', '李家兴', '和饿', NULL, NULL, NULL, NULL, NULL, '0', '2022-05-29 22:01:20', '2022-05-29 22:01:20', NULL, NULL, '0', NULL);
INSERT INTO `order_logistics` VALUES ('1530913806197354498', '17615123397', '李家兴', '市府广场2201', NULL, NULL, NULL, NULL, NULL, '0', '2022-05-29 22:07:49', '2022-05-29 22:07:49', NULL, NULL, '0', NULL);
INSERT INTO `order_logistics` VALUES ('1530922796666535938', '17615123397', '李家兴', '市府广场2201', NULL, NULL, NULL, NULL, NULL, '0', '2022-05-29 22:43:32', '2022-05-29 22:43:32', NULL, NULL, '0', NULL);
INSERT INTO `order_logistics` VALUES ('1533769540291428354', '17615123397', '李家兴', '辽宁省沈阳市和平区市府广场2201', NULL, NULL, NULL, NULL, NULL, '0', '2022-06-06 19:15:30', '2022-06-06 19:15:30', NULL, NULL, '0', NULL);
INSERT INTO `order_logistics` VALUES ('1533786839475978241', '17615123397', '李家兴', '辽宁省沈阳市和平区市府广场2201', NULL, NULL, NULL, NULL, NULL, '0', '2022-06-06 20:24:14', '2022-06-06 20:24:14', NULL, NULL, '0', NULL);
INSERT INTO `order_logistics` VALUES ('1534829377447665665', '17615123397', '李家兴', '辽宁省沈阳市和平区市府广场2201', NULL, NULL, NULL, NULL, NULL, '0', '2022-06-09 17:26:54', '2022-06-09 17:26:54', NULL, NULL, '0', NULL);
INSERT INTO `order_logistics` VALUES ('1535272811342045186', '17615123397', '李家兴', '辽宁省沈阳市和平区市府广场2201', NULL, NULL, NULL, NULL, NULL, '0', '2022-06-10 22:48:56', '2022-06-10 22:48:56', NULL, NULL, '0', NULL);
INSERT INTO `order_logistics` VALUES ('1535528966593769474', '17615123397', '李家兴', '辽宁省沈阳市和平区市府广场2201', NULL, NULL, NULL, NULL, NULL, '0', '2022-06-11 15:46:48', '2022-06-11 15:46:48', NULL, NULL, '0', NULL);
INSERT INTO `order_logistics` VALUES ('1535531570631569410', '17615123397', '李家兴', '辽宁省沈阳市和平区市府广场2201', NULL, NULL, NULL, NULL, NULL, '0', '2022-06-11 15:57:09', '2022-06-11 15:57:09', NULL, NULL, '0', NULL);
INSERT INTO `order_logistics` VALUES ('1535532390743867393', '17615123397', '李家兴', '辽宁省沈阳市和平区市府广场2201', NULL, NULL, NULL, NULL, NULL, '0', '2022-06-11 16:00:24', '2022-06-11 16:00:24', NULL, NULL, '0', NULL);
INSERT INTO `order_logistics` VALUES ('1535532741941329922', '17615123397', '李家兴', '辽宁省沈阳市和平区市府广场2201', NULL, NULL, NULL, NULL, NULL, '0', '2022-06-11 16:01:48', '2022-06-11 16:01:48', NULL, NULL, '0', NULL);
INSERT INTO `order_logistics` VALUES ('1535543135611146241', '17615123397', '李家兴', '辽宁省沈阳市和平区市府广场2201', NULL, NULL, NULL, NULL, NULL, '0', '2022-06-11 16:43:06', '2022-06-11 16:43:06', NULL, NULL, '0', NULL);
INSERT INTO `order_logistics` VALUES ('1535543465824505857', '17615123397', '李家兴', '辽宁省沈阳市和平区-市府广场2201', NULL, 'shentong', '申通快递', '773164991339975', '3', '1', '2022-06-11 16:44:25', '2022-06-15 22:01:22', NULL, NULL, '0', '已签收，签收人凭取货码签收。');
INSERT INTO `order_logistics` VALUES ('1536190217782501377', '17615123397', '李家兴', '辽宁省沈阳市和平区-市府广场2201', NULL, 'yunda', '圆通速递', '1231232131231', '1', '0', '2022-06-13 11:34:23', '2022-06-15 22:19:41', NULL, NULL, '0', NULL);
INSERT INTO `order_logistics` VALUES ('1536191811655770113', '17615123397', '李家兴', '辽宁省沈阳市和平区-市府广场2201', NULL, NULL, NULL, NULL, NULL, '0', '2022-06-13 11:40:43', '2022-06-13 11:40:43', NULL, NULL, '0', NULL);
INSERT INTO `order_logistics` VALUES ('1537815545341222913', '17615123397', '李家兴', '辽宁省沈阳市和平区-市府广场2201', NULL, NULL, NULL, NULL, NULL, '0', '2022-06-17 23:12:51', '2022-06-17 23:12:51', NULL, NULL, '0', NULL);
INSERT INTO `order_logistics` VALUES ('1537815968882040833', '17615123397', '李家兴', '辽宁省沈阳市和平区-市府广场2201', NULL, NULL, NULL, NULL, NULL, '0', '2022-06-17 23:14:32', '2022-06-17 23:14:32', NULL, NULL, '0', NULL);
INSERT INTO `order_logistics` VALUES ('1542034765126164481', '17615123397', '李家兴', '辽宁省沈阳市和平区-市府广场2201', NULL, NULL, NULL, NULL, NULL, '0', '2022-06-29 14:38:30', '2022-06-29 14:38:30', NULL, NULL, '0', NULL);
INSERT INTO `order_logistics` VALUES ('1542417299470422018', '17615123397', '李家兴', '辽宁省沈阳市和平区-市府广场2201', NULL, NULL, NULL, NULL, NULL, '0', '2022-06-30 15:58:35', '2022-06-30 15:58:35', NULL, NULL, '0', NULL);
INSERT INTO `order_logistics` VALUES ('1542708304774729730', '17615123397', '李家兴', '辽宁省沈阳市和平区-市府广场2201', NULL, NULL, NULL, NULL, NULL, '0', '2022-07-01 11:14:53', '2022-07-01 11:14:53', NULL, NULL, '0', NULL);
INSERT INTO `order_logistics` VALUES ('1542708546400194561', '17615123397', '李家兴', '辽宁省沈阳市和平区-市府广场2201', NULL, NULL, NULL, NULL, NULL, '0', '2022-07-01 11:15:50', '2022-07-01 11:15:50', NULL, NULL, '0', NULL);
INSERT INTO `order_logistics` VALUES ('1542720634308198401', '17615123397', '李家兴', '辽宁省沈阳市和平区-市府广场2201', NULL, NULL, NULL, NULL, NULL, '0', '2022-07-01 12:03:56', '2022-07-01 12:03:56', NULL, NULL, '0', NULL);
INSERT INTO `order_logistics` VALUES ('1542750417444392961', '17615123397', '李家兴', '辽宁省沈阳市和平区-市府广场2201', NULL, NULL, NULL, NULL, NULL, '0', '2022-07-01 14:02:17', '2022-07-01 14:02:17', NULL, NULL, '0', NULL);
INSERT INTO `order_logistics` VALUES ('1542750553784438785', '17615123397', '李家兴', '辽宁省沈阳市和平区-市府广场2201', NULL, NULL, NULL, NULL, NULL, '0', '2022-07-01 14:02:49', '2022-07-01 14:02:49', NULL, NULL, '0', NULL);
INSERT INTO `order_logistics` VALUES ('1544505777734328321', '17615123397', '李家兴', '辽宁省沈阳市和平区-市府广场2201', NULL, NULL, NULL, NULL, NULL, '0', '2022-07-06 10:17:24', '2022-07-06 10:17:24', NULL, NULL, '0', NULL);
INSERT INTO `order_logistics` VALUES ('1544871403195777025', '17615123397', '李家兴', '辽宁省沈阳市和平区-市府广场2201', NULL, NULL, NULL, NULL, NULL, '0', '2022-07-07 10:30:19', '2022-07-07 10:30:19', NULL, NULL, '0', NULL);
INSERT INTO `order_logistics` VALUES ('1544872762771030018', '17615123397', '李家兴', '辽宁省沈阳市和平区-市府广场2201', NULL, NULL, NULL, NULL, NULL, '0', '2022-07-07 10:35:43', '2022-07-07 10:35:43', NULL, NULL, '0', NULL);
INSERT INTO `order_logistics` VALUES ('1544873370332741633', '17615123397', '李家兴', '辽宁省沈阳市和平区-市府广场2201', NULL, NULL, NULL, NULL, NULL, '0', '2022-07-07 10:38:08', '2022-07-07 10:38:08', NULL, NULL, '0', NULL);
INSERT INTO `order_logistics` VALUES ('1544877119067439106', '17615123397', '李家兴', '辽宁省沈阳市和平区-市府广场2201', NULL, NULL, NULL, NULL, NULL, '0', '2022-07-07 10:53:02', '2022-07-07 10:53:02', NULL, NULL, '0', NULL);
INSERT INTO `order_logistics` VALUES ('1544880121081806850', '17615123397', '李家兴', '辽宁省沈阳市和平区-市府广场2201', NULL, NULL, NULL, NULL, NULL, '0', '2022-07-07 11:04:57', '2022-07-07 11:04:57', NULL, NULL, '0', NULL);
INSERT INTO `order_logistics` VALUES ('1544925695617318914', '17615123397', '李家兴', '辽宁省沈阳市和平区-市府广场2201', NULL, NULL, NULL, NULL, NULL, '0', '2022-07-07 14:06:03', '2022-07-07 14:06:03', NULL, NULL, '0', NULL);
INSERT INTO `order_logistics` VALUES ('1544928240293167106', '17615123397', '李家兴', '辽宁省沈阳市和平区-市府广场2201', NULL, NULL, NULL, NULL, NULL, '0', '2022-07-07 14:16:10', '2022-07-07 14:16:10', NULL, NULL, '0', NULL);
INSERT INTO `order_logistics` VALUES ('1544929036980240386', '17615123397', '李家兴', '辽宁省沈阳市和平区-市府广场2201', NULL, NULL, NULL, NULL, NULL, '0', '2022-07-07 14:19:20', '2022-07-07 14:19:20', NULL, NULL, '0', NULL);
INSERT INTO `order_logistics` VALUES ('1544966210098348034', '17615123397', '李家兴', '辽宁省沈阳市和平区-市府广场2201', NULL, NULL, NULL, NULL, NULL, '0', '2022-07-07 16:47:02', '2022-07-07 16:47:02', NULL, NULL, '0', NULL);
INSERT INTO `order_logistics` VALUES ('1545592618683600897', '17615123397', '李家兴', '辽宁省沈阳市和平区-市府广场2201', NULL, NULL, NULL, NULL, NULL, '0', '2022-07-09 10:16:10', '2022-07-09 10:16:10', NULL, NULL, '0', NULL);
INSERT INTO `order_logistics` VALUES ('1545649352483672066', '17615123397', '李家兴', '辽宁省沈阳市和平区-市府广场2201', NULL, NULL, NULL, NULL, NULL, '0', '2022-07-09 14:01:37', '2022-07-09 14:01:37', NULL, NULL, '0', NULL);
INSERT INTO `order_logistics` VALUES ('1545649367415394305', '17615123397', '李家兴', '辽宁省沈阳市和平区-市府广场2201', NULL, NULL, NULL, NULL, NULL, '0', '2022-07-09 14:01:40', '2022-07-09 14:01:40', NULL, NULL, '0', NULL);
INSERT INTO `order_logistics` VALUES ('1546433879846031362', '17615123397', '李家兴', '辽宁省沈阳市和平区-市府广场2201', NULL, NULL, NULL, NULL, NULL, '0', '2022-07-11 17:59:02', '2022-07-11 17:59:02', NULL, NULL, '0', NULL);

-- ----------------------------
-- Table structure for order_logistics_detail
-- ----------------------------
DROP TABLE IF EXISTS `order_logistics_detail`;
CREATE TABLE `order_logistics_detail`  (
                                           `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'PK',
                                           `logistics_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '物流信息主键',
                                           `logistics_time` datetime(0) NULL DEFAULT NULL COMMENT '物流时间',
                                           `logistics_context` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '物流信息',
                                           `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                                           `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
                                           `create_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建者ID',
                                           `update_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改者ID',
                                           `del_flag` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                                           PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '物流信息明细' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_logistics_detail
-- ----------------------------
INSERT INTO `order_logistics_detail` VALUES ('1535956902903226370', '1535543465824505857', '2022-06-12 19:29:52', '已签收，签收人凭取货码签收。', '2022-06-12 20:07:17', '2022-06-12 20:07:17', NULL, NULL, '0');
INSERT INTO `order_logistics_detail` VALUES ('1535956902907420673', '1535543465824505857', '2022-06-12 14:34:24', '快件已暂存至沈阳局西小区店菜鸟驿站，如有疑问请联系157xxxx9918', '2022-06-12 20:07:17', '2022-06-28 17:53:46', NULL, NULL, '0');
INSERT INTO `order_logistics_detail` VALUES ('1535956902907420674', '1535543465824505857', '2022-06-12 13:40:09', '【辽宁沈阳和平北】的派件员【杨天越】正在为您派件，如有疑问请联系派件员，联系电话【150xxxx2611】，快件已消毒，申通小哥已测体温，请放心收寄快递（95132为申通业务员外呼专属号码，可放心接听）', '2022-06-12 20:07:17', '2022-06-28 17:53:59', NULL, NULL, '0');
INSERT INTO `order_logistics_detail` VALUES ('1535956902907420675', '1535543465824505857', '2022-06-12 13:00:01', '快件已到达【辽宁沈阳和平北】扫描员是【和平北】', '2022-06-12 20:07:17', '2022-06-12 20:07:17', NULL, NULL, '0');
INSERT INTO `order_logistics_detail` VALUES ('1535956902907420676', '1535543465824505857', '2022-06-12 07:20:16', '快件由【辽宁沈阳转运中心】发往【辽宁沈阳和平北】，包裹已消杀', '2022-06-12 20:07:17', '2022-06-12 20:07:17', NULL, NULL, '0');
INSERT INTO `order_logistics_detail` VALUES ('1535956902907420677', '1535543465824505857', '2022-06-12 07:08:50', '快件已到达【辽宁沈阳转运中心】扫描员是【出港称重1】', '2022-06-12 20:07:17', '2022-06-12 20:07:17', NULL, NULL, '0');
INSERT INTO `order_logistics_detail` VALUES ('1535956902907420678', '1535543465824505857', '2022-06-11 18:53:06', '快件由【辽宁沈阳市场部五部】发往【辽宁沈阳转运中心】', '2022-06-12 20:07:17', '2022-06-12 20:07:17', NULL, NULL, '0');
INSERT INTO `order_logistics_detail` VALUES ('1535956902907420679', '1535543465824505857', '2022-06-11 18:23:50', '【辽宁沈阳市场部五部】的收件员【刘腾飞】已收件', '2022-06-12 20:07:17', '2022-06-12 20:07:17', NULL, NULL, '0');
INSERT INTO `order_logistics_detail` VALUES ('1535956902907420680', '1535543465824505857', '2022-06-11 17:44:03', '【辽宁沈阳市场部五部】的收件员【出港称重1】已收件', '2022-06-12 20:07:17', '2022-06-12 20:07:17', NULL, NULL, '0');

-- ----------------------------
-- Table structure for order_refund
-- ----------------------------
DROP TABLE IF EXISTS `order_refund`;
CREATE TABLE `order_refund`  (
                                 `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'PK',
                                 `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户主键',
                                 `order_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '订单ID',
                                 `order_item_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '子订单ID',
                                 `status` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '退款状态：1.退款中；2.退货退款中；11.同意退款；12.同意退货退款；21.拒绝退款；22.拒绝退货退款',
                                 `arrival_status` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '退款是否到账：0.未到账；1.已到账',
                                 `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                                 `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
                                 `create_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建者ID',
                                 `update_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改者ID',
                                 `del_flag` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                                 `refund_amount` decimal(10, 2) NOT NULL COMMENT '退款金额',
                                 `refund_trade_no` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '退款流水号',
                                 `refund_reason` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '退款原因',
                                 `refuse_reason` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '拒绝退款原因',
                                 `user_received_account` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '退款入账账户',
                                 `refund_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '退款单号',
                                 PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '商城退款单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_refund
-- ----------------------------
INSERT INTO `order_refund` VALUES ('1', '1499374465621065729', '1530913806558064642', '1530913806922969090', '21', '0', '2022-05-31 16:52:04', '2022-07-02 16:29:47', NULL, NULL, '0', 100.00, NULL, '不想要了', '超出规定退款时间.', NULL, NULL);
INSERT INTO `order_refund` VALUES ('1542795446368665601', '1499374465621065729', '1542750553906073602', '1542750554027708418', '11', '1', '2022-07-01 17:01:09', '2022-07-02 16:29:49', NULL, NULL, '0', 0.10, NULL, '不要了', NULL, '支付用户零钱', '50300002532022070222255718613');
INSERT INTO `order_refund` VALUES ('1543049478496595970', '1499374465621065729', '1542720634446610433', '1542720634597605378', '22', '0', '2022-07-02 09:50:34', '2022-07-02 16:29:50', NULL, NULL, '0', 0.10, NULL, '7天无理由退款', '超出7天，不允许退款.', NULL, NULL);
INSERT INTO `order_refund` VALUES ('1543147854714171394', '1499374465621065729', '1542720634446610433', '1542720634597605378', '1', '0', '2022-07-02 16:21:33', '2022-07-02 16:29:51', NULL, NULL, '0', 0.10, NULL, '我不要了', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for shopping_cart
-- ----------------------------
DROP TABLE IF EXISTS `shopping_cart`;
CREATE TABLE `shopping_cart`  (
                                  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'PK',
                                  `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户ID',
                                  `spu_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '商品ID',
                                  `sku_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'skuId',
                                  `quantity` int(11) NULL DEFAULT NULL COMMENT '加入数量',
                                  `spu_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '商品名称',
                                  `sales_price` decimal(10, 2) NOT NULL COMMENT '销售价格（元）',
                                  `pic_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '商品图',
                                  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                                  `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
                                  `create_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建者ID',
                                  `update_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改者ID',
                                  `del_flag` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0、显示；1、隐藏',
                                  `specs_info` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '规格信息',
                                  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '购物车' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of shopping_cart
-- ----------------------------
INSERT INTO `shopping_cart` VALUES ('1539871332980363266', '1539866194735665154', '1535191486845100033', '1535191487222587394', 1, 'HUAWEI P50 原色双影像单元 搭载HarmonyOS 2 万象双环设计 支持66W超级快充', 4888.00, 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/03c835f5-c9b2-4bfe-bcff-7be4f05461d3.jpg', '2022-06-23 15:21:49', '2022-06-23 15:21:49', NULL, NULL, '0', '雪域白;8+256;');

-- ----------------------------
-- Table structure for user_address
-- ----------------------------
DROP TABLE IF EXISTS `user_address`;
CREATE TABLE `user_address`  (
                                 `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'PK',
                                 `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户主键',
                                 `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                                 `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
                                 `create_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建者ID',
                                 `update_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改者ID',
                                 `del_flag` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                                 `addressee_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '收件人姓名',
                                 `telephone` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '联系电话',
                                 `postal_code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '邮政编码',
                                 `province_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '省名称',
                                 `city_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '市名称',
                                 `county_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '区名称',
                                 `is_default` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '默认地址 0.否；1.是；',
                                 `detail_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '详细地址',
                                 PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户收货地址' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_address
-- ----------------------------
INSERT INTO `user_address` VALUES ('1530185236315066369', '1499374465621065729', '2022-05-27 21:52:44', '2022-05-30 23:11:44', NULL, NULL, '0', '李家兴', '17615123397', NULL, '辽宁省', '沈阳市', '和平区', '1', '市府广场2201');

-- ----------------------------
-- Table structure for user_bill
-- ----------------------------
DROP TABLE IF EXISTS `user_bill`;
CREATE TABLE `user_bill`  (
                              `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'PK',
                              `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户主键',
                              `bill_amount` decimal(10, 2) NOT NULL COMMENT '账单金额（元）',
                              `bill_type` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '账单类型：1.收入；2.支出；',
                              `bill_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '账单描述',
                              `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                              `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
                              `create_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建者ID',
                              `update_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改者ID',
                              `del_flag` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                              PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户账单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_bill
-- ----------------------------
INSERT INTO `user_bill` VALUES ('1', '1499374465621065729', 10.00, '1', '手动充值到账户余额', '2022-07-11 14:17:58', '2022-07-11 14:17:58', NULL, NULL, '0');

-- ----------------------------
-- Table structure for user_info
-- ----------------------------
DROP TABLE IF EXISTS `user_info`;
CREATE TABLE `user_info`  (
                              `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'PK',
                              `user_number` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户编号',
                              `nick_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '昵称',
                              `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '手机号',
                              `password` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '密码',
                              `user_grade` tinyint(2) NULL DEFAULT 0 COMMENT '用户等级：0、普通用户；1.会员',
                              `sex` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '性别：1、男；2、女；0、未知；',
                              `avatar_url` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '头像',
                              `city` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '所在城市',
                              `country` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '所在国家',
                              `province` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '所在省份',
                              `del_flag` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                              `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                              `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
                              `user_source` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户来源：微信小程序.WX-MA；普通H5.H5；APP.APP；',
                              `parent_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '上级用户主键',
                              `account_balance` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '账户余额（元）',
                              PRIMARY KEY (`id`) USING BTREE,
                              UNIQUE INDEX `hx_phone`(`phone`) USING BTREE COMMENT '用户手机号',
                              INDEX `ids_number`(`user_number`) USING BTREE COMMENT '用户编号'
) ENGINE = InnoDB AUTO_INCREMENT = 50 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '商城用户' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_info
-- ----------------------------
INSERT INTO `user_info` VALUES ('1499374465621065729', 1, '环兴', '17615123397', NULL, 0, '1', 'https://thirdwx.qlogo.cn/mmopen/vi_32/ljAibJg5VULIPGG51fAXQ6dqYryMukM7ksQkO20S77KbMa1DoCTvus69Q14vDxjoQkqJYhMaTgDibkV55PHtic8lw/132', '沈阳市', NULL, '辽宁省', '0', '2022-03-03 21:21:44', '2022-07-12 13:59:36', 'APP', '1504006490705674242', 10.00);

SET FOREIGN_KEY_CHECKS = 1;
