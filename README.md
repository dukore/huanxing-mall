# HuanXing 商城 （开发中）点个star支持一下吧

#### 项目介绍
环兴商城基于 Spring Cloud 微服务商城系统，前后端分离，前端采用 Vue3 Element Plus 移动端采用 uniapp一套代码，可发布到iOS、Android、以及各种小程序。核心功能包括（sku、运费模板、素材库、二级分销、快递物流、线下聚合码收款）

#### 核心依赖

| 依赖                   | 版本     |
|----------------------|--------|
| Spring Boot          | 2.6.3  |
| Spring Cloud         | 2021   |
| Spring Cloud Alibaba | 2021.1 |
| Sa-Token             | 1.30.0 |
| Mybatis-Plus         | 3.5.1  |
| Hutool               | 5.7.20 |
| Vue                  | 3.2.25 |
| Element-Plus         | 2.2.2  |
| Echarts              | 5.3.2 |



#### 项目结构


#### 系统功能
1. 用户管理
2. 角色管理
3. 菜单管理
4. 部门管理
5. 登录日志
6. 操作日志
7. 服务监控
8. 文件存储配置（阿里云、腾讯云、七牛云）

#### 商城功能
1. 商品管理
2. 商城分类
3. 商城用户
4. 商城订单
5. 素材库
6. 素材分组
7. 运费模版
8. 商城退单
9. 分销管理
10. 主题配置
11. 聚合码收款（微信已完成、支付宝开发中）
括弧 支持账户余额付款 <br />
![聚合码](db/demohttps___hx-test.suokelian.com_.png)
12. 聚合码退款 （开发中）
#### 商城后台移动端示例图
<div align="center">
<img src="https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/f2dd3764-b12b-45e7-b097-188d386f2184.jpg" height="330" width="190" >

<img src="https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/5639bb25-7aba-4e5b-b5d7-557107d2fc36.jpg" height="330" width="190" >

<img src="https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/8bbb5338-5117-49db-b0a5-d1f80108baf1.jpg" height="330" width="190" >

<img src="https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/8f4c452f-4fad-4e35-ad20-f5d5f554864d.jpg" height="330" width="190" >


 </div>
<div align="center">

<img src="https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/75882ff1-f274-41df-9098-927498082b6c.jpg" height="330" width="190" >

<img src="https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/06536b60-ee71-4829-acb1-a51ac22d8b2a.jpg" height="330" width="190" >

<img src="https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/8650dd66-5ea0-4311-b6d7-214f04a43ff8.jpg" height="330" width="190" >
 </div>


#### 商城后台演示地址
<<<<<<< hx_dev_v1.0_20220627
[商城后台演示地址](http://mall.huanxing.shop)：http://mall.huanxing.shop 账号/密码 admin/123456
=======
#### 商城演示
| 安卓APP  |  <img src="https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/0b849f8b-eacb-436d-814b-0ff6e99ad9e4.png" height="300" width="300" >  |
|---|---|
| 商城H5  | <img src="https://huanxing.oss-cn-beijing.aliyuncs.com/hx-h5.png" height="300" width="300" > |
|---|---|
| 微信小程序 | <img src="https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/3da3d902-ec1c-4107-b8e7-0614bbc1b89b.jpg" height="300" width="300" > |
#### 上传命令
- scp huanxing-gateway.jar root@124.223.202.234:/root/huanxing
- scp huanxing-auth.jar root@124.223.202.234:/root/huanxing
- scp huanxing-upms-admin.jar root@124.223.202.234:/root/huanxing
- scp huanxing-mall-admin.jar root@124.223.202.234:/root/huanxing
- scp huanxing-miniapp-weixin.jar root@124.223.202.234:/root/huanxing
- scp huanxing-mall-api.jar root@124.223.202.234:/root/huanxing
- scp huanxing-pay-api.jar root@124.223.202.234:/root/huanxing
#### 启动命令
| nacos服务 | 8848 |  Linux/Unix/Mac: sh startup.sh -m standalone Windows: startup.cmd -m standalone   |
|---|---|---|
| 网关服务   | 9999 |  nohup java -Xms128m -Xmx128m -jar huanxing-gateway.jar > huanxing-gateway.out &  tail -f huanxing-gateway.out  |
|---|---|---|
| 授权服务   | 5227 |  nohup java -Xms128m -Xmx128m -jar huanxing-auth.jar > huanxing-auth.out &  tail -f huanxing-auth.out |
|---|---|---|
| 系统服务   | 5327 |  nohup java -Xms128m -Xmx128m -jar huanxing-upms-admin.jar > huanxing-upms-admin.out &  tail -f huanxing-upms-admin.out |
|---|---|---|
| 商城服务  | 6127  |  nohup java -Xms128m -Xmx128m -jar huanxing-mall-admin.jar > huanxing-mall-admin.out &  tail -f huanxing-mall-admin.out |
|---|---|---|
| 商城接口服务  | 6227  |  nohup java -Xms128m -Xmx128m -jar huanxing-mall-api.jar > huanxing-mall-api.out &  tail -f huanxing-mall-api.out |
|---|---|---|
| 支付服务  | 7121  |  nohup java -Xms128m -Xmx128m -jar huanxing-pay-api.jar > huanxing-pay-api.out &  tail -f huanxing-pay-api.out |
|---|---|---|
| 微信小程序服务 | 8121   |  nohup java -Xms128m -Xmx128m -jar huanxing-miniapp-weixin.jar > huanxing-miniapp-weixin.out &  tail -f huanxing-miniapp-weixin.out |
|---|---|---|
| 支付宝小程序服务 | 8221   |  nohup java -Xms128m -Xmx128m -jar huanxing-miniapp-alipay.jar > huanxing-miniapp-alipay.out &  tail -f huanxing-miniapp-alipay.out |
|---|---|---|
| 任务调度服务 | 7002   |  nohup java -Xms128m -Xmx128m -jar huanxing-xxl-job-admin.jar > huanxing-xxl-job-admin.out &  tail -f huanxing-xxl-job-admin.out |
|---|---|---|
| 监控服务 | 7003   |  nohup java -Xms128m -Xmx128m -jar huanxing-monitor.jar > huanxing-monitor.out &  tail -f huanxing-monitor.out |
|---|---|---|






