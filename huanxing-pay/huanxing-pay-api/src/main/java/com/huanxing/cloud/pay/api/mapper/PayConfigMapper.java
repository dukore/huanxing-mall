package com.huanxing.cloud.pay.api.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huanxing.cloud.pay.common.entity.PayConfig;
/**
 * 支付配置
 *
 * @author lijx
 * @date 2022/6/16
 */
public interface PayConfigMapper extends BaseMapper<PayConfig> {}
